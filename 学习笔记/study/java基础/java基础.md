---
typora-copy-images-to: ..\img
---

# Java

## Java基础

### java亮眼特点

Java是一门面向对象编程语言，不仅吸收了C++语言的各种优点，还摒弃了C++里难以理解的多继承、指针等概念，因此Java语言具有功能强大和简单易用两个特征。Java语言作为静态面向对象编程语言的代表，极好地实现了面向对象理论，允许程序员以优雅的思维方式进行复杂的编程 。
Java具有简单性、面向对象、分布式、健壮性、安全性、平台独立与可移植性、多线程、动态性等特点 。Java可以编写桌面应用程序、Web应用程序、分布式系统和嵌入式系统应用程序等。

### java和python的区别

从大的方面说：
1、python 既面向对象又面向函数；java存面向对象
2、python 简单，开发效率高，但运行效率慢；java运行效率相对高
3、python 比java更方便的调用c或c++的库
4、python 拥有大量的计算第三方库，更适合科学计算，数据分析等研究工作，而java 更适合商业开发
5、python 有全局解析性锁，Java支持真正的多线程并发操作，能很好的控制资源的的共享
6、python是动态语言，而java是静态语言
从细节上说：
1、java 单继承，而python 多继承
2、python只有四种数据：整数，长整数、浮点数和复数 java则有char，short,byte，int，long，float,double类型
3、语法上的一些区别。

### JDk

JDK(Java Development Kid，Java 开发开源工具包)，是针对 Java 开发人员的产品，是整个 Java 的核心，包括了 Java 运行环境 JRE、Java 工具和 Java 基础类库。

JRE(Java Runtime Environment，Java 运行环境)是运行 JAVA 程序所必须的环境的集合，包含 JVM 标准实现及 Java 核心类库。

JVM(Java Virtual Machine，Java 虚拟机)是整个 Java 实现跨平台的最核心的部分，能够运行以 Java 语言写作的软件程序。


### 8大基础类型 

![1648986339522](../img/1648986339522.png)

#### **基本数据类型分类**

- 字符型（char）
- 布尔类型（boolean）
- 数字型
  - 整形
    - 字节型（byte）
    - 短整形（short）
    - 整形（int）
    - 长整形（long）
  - 浮点型
    - 单精度浮点型（float）
    - 双精度浮点型（double）

引用类型：

- 类（class）

- 接口（interface)

- 数组（array）

- 枚举（enum）

  ​

#### **值传递和引用传递**

在java方法中参数列表有两种类型，基本类型和引用类型

**值传递：**值存放在局部变量表中，无论如何修改只会修改当前栈帧的值，方法执行结束对方法外不会做任何改变；此时需要改变外层的变量，必须返回主动赋值。

**引用传递：**实际参数的引用也就是地址被传递给方法中相对应的形参，函数接受的是原始值的内存地址。形参和实参内容相同，值向同一块内存地址，方法执行时对引用的操作会影响到实际对象。 



#### Boolean占几个字节 

未精确定义字节。Java语言表达式所操作的boolean值，在编译之后都使用Java虚拟机中的int数据类型来代替，而boolean数组将会被编码成Java虚拟机的byte数组，每个数组元素boolean占一个字节。

#### 装箱拆箱：

- 装箱：将基本类型用它们对应的**引用类型**包装起来；

- 拆箱：将包装类型转换为**基本数据类型**；

- valueOf

  ```
  // This method will always cache values in the range -128 to 127,
  public static Integer valueOf(int i) {
      if (i >= IntegerCache.low && i <= IntegerCache.high) // 条件
          return IntegerCache.cache[i + (-IntegerCache.low)];// 取缓存，
          // Integeer的源码中：
          // static final int low = -128; IntegerCache.low = -128
      return new Integer(i);
  }

  ```

> 当使用自动装箱方式创建一个Integer对象时（Integer i4 = 127==》Integer i4 = Integer.valueOf(127)），当数值在-128 ~127时，会将创建的 Integer 对象缓存起来，当下次再出现该数值时，直接从缓存中取出对应的Integer对象。所以上述代码中，x和y引用的是相同的Integer对象。

**为什么要有包装类：**

因为 Java 的设计理念是一切皆是对象，在很多情况下，需要以对象的形式操作，比如 hashCode() 获取哈希值，或者 getClass() 获取类等。**包装类的存在解决了基本数据类型无法做到的事情泛型类型参数、序列化、类型转换、高频区间数据缓存等问题**。

**int和Integer的区别：**

- 最基本的一点是，int是基础数据类型，默认值为0，而Integer是包装数据类型，默认值是null，所以需要实例化才能使用
- 内存中存储的方式不同：int 在内存中直接存储的是数据值，而 Integer 实际存储的是对象引用，当 new 一个 Integer 时实际上是生成一个指针指向此对象
- 变量的比较方式不同：int 可以使用 == 来对比两个变量是否相等，而 Integer 一定要使用 equals 来比较两个变量是否相等。
- 当使用自动装箱方式创建一个Integer对象，也就是通过valueOf（）方法创建Integer对象时，数值在-128 ~127时，会将创建的 Integer 对象缓存起来，当下次再出现该数值时，直接从缓存中取出对应的Integer对象。



#### 类型转换

Java是强类型语言，所以要进行有些运算的时候需要用到类型转换

低--------------------------------------------------------------高

byte，short，char-->int-->long-->float-->double 

运算中，不同类型的数据先转化成同一类型，然后进行运算

允许把容量小的类型转换为容量大的类型

容量大的类型转换为容量小的类型要使用强制类型转换

```
int i= 128 ;  
//强制转换(高到低)
byte b = (byte) i ;//内存溢出
```



### ==、equals和hashcode 

#### == 

它的作用是**判断两个对象的地址是不是相等**。即，判断两个对象是不是同一个对象：

- 基本数据类型==比较的是**值**
- 引用数据类型==比较的是**内存地址**

#### equals 

它的作用也是判断两个对象是否相等。但它一般有两种使用情况：

- 情况1：类没有覆盖 equals() 方法。则通过 equals() 比较该类的两个对象时，等价于通过==比较这两个对象。
- 情况2：类覆盖了 equals() 方法。一般，我们都覆盖 equals() 方法来比较两个对象的内容是否相等；若它们的内容相等，则返回 true (即，认为这两个对象相等)，String类就是个典型的例子。



### 面向对象 

#### 理解

一个对象包含了数据以及对数据操作的方法，这其实是将日常生活中的对象概念应用在软件设计的发展模式中，比如真实世界的一本书，在代码中描述就要抽象书这个概念，将事务特性抽象成属性，比如String name，String  price。而类则描述具有相同特性和行为的对象的集合，是对象的一种抽象，用来定义对象的结构。

面向对象的要素就是**封装，继承与多态**。

#### 封装 

封装把一个对象的**属性私有化**，同时提供一些可以**被外界访问的属性的方法**。

#### 继承 

继承是指某新类继承已经存在的类，该新类拥有被继承的类的所有属性和方法，并且新类可以根据自己的情况拓展属性或方法。其中新类称为子类，原存在的类被称为父类。

注意：

- 子类拥有父类对象**所有的属性和方法**（包括私有属性和私有方法），但是**父类中的私有属性和方法子类是无法访问，只是拥有**。

- 子类可以拥有自己属性和方法，即子类可以对父类进行**扩展**。

- 子类可以用自己的方式实现父类的方法。（多态）。

  > java不支持多继承

#### 多态 

父类和子类都有的一个同名的方法，而在继承关系下，不同的对象采取不同的实现方式。

为什么要统一用一个方法名呢？

虽然简单的把子类的方法名重新命名也能实现功能，但在继承关系复杂的情况下，假如产品需要派生出不同的版本，同一个开发小组的成员每个人承担其中一个版本的工作，组长负责最后的产品整合，每个成员实现各自的父类函数的功能并赋予不同的方法名，组长就得牢牢的记住每一个方法名，随着派生的类越来越多，组长就得崩溃了。而如果使用多态，程序运行期间可以根据对象类型分辨出并该**引用变量发出的方法调用到底是哪个类中实现的方法** 动态调用底层实现是依靠方法表来实现的。



举个例子：

任何事物的多个姿态，多个形态。比如，你说一个猫在吃东西，同样的，你也能说一个动物在吃东西。

```
public class Test {
    public static void main(String[] args){
        Animal animal = new Cat();
        animal.eat() // 猫也会吃饭
        // 你看到了一只猫，同样它也是动物
        // 比如有很多其他种类继承了动物哈，
        // 当编译期间的animal引用变量，到底指的哪个实例对象，（重要）（主语是引用变量）
        // 或者该引用调用的eat方法，到底是哪个实例对象的eat，编译期间恐怕不知道哦（主语是引用变量）
        // 只有运行期间，哦哦， 原来是猫的eat方法哇...
    }
}

```



##### 底层

首先当程序运行需要某个类时，类加载器会将相应的class文件载入到JVM中，并在方法区建立该类的类元信息，里面有类的类型信息，方法信息以及**方法表**。（标黑的这个玩意）

![](../img/方法区.awebp)

面试官：方法表有啥？

我：方法表的结构如同字段表一样，依次包括了**访问标志、名称索引、描述符索引、属性表集合**几项。

接着回答：**方法表是实现动态调用的核心。为了优化对象调用方法的速度，方法区的类型信息会增加一个指针，该指针指向记录该类方法的方法表，方法表中的每一个项都是对应方法的指针**。

到这里：就要分情况讨论了，一个是方法调用，一个是接口

###### 方法调用

先说方法调用：举个例子

```
class Person {
    // 重写object的toString
    public String toString(){
        return "I'm a person.";
    }
    public void eat(){}
    public void speak(){}

}

class Boy extends Person{
    // 重写object的toString
    public String toString(){
        return "I'm a boy";
    }
    // 继承Person的speak
    public void speak(){}
    // 自己实现的自定义方法
    public void fight(){}
}

class Girl extends Person{
    // 重写object的toString
    public String toString(){
        return "I'm a girl";
    }
    // 继承Person的speak
    public void speak(){}
    // 自己实现的自定义方法
    public void sing(){}
}

```

![多态方法调用](../img/多态方法调用.png)

这张图的指向：你可以根据颜色对应上，注意方法表条目指向的具体的**方法地址**。其次注意蓝色部分其继承自于 Person 的方法 eat() 和 speak() 分别指向 **Person 的方法实现和本身的实现**。如果子类改写了父类的方法，那么子类和父类的那些**同名的方法共享一个方法表项**。因此，**所有继承父类的子类的方法表中，其父类所定义的方法的偏移量也总是一个定值**。Person 或 Object中的任意一个方法，在它们的方法表和其子类 Girl 和 Boy 的方法表中的位置 (index) 是一样的。这样 JVM 在调用实例方法其实只需要指定调用方法表中的第几个方法即可。

调用过程：

1. 在常量池里找到方法调用的**符号引用**（肯定先看到Person定义引用类型）
2. 查看Person的方法表，得到speak方法在该**方法表的偏移量**（假设为15），这样就得到该方法的直接引用。
3. 根据this（invoker this字节码）指针得到**具体的对象**（即 girl 所指向的位于堆中的对象）。
4. 根据对象得到该对象对应的方法表，根据偏移量15查看**有无重写（override）该方法**，如果重写，则可以直接调用（Girl的方法表的speak项指向自身的方法而非父类）；如果没有重写，则需要拿到按照继承关系从下往上的基类（这里是Person类）的方法表，同样按照这个偏移量15查看有无该方法。

###### 接口调用

一个类可以实现多个接口，这样的话，在方法表中的索引就会不一样，所以Java 对于接口方法的调用是采用**搜索方法表**的方式。

补充一下：

- **Java的方法重载，就是在类中可以创建多个方法，它们具有相同的名字，但可具有不同的参数列表、返回值类型。调用方法时通过传递的参数类型来决定具体使用哪个方法**，这就是多态性。
- **Java的方法重写，是父类与子类之间的多态性，子类可继承父类中的方法，但有时子类并不想原封不动地继承父类的方法，而是想作一定的修改，这就需要采用方法的重写。重写的参数列表和返回类型均不可修改**。这也是多态性。

#### 重载和重写（覆盖）的区别 

- 重载：发生在同一个类中，**方法名必须相同，参数类型、个数、和顺序不同，方法返回值和访问修饰符也可以不同**。
- 重写：重写是子类对父类的允许访问的方法的实现过程进行重新编写,发生在子类中，**方法名、参数列表必须相同，返回值范围小于等于父类**，抛出的异常范围小于等于父类，访问修饰符范围大于等于父类。另外，如果父类方法访问修饰符为 private 则子类就不能重写该方法。也就是说方法提供的行为改变，而方法的外貌并没有改变。

#### 接口和抽象类的区别

##### **抽象类：**

抽象方法使用 abstract 关键字修饰，仅有声明没有方法体的方法。

```
public abstract void f();    //没有内容
```

抽象类是用abstract修饰的类，可以有具体方法和成员，也可以没有抽象方法。

```
public abstract class BaseActivity {
    private final String TAG = this.getClass().getSimpleName(); //抽象类可以有成员

    void log(String msg){   //抽象类可以有具体方法
        System.out.println(msg);
    }

//    abstract void initView();     //抽象类也可以没有抽象方法
}
```

 抽象类在Java语言中体现了一种继承关系，要想使得继承关系合理，父类和派生类之间必须存在”is a”关系，即父类和派生类在概念本质上应该是相同的。抽象类主要用来进行类型隐藏，我们可以通过抽象类构造出一个固定的一组行为的抽象描述，但是这组行为却能够有任意个可能的具体实现方式。比如动物是一个抽象类，有很多种具体实方式，比如人、猴子、老虎就是具体实现的派生类，我们就可以用动物类型来隐藏人、猴子和老虎的类型。

实例：动物作为一个抽象类，具备了吃饭行为，具体怎么吃饭的需要子类来实现：

```
class Test{
    public static void main(String[] args) {
        Man man = new Man("man");
        man.howToEat();
        Tiger tiger = new Tiger("tiger");
        tiger.howToEat();
        Mokey mokey = new Mokey("mokey");
        mokey.howToEat();
    }
}
abstract  class animal {
    public String name;
    void eat(){
        System.out.println(name + "具有吃饭行为");
    }
    abstract  public void howToEat();

}

class Man extends animal{
    
    Man(String name){
        this.name = name;
    }
    @Override
    public void howToEat() {
        System.out.println("吃饭");
    }
}

class Tiger extends animal{

    Tiger(String name){
        this.name = name;
    }
    @Override
    public void howToEat() {
        System.out.println("吃肉");
    }
}

class Mokey extends animal{

    Mokey(String name){
        this.name = name;
    }
    @Override
    public void howToEat() {
        System.out.println("吃香蕉");
    }
}
```



##### **接口：**

接口使用interface修饰

```
public interface OnClickListener {
    void onClick(View v);
}
```

接口比抽象类更抽象，表示的是”like a”的关系。是一系列方法的声明，是一些方法特征的集合。每个接口可以代表一种最顶层的抽象，可以理解为代表一类东西，如果一个类实现了多个接口，那这个类就有了多种类型，比如我们可以定义一个常量接口，工具方法接口，需要使用常量或者工具方法实现这些接口就行。



接口和抽象类都不能实例化，接口的实现类或抽象类的子类都只有实现了接口或抽象类中的方法后才能实例化，它们的区别是：

1. 方法是否能实现：所有**方法在接口中不能有实现**，而**抽象类可以有非抽象的方法**。
2. 接口中除了**static、final变量**，不能有其他变量，而且方法默认修饰符是public，而抽象类可以有其他变量，抽象方法可以有public、protected和default这些修饰符（要给实现类访问所以不能private）
3. 实现：一个类可以实现**多个接口**，但**只能实现一个抽象类**。接口自己本身可以通过implement关键字扩展多个接口，可以间接的实现多重继承。
4. 抽象类的演化比接口的演化要容易的多，抽象类添加一个方法，其所有实现类都自动的提供了这个新的方法。而接口添加一个方法，所有实现类必须实现这个方法，这样就比较麻烦。

> 关于2抽象类：
>
> 1.子类重写父类方法时，方法的访问权限不能小于原访问权限，在接口中，方法的默认权限就是public，所以子类重写后只能是public，因为接口必须要具体类实现才有意义，所以必须是public，具体类才能访问到。
>
> 2.static修饰就表示它属于类的，随的类的加载而存在的， 如果是非static的话，就表示属于对象的，只有建立对象时才有它，而接口是不能建立对象的 ，所以接口的常量必须定义为static。
>
> 3.final修饰就是保证接口定义的常量不能被实现类去修改，如果没有final的话，由子类随意去修改的话，接口建立这个常量就没有意义了。

### String 

#### String StringBuffer 和 StringBuilder 的区别是什么? 

1. 可变性

- 简单的来说：`String` 类中使用 `final` 关键字修饰字符数组来保存字符串，`private　final　char　value[]`，所以 `String` 对象是不可变的。
- `StringBuilder` 与 `StringBuffer` 都继承自 `AbstractStringBuilder` 类，在 `AbstractStringBuilder` 中也是使用字符数组保存字符串char[]value 但是没有用 `final` 关键字修饰，所以这两种对象都是可变的。

   2.线程安全

- `String` 中的对象是不可变的，也就可以理解为常量，线程安全。
- `AbstractStringBuilder` 是 `StringBuilder` 与 `StringBuffer` 的公共父类，定义了一些字符串的基本操作，如 `expandCapacity`、`append`、`insert`、`indexOf` 等公共方法。`StringBuffer` 对方法加了同步锁或者对调用的方法加了同步锁，所以是线程安全的。`StringBuilder` 并没有对方法进行加同步锁，所以是非线程安全的。

   3.性能

- 每次对 `String` 类型进行改变的时候，都会生成一个新的 `String` 对象，然后将指针指向新的 `String` 对象。
- `StringBuffer` 每次都会对 `StringBuffer` 对象本身进行操作，而不是生成新的对象并改变对象引用。相同情况下使用 `StringBuilder` 相比使用 `StringBuffer` 仅能获得 10%~15% 左右的性能提升，但却要冒多线程不安全的风险。
- 操作少量的数据: 适用String
- 单线程操作字符串缓冲区下操作大量数据: 适用StringBuilder
- 多线程操作字符串缓冲区下操作大量数据: 适用StringBuffer

#### String对象和常量池

```
public class StringTest {
    public static void main(String[] args) {
        String str1 = "todo"; // 常量池
        String str2 = "todo"; // 从常量池找了str1
        String str3 = "to"; // 常量池
        String str4 = "do"; // 常量池
        String str5 = str3 + str4; // 内部用StringBuilder拼接了一波。 因此， 并非常量池
        String str6 = new String(str1); //  创建对象了， 那还能是常量池的引用？
    }
}

```

分析一波：

- 成的class文件中会在常量池中**保存“todo”、“to”和“do”三个String常量**。
- 变量str1和str2均保存的是常量池中“todo”的引用，所以str1==str2成立；
- 在执行 str5 = str3 + str4这句时，**JVM会先创建一个StringBuilder对象，通过StringBuilder.append()方法将str3与str4的值拼接**，然后通过StringBuilder.toString()返回一个堆中的String对象的引用，赋值给str5，因此str1和str5指向的不是同一个String对象，str1 == str5不成立；
- String str6 = new String(str1)一句显式创建了一个新的String对象，因此str1 == str6不成立便是显而易见的事了。

#### intern

- jdk6： 执行intern()方法时，**若常量池中不存在等值的字符串，JVM就会在常量池中创建一个等值的字符串，然后返回该字符串的引用**。
- jdk7： 执行intern操作时，如果常量池已经存在该字符串，则直接返回字符串引用，否则**复制该字符串对象的引用到常量池中并返回**。

### 关键字 

#### final

final关键字主要用在三个地方：变量、方法、类。

- 对于一个final变量，如果是**基本数据类型的变量，则其数值一旦在初始化之后便不能更改**；如果是引用类型的变量，则在对其初始化之后便**不能再让其指向另一个对象**。
- 当用final修饰一个类时，表明**这个类不能被继承**。final类中的所有成员方法都会被隐式地指定为final方法。
- 使用final方法的原因有两个。第一个原因是把方法锁定，以防任何继承类修改它的含义；第二个原因是效率。在早期的Java实现版本中，会将final方法转为内嵌调用。但是如果方法过于庞大，可能看不到内嵌调用带来的任何性能提升（现在的Java版本已经不需要使用final方法进行这些优化了）。类中所有的private方法都隐式地指定为final。

final修饰有啥好处

- final的关键字**提高了性能**，JVM和java应用会**缓存final变量**；
- final变量可以在多线程环境下保持**线程安全**；
- 使用final的关键字提高了性能，JVM会对方法变量类进行优化；

#### static

- **修饰成员变量和成员方法:** 被 static 修饰的成员属于类，不属于单个这个类的某个对象，被类中所有对象共享，可以并且建议通过类名调用。被static 声明的成员变量属于静态成员变量，静态变量存放在 Java 内存区域的方法区。调用格式：`类名.静态变量名` `类名.静态方法名()`
- **静态代码块:** 静态代码块定义在类中的方法外, 静态代码块在非静态代码块之前执行(静态代码块—>非静态代码块—>构造方法)。 该类不管创建多少对象，静态代码块只执行一次.
- **静态内部类（static修饰类的话只能修饰内部类）：** 静态内部类与非静态内部类之间存在一个最大的区别: 非静态内部类在编译完成之后会隐含地保存着一个引用，该引用是指向创建它的外围类，但是静态内部类却没有。没有这个引用就意味着：1. 它的创建是不需要依赖外围类的创建。2. 它不能使用任何外围类的非static成员变量和方法。
- **静态导包(用来导入类中的静态资源，1.5之后的新特性):** 格式为：`import static` 这两个关键字连用可以指定导入某个类中的指定静态资源，并且不需要使用类名调用类中静态成员，可以直接使用类中静态成员变量和成员方法。

### Java值传递

下面再总结一下 Java 中方法参数的使用情况：

- 一个方法**不能修改一个基本数据类型的参数**（即数值型或布尔型）。
- 一个方法可以改变**一个对象参数的状态**。
- 一个方法**不能让对象参数引用一个新的对象**。

[个人写的例子](https://dreamcater.gitee.io/javabooks/#/codes/Java%E5%80%BC%E4%BC%A0%E9%80%92%E7%9A%84%E9%97%AE%E9%A2%98)

### 空指针场景及解决

**场景类型：**

1、数据库空数据

数据库存null值，其实还是比较容易发现的。但是安全保险，还是建议还是先确认下当前所要使用字段的含义。同时去数据库中抽样遍历下所要使用的字段。注意要，不要只关注一张表，最好涉及到的表都要看下。记住作为一名CRUD选手，也要时刻保持一个谨慎心的。

2、容器空指针

使用上层业务返回的容器时，一定要进行空指针校验。可以自己写一个Util，或者使用开源打Util包进行判断。

```
//1、本身为空。
List<String> list = null;
//2、map本身key不存在，对于返回值没有校验，直接使用。
Map<String,String> map = new HashMap<>();
map.put("1","1");
String s = map.get(2);
// s为null，调用其方法必然空指针异常
s.length();
```

一个非常好的技术是创建返回一个空集合的方法，而不是一个null值。你的应用程序的代码可以遍历空集合并使用它的方法和字段，而不会抛出一个NullPointerException。Collections类提供了方便的空 List，Set 和 Map: `Collections.EMPTY_LIST`，`Collections.EMPTY_SET`，`Collections.EMPTY_MAP`。

```
public class Example {
   private static List<Integer> numbers = null;

   public static List<Integer> getList() {
     if (numbers == null){
         return Collections.EMPTY_LIST;
     } else {
         return numbers;
     }
   }
}
```

3、xx.valueOf()

在接口调用的过程中，会存在类型不同，需要转换的场景。最常见的是：将Long类型，转换成String类型，这时候便会调用
String.valueOf(xx);。你不保证别人传递给你的参数会不会有脏数据，即使已经约定好不能传空数据。但是一定要记住，别人的服务一定是不可靠的，可靠性要由自己来保证。

```
Long temp = null;
String.valueOf(temp ==null ? "" : temp);
```

4、注解注入

对于Java的项目，现在基本都基于Spring的。这就避免不了使用Spring提供的IOC功能。记住，当使用注解（@Autowired等等）进行对象注入的时候，一定要先将类提供给IOC进行管理。可以通过注解方式（@Service等等）也可以使用配置文件的方式。推荐使用注解的方式，简单便于维护。

**equals和equalsIgnoreCase()方法**

Object类中的equals 方法在非空对象引用上实现相等关系，具有对称性 x.equals(y) 和 y.equals(x) 结果是一样的，但当x == null时会抛出空指针异常。所以我们要把确定不为null的对象或值放在前面。

```
String x = null;
String y = "world";
if(x.equals(y)){ // java.lang.NullPointerException
	
}

if(y.equals(x)){//即便 x 是 null也能避免 NullPointerException
    
}
```

**使用null安全的方法和库**

有很多开源库已经为您做了繁重的空指针检查工作。其中最常用的一个的是Apache commons 中的`StringUtils`。你可以使用`StringUtils.isBlank()`，`isNumeric()`，`isWhiteSpace()`以及其他的工具方法而不用担心空指针异常。

```
//StringUtils方法是空指针安全的，他们不会抛出空指针异常
System.out.println(StringUtils.isEmpty(null));
System.out.println(StringUtils.isBlank(null));
System.out.println(StringUtils.isNumeric(null));
System.out.println(StringUtils.isAllUpperCase(null));

输出:
true
true
false
false
```

**断言**

断言是用来检查程序的安全性的，在使用之前进行检查条件，如果不符合条件就报异常，符合就继续。Java 中自带的断言关键字：assert，如：

```
assert name == null : "名称不能为空";
```

输出：

```
Exception in thread "main" java.lang.AssertionError: 名称不正确
```

不过默认是不启动断言检查的，需要要带上 JVM 参数：-enableassertions 才能生效。

Java 中这个用的很少，建议使用 Spring 中的，更强大，更方便好用。Spring中的用法：

```
Assert.notNull(name,"名称不能为空");
```





### 异常

Java中异常主要分为Error和Exception

![1649249765967](../img/1649249765967.png)

#### Error

error是致命性的、用户无法处理的系统程序错误。如果程序在启动时出现Error，则启动失败；如果程序运行过程中出现Error，则系统将退出程序。出现Error是系统的内部错误或资源耗尽，Error不能在程序运行过程中被动态处理，一旦出现Error，系统能做的只有记录错误的原因和安全终止。

#### **Exception** 

exception不是致命的，表示在运行中的程序发生了程序员不期望发生的事情，异常分为可分为检查型异常和非检查型异常，检查异常在源代码里必须显式地进行捕获处理，比如标准I/O输入时就必须捕获IOException异常。不检查异常就是所谓的运行时异常，常见的如数组越界、除零等。如下图所示

![1649250048540](../img/1649250048540.png)

#### 捕获异常 

面试官：如何捕获异常？

我：

第一种方法是使用try{...}catche(...){...}块

- `try` 块： 用于捕获异常。其后可接零个或多个`catch`块，如果没有`catch`块，则必须跟一个`finally`块。
- `catch` 块： 用于处理`try`捕获到的异常。
- `finally` 块： 无论是否捕获或处理异常，`finally`块里的语句**都会**被执行。当在`try`块或`catch`块中遇到`return`语句时，`finally`语句块**将在方法返回之前被执行**。

第二种方法是声明抛出异常，在方法名后面加上throws 来抛出异常,出现异常时会沿着调用层次向上传递，交给调用它的上一层方法来处理。

> 与 throws 不同，可以直接使用 throw 抛出一个异常，抛出时直接抛出异常类的实例化对象即可，需配合throws或try，catch使用
>
> ```
> public class Test{
>     public static void main(String[] args) {
>         try {
>             throw new Exception("自己抛出异常！");//抛出异常的实例化对象
>         }catch (Exception e){//捕获异常
>             System.out.println(e);
>         }
>     }
> }
> ```
>
> 虽然 throw 关键字的使用完全符合异常的处理机制，但是一般来讲用户都在避免异常的产生，所以不会手工抛出一个新的异常类的实例，往往会抛出程序中已经产生的异常类实例。

<https://blog.csdn.net/efei7968/article/details/87077218>

<https://blog.csdn.net/efei7968/article/details/87174324?>

### IO 

一个进程的地址空间划分为**用户空间**和**内核空间**。像我们平常运行的应用程序都是运行在用户空间，只有内核空间才能进行系统态级别的资源有关的操作，比如如文件管理、进程通信、内存管理等等。也就是说，我们想要进行 IO 操作，一定是要依赖内核空间的能力。

**我们的应用程序对操作系统的内核发起 IO 调用（系统调用），操作系统负责的内核执行具体的 IO 操作。**

**Java中常见的IO模型有**：BIO（同步阻塞IO模型）、NIO（同步非阻塞IO模型）、AIO（异步IO模型）。

#### BIO 

**BIO (Blocking I/O)**:**同步阻塞I/O模式**，数据的读取写入必须阻塞在一个线程内等待其完成。以Socket多线程为例，当一个线程调用read（）或write（）时，该线程会被阻塞。同步阻塞I/O适用于发送数据量大的情况，比较适合少量的连接使用较大的带宽同步阻塞I/O。因为一个线程处理一个网络请求，承载不了高并发。



![1650690841561](../img/1650690841561.png)

![1650852114415](../img/1650852114415.png)

#### NIO 

**NIO (New I/O)**:NIO是一种**同步非阻塞的I/O模型**，也是I/O多路复用的基础，，已经被越来越多地应用到大型应用服务器，成为解决高并发与大量连接、I/O处理问题的有效方式。一个线程可以管理多个网络连接。

![1650690892991](../img/1650690892991.png)

![1650852161740](../img/1650852161740.png)

**原理：**

NIO是一种基于通道和缓冲区的I/O方式，它可以使用Native函数库直接分配堆外内存（区别于JVM的运行时数据区），然后通过一个存储在java堆里面的DirectByteBuffer对象作为这块内存的直接引用进行操作。这样能在一些场景显著提高性能，因为避免了在Java堆和Native堆中来回复制数据。

**Java NIO组件**
NIO主要有三大核心部分：Channel(通道)，Buffer(缓冲区), Selector（选择器）。传统IO是基于字节流和字符流进行操作（基于流），而NIO基于Channel和Buffer(缓冲区)进行操作，数据总是从通道读取到缓冲区中，或者从缓冲区写入到通道中。Selector(选择区)用于监听多个通道的事件（比如：连接打开，数据到达）。因此，单个线程可以监听多个数据通道。

- Buffer

  Buffer（缓冲区）是一个用于存储特定基本类型数据的容器。除了boolean外，其余每种基本类型都有一个对应的buffer类。Buffer类的子类有ByteBuffer, CharBuffer, DoubleBuffer, FloatBuffer, IntBuffer, LongBuffer, ShortBuffer 。

- Channel

  Channel（通道）表示到实体，如硬件设备、文件、网络套接字或可以执行一个或多个不同 I/O 操作（如读取或写入）的程序组件的开放的连接。Channel接口的常用实现类有FileChannel（对应文件IO）、DatagramChannel（对应UDP）、SocketChannel和ServerSocketChannel（对应TCP的客户端和服务器端）。Channel和IO中的Stream(流)是差不多一个等级的。只不过Stream是单向的，譬如：InputStream, OutputStream.而Channel是双向的，既可以用来进行读操作，又可以用来进行写操作。

- Selector

  Selector（选择器）用于监听多个通道的事件（比如：连接打开，数据到达）。因此，单个的线程可以监听多个数据通道。即用选择器，借助单一线程，就可对数量庞大的活动I/O通道实施监控和维护。

**NIO服务器端如何实现非阻塞？**
服务器上所有Channel需要向Selector注册，而Selector则负责监视这些Socket的IO状态(观察者)，当其中任意一个或者多个Channel具有可用的IO操作时，该Selector的select()方法将会返回大于0的整数，该整数值就表示该Selector上有多少个Channel具有可用的IO操作，并提供了selectedKeys（）方法来返回这些Channel对应的SelectionKey集合(一个SelectionKey对应一个就绪的通道)。正是通过Selector，使得服务器端只需要不断地调用Selector实例的select()方法即可知道当前所有Channel是否有需要处理的IO操作。注：java NIO就是多路复用IO，jdk7之后底层是epoll模型。

#### AIO 

**AIO (Asynchronous I/O)**: AIO 也就是 NIO 2。在 Java 7 中引入了 NIO 的改进版 NIO 2,它是**异步非阻塞的IO模型**。异步 IO 是基于事件和回调机制实现的，也就是应用操作之后会直接返回，不会堵塞在那里，当后台处理完成，操作系统会通知相应的线程进行后续的操作。AIO 是异步IO的缩写，虽然 NIO 在网络操作中，提供了非阻塞的方法，但是 NIO 的 IO 行为还是同步的。对于 NIO 来说，我们的业务线程是在 IO 操作准备好时，得到通知，接着就由这个线程自行进行 IO 操作，IO操作本身是同步的。查阅网上相关资料，我发现就目前来说 AIO 的应用还不是很广泛，Netty 之前也尝试使用过 AIO，不过又放弃了。

![1650852198340](../img/1650852198340.png)

### 反射 

> [反射 ](https://juejin.cn/post/6864324335654404104)

#### 反射是什么

我：在Java的反射机制中是指在**运行状态**中，对于任意一个类都能够知道这个类所有的**属性和方法**；并且对于任意一个对象，都能够调用它的**任意一个方法**；这种**动态获取信息以及动态调用对象方法**的功能成为 Java 语言的反射机制。

#### 反射有什么好处

我：对于在`编译期`无法确定使用哪个数据类的场景，通过`反射`可以在程序运行时**构造出不同的数据类实例**。

比如我们要定义一个map，首先用的是HashMap，但我们某一天发现不适合用HashMap来存储键值对，更倾向于用LinkedHashMap存储，如果我们重新去修改源码，然后对代码进行编译打包，再到JVM上重启项目，效率会非常低。

所以我们可以定义一个开关，也就是一个getMap方法，传入一个参数代表类型，然后返回对应的类型。

```
 public static Map<Integer,Integer> getMap(String param){
        Map<Integer, Integer> map = null;
        if (param.equals("HashMap")) {
            map = new HashMap<>();
        } else if (param.equals("LinkedHashMap")) {
            map = new LinkedHashMap<>();
        } else if (param.equals("WeakHashMap")) {
            map = new WeakHashMap<>();
        }
        return map;
    }
    public static void main(String[] args) {
        Map<Integer, Integer> map = getMap("HashMap");
    }
```

但如果某一天多了一种Map的类型，还是避免不了修改代码，所以这时候反射就派上用场了。

在代码运行之前，我们**不确定**将来会使用哪一种数据结构，只有在程序**运行时才决定**使用哪一个数据类，而`反射`可以在**程序运行过程**中动态**获取类信息**和**调用类方法**。通过反射构造类实例，代码会演变成下面这样。

```
    public static Map<Integer,Integer> getMap(String className) {
        Class aClass = Class.forName(className);
        Constructor constructor = aClass.getConstructor();
        return (Map<Integer,Integer>)constructor.newInstance();
    }
    public static void main(String[] args) {
        Map<Integer, Integer> map = getMap("HashMap");
    }
```

首先通过类名查找类，然后找到这个类的构造方法，执行这个构造方法就能创建所需要的类。



#### 反射的API

1. Class 类：反射的核心类，任何运行在内存中的所有类都是该 Class 类的实例对象，可以获取类的属性，方法等信息。
2. Field 类：表示类的成员变量，可以用来获取和设置类之中的属性值。
   - Field[] getFields()：获取类中所有被`public`修饰的所有变量
   - Field getField(String name)：根据**变量名**获取类中的一个变量，该**变量必须被public修饰**
   - Field[] getDeclaredFields()：获取类中所有的变量，但**无法获取继承下来的变量**
   - Field getDeclaredField(String name)：根据姓名获取类中的某个变量，**无法获取继承下来的变量**
3. Method 类：表示类的方法，它可以用来获取类中的方法信息或者执行方法。
   - Method[] getMethods()：获取类中被`public`修饰的所有方法
   - Method getMethod(String name, Class...<?> paramTypes)：根据**名字和参数类型**获取对应方法，该方法必须被`public`修饰
   - Method[] getDeclaredMethods()：获取`所有`方法，但**无法获取继承下来的方法**
   - Method getDeclaredMethod(String name, Class...<?> paramTypes)：根据**名字和参数类型**获取对应方法，**无法获取继承下来的方法**
4. Constructor 类：表示类的构造方法
   - Constuctor[] getConstructors()：获取类中所有被`public`修饰的构造器
   - Constructor getConstructor(Class...<?> paramTypes)：根据`参数类型`获取类中某个构造器，该构造器必须被`public`修饰
   - Constructor[] getDeclaredConstructors()：获取类中所有构造器
   - Constructor getDeclaredConstructor(class...<?> paramTypes)：根据`参数类型`获取对应的构造器

#### 获取class对象的三种方式

- 类名.class：会使 JVM 将使用类装载器将类装入内存（前提是类还没有装入内存），不做类的初始化工作，返回 Class 对象。

```
Class clazz = SmallPineapple.class;
复制代码
```

- 实例.getClass()：通过实例化对象获取该实例的 Class 对象。会对类进行静态初始化、非静态初始化，返回引用运行时真正所指的对象（因为子对象的引用可能会赋给父对象的引用变量中）所属的类的 Class 对象。

```
SmallPineapple sp = new SmallPineapple();
Class clazz = sp.getClass();
复制代码
```

- Class.forName(className)：通过类的**全限定名**获取该类的 Class 对象，会装入类并做类的静态初始化，返回 Class 对象。

```
Class clazz = Class.forName("com.bean.smallpineapple");
```

> 静态属性初始化是在加载类的时候初始化，而非静态属性初始化是 new 类实例对象的时候初始化。它们三种情况在生成 Class 对象的时候都会先判断内存中是否已经加载此类。

#### 反射应用场景

反射常见的应用场景这里介绍`3`个：

- Spring 实例化对象：当程序启动时，Spring 会读取配置文件`applicationContext.xml`并解析出里面所有的  标签实例化到`IOC`容器中。
- 反射 + 工厂模式：通过`反射`消除工厂中的多个分支，如果需要生产新的类，无需关注工厂类，工厂类可以应对各种新增的类，`反射`可以使得程序更加健壮。
- JDBC连接数据库：使用JDBC连接数据库时，指定连接数据库的`驱动类`时用到反射加载驱动类

##### Spring 的 IOC 容器

在 Spring 中，经常会编写一个上下文配置文件`applicationContext.xml`，里面就是关于`bean`的配置，程序启动时会读取该 xml 文件，解析出所有的 `<bean>`标签，并实例化对象放入`IOC`容器中。

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean id="smallpineapple" class="com.bean.SmallPineapple">
        <constructor-arg type="java.lang.String" value="小菠萝"/>
        <constructor-arg type="int" value="21"/>
    </bean>
</beans>
```

在定义好上面的文件后，通过`ClassPathXmlApplicationContext`加载该配置文件，程序启动时，Spring 会将该配置文件中的所有`bean`都实例化，放入 IOC 容器中，IOC 容器本质上就是一个工厂，通过该工厂传入 <bean> 标签的`id`属性获取到对应的实例。

```
public class Main {
    public static void main(String[] args) {
        ApplicationContext ac =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        SmallPineapple smallPineapple = (SmallPineapple) ac.getBean("smallpineapple");
        smallPineapple.getInfo(); // [小菠萝的年龄是：21]
    }
}
```

Spring 在实例化对象的过程经过简化之后，可以理解为反射实例化对象的步骤：

- **获取Class对象的构造器**
- 通过构造器**调用 newInstance()** 实例化对象

当然 Spring 在实例化对象时，做了非常多额外的操作，才能够让现在的开发足够的**便捷且稳定**。

> 在之后的文章中会专门写一篇文章讲解如何利用反射实现一个`简易版`的`IOC`容器，IOC容器原理很简单，只要掌握了反射的思想，了解反射的常用 API 就可以实现，我可以提供一个简单的思路：利用 HashMap 存储所有实例，key 代表 <bean> 标签的 `id`，value 存储对应的实例，这对应了 Spring IOC容器管理的对象默认是**单例**的。

##### 反射 + 抽象工厂模式

传统的工厂模式，如果需要生产新的子类，**需要修改工厂类，在工厂类中增加新的分支**；

```
public class MapFactory {
    public Map<Object, object> produceMap(String name) {
        if ("HashMap".equals(name)) {
            return new HashMap<>();
        } else if ("TreeMap".equals(name)) {
            return new TreeMap<>();
        } // ···
    }
}
```

利用反射和工厂模式相结合，在产生新的子类时，**工厂类不用修改任何东西**，可以专注于子类的实现，**当子类确定下来时，工厂也就可以生产该子类了。**

反射 + 抽象工厂的**核心思想**是：

- **在运行时通过参数传入不同子类的全限定名获取到不同的 Class 对象，调用 newInstance() 方法返回不同的子类。细心的读者会发现提到了子类**这个概念，所以反射 + 抽象工厂模式，一般会用于有**继承**或者**接口实现**关系。

例如，在运行时才确定使用哪一种 `Map` 结构，我们可以利用反射传入某个具体 Map 的全限定名，实例化一个特定的子类。

```
public class MapFactory {
    /**
     * @param className 类的全限定名
     */
    public Map<Object, Object> produceMap(String className) {
        Class clazz = Class.forName(className);
        Map<Object, Object> map = clazz.newInstance();
        return map;
    }
}
```

`className` 可以指定为 java.util.HashMap，或者 java.util.TreeMap 等等，根据业务场景来定。

##### JDBC 加载数据库驱动类

在导入第三方库时，JVM不会主动去加载外部导入的类，而是**等到真正使用时，才去加载需要的类**，正是如此，我们可以在获取数据库连接时传入驱动类的全限定名，交给 JVM 加载该类。

```
public class DBConnectionUtil {
    /** 指定数据库的驱动类 */
    private static final String DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";
    
    public static Connection getConnection() {
        Connection conn = null;
        // 加载驱动类
        Class.forName(DRIVER_CLASS_NAME);
        // 获取数据库连接对象
        conn = DriverManager.getConnection("jdbc:mysql://···", "root", "root");
        return conn;
    }
}
```

在我们开发 SpringBoot 项目时，会经常遇到这个类，但是可能习惯成自然了，就没多大在乎，我在这里给你们看看常见的`application.yml`中的数据库配置，我想你应该会恍然大悟吧。

![img](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/a2d5c58f62dd41b7a9affe7751be7428~tplv-k3u1fbpfcp-zoom-in-crop-mark:1304:0:0:0.awebp)

这里的 **driver-class-name**，和我们一开始加载的类是不是觉得很相似，这是因为**MySQL**版本不同引起的**驱动类不同**，这体现使用反射的好处：不需要修改源码，**仅加载配置文件就可以完成驱动类的替换**。

#### 反射的优势及缺陷

反射的**优点**：

- **增加程序的灵活性**：面对需求变更时，可以灵活地实例化不同对象

但是，有得必有失，一项技术不可能只有优点没有缺点，反射也有**两个比较隐晦的缺点**：

- **破坏类的封装性**：可以强制访问 private 修饰的信息
- **性能损耗**：反射相比直接实例化对象、调用方法、访问变量，中间需要非常多的**检查步骤和解析步骤**，JVM无法对它们优化。

##### 增加程序的灵活性

这里不再用 SmallPineapple 举例了，我们来看一个更加`贴近开发`的例子：

- 利用反射连接数据库，**涉及到数据库的数据源**。在 SpringBoot 中一切约定大于配置，想要**定制配置**时，使用`application.properties`配置文件指定数据源

**角色1 - Java的设计者**：我们设计好`DataSource`接口，你们其它数据库厂商想要开发者用`你们的数据源`监控数据库，就得实现`我的这个接口`！

**角色2 - 数据库厂商**：

- MySQL 数据库厂商：我们提供了 **com.mysql.cj.jdbc.MysqlDataSource** 数据源，开发者可以使用它连接 MySQL。
- 阿里巴巴厂商：我们提供了 **com.alibaba.druid.pool.DruidDataSource** 数据源，我这个数据源更牛逼，具有**页面监控**，**慢SQL日志记录**等功能，开发者快来用它监控 MySQL吧！
- SQLServer 厂商：我们提供了 **com.microsoft.sqlserver.jdbc.SQLServerDataSource** 数据源，如果你想实用SQL Server 作为数据库，那就使用我们的这个数据源连接吧

**角色3 - 开发者**：我们可以用`配置文件`指定使用`DruidDataSource`数据源

```
spring.datasource.type=com.alibaba.druid.pool.DruidDataSource
复制代码
```

**需求变更**：某一天，老板来跟我们说，Druid 数据源不太符合我们现在的项目了，我们使用 **MysqlDataSource** 吧，然后程序猿就会修改配置文件，重新加载配置文件，并重启项目，完成数据源的切换。

```
spring.datasource.type=com.mysql.cj.jdbc.MysqlDataSource
复制代码
```

在改变连接数据库的数据源时，只需要改变配置文件即可，**无需改变任何代码**，原因是：

- **Spring Boot 底层封装好了连接数据库的数据源配置，利用反射，适配各个数据源。**

下面来简略的进行源码分析。我们用`ctrl+左键`点击`spring.datasource.type`进入 DataSourceProperties 类中，发现使用setType() 将**全类名转化为 Class 对象**注入到`type`成员变量当中。在连接并监控数据库时，就会使用指定的数据源操作。

```
private Class<? extends DataSource> type;

public void setType(Class<? extends DataSource> type) {
    this.type = type;
}
复制代码
```

`Class`对象指定了泛型上界`DataSource`，我们去看一下各大数据源的`类图结构`。

![img](https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/cedfe98ee1784646bbf5e532246a58fc~tplv-k3u1fbpfcp-zoom-in-crop-mark:1304:0:0:0.awebp)

**上图展示了一部分数据源，当然不止这些，但是我们可以看到，无论指定使用哪一种数据源，我们都只需要与配置文件打交道，而无需更改源码，这就是反射的灵活性！**

##### 破坏类的封装性

很明显的一个特点，反射可以获取类中被`private`修饰的变量、方法和构造器，这**违反了面向对象的封装特性**，因为被 private 修饰意味着不想对外暴露，只允许本类访问，而`setAccessable(true)`可以无视访问修饰符的限制，外界可以强制访问。

还记得`单例模式`一文吗？里面讲到反射破坏饿汉式和懒汉式单例模式，所以之后用了`枚举`避免被反射KO。

回到最初的起点，SmallPineapple 里有一个 weight 属性被 private 修饰符修饰，目的在于自己的体重并不想给外界知道。

```
public class SmallPineapple {
    public String name;
    public int age;
    private double weight; // 体重只有自己知道
    
    public SmallPineapple(String name, int age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }
    
}
复制代码
```

虽然 weight 属性理论上只有自己知道，但是如果经过反射，这个类就像在**裸奔**一样，在反射面前变得`一览无遗`。

```
SmallPineapple sp = new SmallPineapple("小菠萝", 21, "54.5");
Clazz clazz = Class.forName(sp.getClass());
Field weight = clazz.getDeclaredField("weight");
weight.setAccessable(true);
System.out.println("窥觑到小菠萝的体重是：" + weight.get(sp));
// 窥觑到小菠萝的体重是：54.5 kg
复制代码
```

##### 性能损耗

**在直接 new 对象并调用对象方法和访问属性时，编译器会在编译期提前检查可访问性，如果尝试进行不正确的访问，IDE会提前提示错误，例如参数传递类型不匹配，非法访问 private 属性和方法。**

> 而在利用反射操作对象时，编译器无法提前得知对象的类型，访问是否合法，参数传递类型是否匹配。只有在程序运行时调用反射的代码时才会从头开始检查、调用、返回结果，JVM也无法对反射的代码进行优化。

虽然反射具有性能损耗的特点，但是我们不能一概而论，产生了使用反射就会性能下降的思想，反射的慢，需要同时调用上`100W`次才可能体现出来，在几次、几十次的调用，并不能体现反射的性能低下。所以不要一味地戴有色眼镜看反射，**在单次调用反射的过程中，性能损耗可以忽略不计。如果程序的性能要求很高，那么尽量不要使用反射。**



### 深浅拷贝

clone（）方法是object对象类的protected保护方法，它不是public，使用要使用该方法的类需要实现重写clone方法，将该方法的属性设置为public。而且还要注意的是，还需要用该类来实现Cloneable接口，来指示Object类中的clone()方法可以用来合法的进行克隆，这样就不会报CloneNotSupportedException异常。

> clone方法是native方法，native方法的效率一般远高于非native方法。

拷贝的两种模式：

- **浅拷贝**：在内存中开辟一块和原始对象一样的内存空间，然后原样拷贝原始对象中的内容。也就是对**基本数据类型进行值传递**，对**引用数据类型进行引用传递般的拷贝** ，这就是**浅拷贝**。
- **深拷贝**：对**基本数据类型进行值传递**，对**引用数据类型，创建一个新的对象，并复制其内容** ，这就是**深拷贝**。
- 也就二者对引用数据类型有区别

[个人写的例子](https://dreamcater.gitee.io/javabooks/#/codes/%E6%B5%85%E6%8B%B7%E8%B4%9D%E5%92%8C%E6%B7%B1%E6%8B%B7%E8%B4%9D%E4%BE%8B%E5%AD%90)

### Object 

```
public final native Class<?> getClass();
public native int hashCode(); // 返回对象的哈希代码值。
public boolean equals(Object obj) 
protected native Object clone() // 创建并返回此对象的副本。
public String toString() // 返回对象的字符串表示形式。
public final native void notify(); // 唤醒正在该对象的监视器上等待的单个线程。
public final native void notifyAll(); // 唤醒正在该对象的监视器上等待的全部线程。
public final native void wait(); // 使当前线程等待，直到另一个线程调用此对象的方法或方法。
protected void finalize(); // 当垃圾回收确定不再有对对象的引用时，由对象上的垃圾回收器调用。

```

### 四种修饰符的限制范围

1. public：可以被所有其他类所访问。
2. private：只能被自己访问和修改。
3. protected：自身，子类及同一个包中类可以访问。
4. default（默认）：同一包中的类可以访问

### 内部类

> [引用链接](https://juejin.cn/post/6844903870062149646)

使用场景：

- 每个内部类都可以独立的继承一个类，所以无论外部类是否继承了某个类，内部类依然可以继承其他类，这就完美的解决了java没有多继承的问题。
- 可以有效的将有一定关系的类组织在一起，又可以对外界有所隐藏。
- 内部类有访问外部类成员变量的能力
- 方便编写多线程代码

java有三种类型的内部类

- 普通内部类

  ```
  public class Demo {

      // 普通内部类
      public class DemoRunnable implements Runnable {
          @Override
          public void run() {
          }
      }
  }
  ```

- 匿名内部类

  ```
  public class Demo {

      // 匿名内部类
      private Runnable runnable = new Runnable() {
          @Override
          public void run() {

          }
      };
  }
  ```

- 方法内局部内部类

  ```
  public class Demo {

      // 局部内部类
      public void work() {
          class InnerRunnable implements Runnable {
              @Override
              public void run() {

              }
          }
          InnerRunnable runnable = new InnerRunnable();
      }

  }
  ```

**实现：**

```
public class Demo {
    // 普通内部类
    public class DemoRunnable implements Runnable {
        @Override
        public void run() {
        }
    }
}
```

字节码文件：

```
package inner;

public class Demo$DemoRunnable implements Runnable {
    public Demo$DemoRunnable(Demo var1) {
        this.this$0 = var1;
    }

    public void run() {
    }
}
```

生成的类只有一个构造器，参数就是Demo类型，而且保存到内部类本身的this$0字段中。到这里我们其实已经可以想到，内部类持有的外部类引用就是通过这个构造器传递进来的（局部变量也是靠构造器传参），它是一个强引用。

匿名内部类和普通内部类实现基本一致，只是编译器自动给它拼了个类名，所以匿名内部类不能自定义构造器，因为名字编译完成后才能确定。 方法局部内部类原理也是一样的。

**JDK8之前为什么匿名内部类访问的局部变量必须要用final修饰？**

用final修饰实际上就是为了保护数据的一致性。用final修饰后，引用变量的地址值不能改变，也就是说这个引用变量就无法再指向其它对象了。

>  注：引用类型变量其本质是存入的是一个引用地址，说白了还是一个值（可以理解为内存中的地址值）

将数据拷贝完成后，如果不用final修饰，则原先的局部变量可以发生变化。这里到了问题的核心了，如果局部变量发生变化后，匿名内部类是不知道的（因为他只是拷贝了局部变量的值，并不是直接使用的局部变量）。这里举个栗子：原先局部变量指向的是对象A，在创建匿名内部类后，匿名内部类中的成员变量也指向A对象。但过了一段时间局部变量的值指向另外一个B对象，但此时匿名内部类中还是指向原先的A对象。那么程序再接着运行下去，可能就会导致程序运行的结果与预期不同。
![1649682937014](../img/1649682937014.png)

JDK1.8之后，匿名内部类中需要访问的局部变量不需要用final修饰符修饰，因为底层还是帮我们加了final，改变了局部变量的引用值就会编译报错。

**如何创建内部类实例?**

由于内部类对象需要持有外部类对象的引用，所以必须得先有外部类对象

```
Demo.DemoRunnable demoRunnable = new Demo().new DemoRunnable();
```

**如何继承内部类？**

```
    public class Demo2 extends Demo.DemoRunnable {
        public Demo2(Demo demo) {
            demo.super();
        }

        @Override
        public void run() {
            super.run();
        }
    }
```

必须在构造器中传入一个Demo对象，并且还需要调用demo.super()

**Lambda表达式是如何实现的？**

Java8引入了Lambda表达式，一定程度上可以简化我们的代码，使代码结构看起来更优雅。

```
public class Animal {
    public void run(Runnable runnable) {
    }
}
```

JDK8之前使用匿名内部类：

```
run(new Runnable() {
            @Override
            public void run() {
            }
});
```

JDK8之后使用Lambda表达式：

```
 public void run(Runnable runnable) {
    }

    public void test() {
        run(() -> {});
    }
```

Lambda表达式并不是匿名内部类的语法糖，它是基于invokedynamic指令，在运行时使用ASM生成类文件来实现的。



### 序列化 

1. 所有需要网络传输的对象都需要实现序列化接口，通过建议所有的javaBean都实现Serializable接口。
2. 对象的类名、实例变量（包括基本类型，数组，对其他对象的引用）都会被序列化；方法、类变量、transient实例变量都不会被序列化。
3. 如果想让某个变量不被序列化，使用transient修饰。
4. 序列化对象的引用类型成员变量，也必须是可序列化的，否则，会报错。
5. 反序列化时必须有序列化对象的class文件。
6. 当通过文件、网络来读取序列化后的对象时，必须按照实际写入的顺序读取。
7. 单例类序列化，需要重写readResolve()方法；否则会破坏单例原则。
8. 同一对象序列化多次，只有第一次序列化为二进制流，以后都只是保存序列化编号，不会重复序列化。
9. 建议所有可序列化的类加上serialVersionUID 版本号，方便项目升级。

<https://juejin.im/post/5ce3cdc8e51d45777b1a3cdf#heading-9>

### 泛型 

泛型意思是泛指的类型，也叫参数化类型，也就是说把所操作的数据类型被指定为一个参数。

比如我们现在要写一个支持数据增删查询的容器类，我们写了支持String类型的，后面还需要写支持Integer类型的。然后呢？Doubel、Float、各种自定义类型？这样重复代码太多了，而且这些容器的算法都是一致的。我们可以通过泛指一种类型T,来代替我们之前需要的所有类型，把我们需要的类型作为参数传递到容器里面，这样我们算法只需要写一套就可以适应所有的类型。最典型的的例子就是ArrayList了，这个集合我们无论传递什么数据类型，它都能很好的工作。

**Java泛型介绍**

- Java泛型类和Java泛型方法

  ```
  class DataHolder<T>{
      T item;
      
      public void setData(T t) {
      	this.item=t;
      }
      
      public T getData() {
      	return this.item;
      }
      
      /**
       * 泛型方法
       * @param e
       */
      public <E> void PrinterInfo(E e) {
      	System.out.println(e);
      }
  }
  ```

- Java泛型接口

  > 实现泛型接口的时候要将泛型的声明定义也加入到类中

  ```
  //定义一个泛型接口
  public interface Generator<T> {
      public T next();
  }
  ```

- Java泛型擦除

  Java泛型擦除目的是为了和 Java 1.5 之前版本进行兼容。**在编译期间所有的泛型信息都会被擦除掉，**譬如 List<Integer> 在运行时仅用一个 List 来表示，所以我们可以通过反射 add 方法来向 Integer 的泛型列表添加字符串，因为编译后都成了 Object。

  泛型擦除具体来说就是在编译成字节码时**首先进行类型检查，接着进行类型擦除**，编译器会把类型参数替换为它的第一个边界的类型。如果没有指明边界，那么类型参数将被擦除到Object。

  ```
  public class Manipulator<T extends HasF> {
  }  //擦除为HasF类型
  ```

- Java泛型通配符

[泛型擦除原理](https://www.jianshu.com/p/328efeb01940)

## Java集合 

### ArrayList和LinkedList的区别

- ArrayList是实现了基于**动态数组**的数据结构，LinkedList基于**链表**的数据结构。
- 对于随机访问get和set，ArrayList觉得优于LinkedList，因为LinkedList要移动指针。
- 对于新增和删除操作add和remove，LinedList比较占优势，因为ArrayList要移动数据。

### ArrayList源代码 

#### 底层数据结构 

```
private static final int DEFAULT_CAPACITY = 10; // 默认容量
transient Object[] elementData; // Object 数组
private int size; // 大小

```

#### 构造方法 

- 无参数：空数组，容量默认为10

  ```
  public ArrayList() {
          this.elementData = DEFAULTCAPACITY_EMPTY_ELEMENTDATA;
      }
  ```

- 有参数：创建了指定大小的Object[]数组来创建集合

  ```
  public ArrayList(int initialCapacity) {
          if (initialCapacity > 0) {
              this.elementData = new Object[initialCapacity];
          } else if (initialCapacity == 0) {
              this.elementData = EMPTY_ELEMENTDATA;
          } else {
              throw new IllegalArgumentException("Illegal Capacity: "+
                                                 initialCapacity);
          }
      }
  ```

- 有参数：将集合转换为ArrayList

  ```
  ArrayList(Collection<? extends E> c);
  ```

  ​

#### 扩容检查 

数组有个明显的特点就是它的容量是**固定不变**的，一旦数组被创建则容量则无法改变。所以在往数组中添加指定元素前，首先要考虑的就是其容量是否饱和。

若接下来的添加操作会时数组中的元素超过其容量，则必须对其进行**扩容**操作。受限于数组容量固定不变的特性，扩容的本质其实就是创建一个**容量更大**的新数组，再将旧数组的元素**复制**到新数组当中去。

这里以 ArrayList 的 添加操作为例，来看下 ArrayList 内部数组扩容的过程。

这里以 ArrayList 的 添加操作为例，来看下 ArrayList 内部数组扩容的过程。

```
public boolean add(E e) {
	// 关键 -> 添加之前，校验容量
	ensureCapacityInternal(size + 1); 
	
	// 修改 size，并在数组末尾添加指定元素
	elementData[size++] = e;
	return true;
}
```

可以发现 ArrayList 在进行添加操作前，会检验内部数组容量并**选择性地**进行数组扩容。在 ArrayList 中，通过私有方法 **ensureCapacityInternal** 来进行数组的扩容操作。下面来看具体的实现过程：

- 扩容操作的第一步会去判断当前 ArrayList 内部数组是否为空，为空则将最小容量 minCapacity 设置为 10。

```
// 内部数组的默认容量
private static final int DEFAULT_CAPACITY = 10;

// 空的内部数组
private static final Object[] EMPTY_ELEMENTDATA = {};

// 关键 -> minCapacity = seize+1，即表示执行完添加操作后，数组中的元素个数 
private void ensureCapacityInternal(int minCapacity) {
	// 判断内部数组是否为空
	if (elementData == EMPTY_ELEMENTDATA) {
		// 设置数组最小容量（>=10）
		minCapacity = Math.max(DEFAULT_CAPACITY, minCapacity);
	}
	ensureExplicitCapacity(minCapacity);
}
```

- 接着判断添加操作会不会导致内部数组的容量饱和。

```
private void ensureExplicitCapacity(int minCapacity) {
	modCount++;
	
	// 判断结果为 true，则表示接下来的添加操作会导致元素数量超出数组容量
	if (minCapacity - elementData.length > 0){
		// 真正的扩容操作
		grow(minCapacity);
	}
}
```

- 数组容量不足，则进行扩容操作，关键的地方有两个：**扩容公式**、通过从旧数组复制元素到新数组完成扩容操作。

```
private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

private void grow(int minCapacity) {
	
	int oldCapacity = elementData.length;
	
	// 关键-> 容量扩充公式
	int newCapacity = oldCapacity + (oldCapacity >> 1);
	
	// 针对新容量的一系列判断
	if (newCapacity - minCapacity < 0){
		newCapacity = minCapacity;
	}
	if (newCapacity - MAX_ARRAY_SIZE > 0){
		newCapacity = hugeCapacity(minCapacity);
	}
		
	// 关键 -> 复制旧数组元素到新数组中去
	elementData = Arrays.copyOf(elementData, newCapacity);
}

private static int hugeCapacity(int minCapacity) {
	if (minCapacity < 0){
		throw new OutOfMemoryError();
	}
			
	return (minCapacity > MAX_ARRAY_SIZE) ? Integer.MAX_VALUE : MAX_ARRAY_SIZE;
}
```

关于 ArrayList 扩容操作，整个过程如下图：

> 这里应该是4 + 4 * 1.5 = 10

![1650697004965](../img/1650697004965.png)

#### 添加方法 

ArrayList 的添加操作，首先判断要添加的位置是否超出了数组的容量，如果当前已经没有位置进行存放的时候，ArrayList进行自动的扩容，扩容成功后进行进行添加操作。

该操作可分为两种方式：**指定位置（添加到数组指定位置）、不指定位置（添加到数组末尾）**。

- 不指定位置时，则默认将新元素存放到数组的末尾位置。过程相对简单，这里不再分析。
- 指定位置时，即将新元素存在到数组的指定位置。若该位置不是数组末尾（即该位置后面还存有元素），则需要将该位置及之后的元素**后移一位**，以腾出空间来存放新元素。

**指定位置（添加到数组指定位置）**

```
public void add(int index, E element) {

	// 校验添加位置，必须在内部数组的容量范围内
	rangeCheckForAdd(index);
	
	// 扩容检测
	ensureCapacityInternal(size + 1);
	
	// 关键 -> 数组内位置为 index 到 （size-1）的元素往后移动一位，这里仍然采用数组复制实现
	System.arraycopy(elementData, index, elementData, index + 1, size - index);
	
	// 腾出新空间添加新元素
	elementData[index] = element;
	
	// 修改数组内的元素数量
	size++;
}

private void rangeCheckForAdd(int index) {
	if (index > size || index < 0){
		// 抛出异常...
	}
}
```

分析代码，在没有扩容操作的情况下，整个过程如下：

![1650697355028](../img/1650697355028.png)

#### **修改方法**

修改方法，就是替换指定位置上的元素。原理相对简单，这里直接贴出源码。

```
public E set(int index, E element) {
	rangeCheck(index);
	
	E oldValue = elementData(index);
	elementData[index] = element;
	return oldValue;
}
```

#### 删除方法

ArrayList 的删除操作首先进行索引合法性的判断，如果索引不合法抛出IndexOutOfBoundsException异常。然后同样存在两种方式：删除指定位置的元素、删除指定元素。后者相较于前者多了一个查询指定元素所处位置的过程。

删除指定位置的元素时，需判断该位置是否在数组末尾，若是则将该位置的元素置空让 GC 自动回收；若不是，则需要将该位置之后的元素**前移一位**，覆盖掉该元素以到达删除的效果，同时需要清空末尾位置的元素。

```
public E remove(int index) {
	
	rangeCheck(index);
	modCount++;
	
	// 取得该位置的元素
	E oldValue = elementData(index);

	// 判断该位置是否为数组末尾
	int numMoved = size - index - 1;

	// 若是，则将数组中位置为 idnex+1 到 size -1 元素前移一位
	if (numMoved > 0){
		System.arraycopy(elementData, index + 1, elementData, index, numMoved);
	}
		
	// 关键 -> 清空末尾元素让 GC 生效，并修改数组中的元素个数（实现的十分巧妙）
	elementData[--size] = null; 

	return oldValue;
}

E elementData(int index) {
	return (E) elementData[index];
}
```

分析代码，若指定位置不在数组末尾时的删除过程如下：



#### 并发问题 

> 其实就是size++ 这一步的问题。 越界就是两个线程临界值去扩容都满足，于是一个线程size++导致的，另外一个线程就溢出了，还有可能值遭到覆盖，第一个线程刚赋完值,还没来得及size++，第二个线程就在原先的索引上把值给覆盖了，并且在下一个索引为null。

越界

- 列表大小为9，即size=9
- 线程A开始进入add方法，这时它获取到size的值为9，调用ensureCapacityInternal方法进行容量判断。
- 线程B此时也进入add方法，它和获取的size的值也为9，也开始调用ensureCapacityInternal方法。
- 线程A发现需求大小为10，而elementData的大小就为10，可以容纳。于是它不再扩容，返回。
- 线程B也发现需要大小为10，也可以容纳，返回。
- 好了，**问题来了哈**
- 线程A开始进行设置值操作，elementData[size++] = e操作。此时size变为10。
- 线程B也开始进行设置值操作，它尝试设置elementData[10] = e, 而elementData没有进行过扩容，它的下标最大为
- 于是此时会报出一个数组越界的异常`ArrayIndexOutOfBoundsException`。

null

- 列表大小为10，即size=0
- 线程A开始添加元素，值为A。此时它执行第一条操作，将A放在了elementData下标为0的位置上。也就是说，线程挂在了`element[0] = e`上。
- 接着线程B刚好也要开始添加一个值为B的元素，且走到了第一条的操作。此时线程B获取的size的值依然为0，于是它将B也放在了elementData下标为0的位置上。
- **问题来了**，其实上面也是问题，覆盖了。。。
- 线程A将size的值增加为1
- 线程B开始将size的值增加为2
- 当你获取1索引的时候，那不就是null了？

#### 获取方法

```
// 一般获取元素，第一步都要判断索引是否越界
public E get(int index) {
    rangeCheck(index); // 判断给定索引是否越界
    return elementData(index);
}

```

#### Fair-Fast机制

ArrayList也采用了快速失败的机制，**通过记录modCount参数来实现**。在面对并发的修改时，迭代器很快就会完全失败，而不是冒着在将来某个不确定时间发生任意不确定行为的风险。



### LinkedList源代码

LinkedList类是List接口的实现类，它是一个集合，可以根据索引来随机的访问集合中的元素，还实现了Deque接口，它还是一个队列，可以被当成双端队列来使用。虽然LinkedList是一个List集合，但是它的实现方式和ArrayList是完全不同的，ArrayList的底层是通过一个动态的Object[]数组来实现的，而LinkedList的底层是通过链表来实现的，因此它的随机访问速度是比较差的，但是它的删除，插入操作会很快。

- LinkedList是基于双向循环链表实现的，除了可以当做链表来操作外，它还可以当做栈、队列和双端队列来使用。
- LinkedList同样是非线程安全的，只在单线程下适合使用。
- LinkedList实现了Serializable接口，因此它支持序列化，能够通过序列化传输，实现了Cloneable接口，能被克隆。

![1650697965966](../img/1650697965966.png)

JDK1.7时已经将双向循环链表改为了双向链表。

循环链表的优势：快速的找到最后一个结点，并从后往前遍历链表

#### 底层数据结构

```
transient int size = 0;     //LinkedList中存放的元素个数
transient Node<E> first;    //头节点
transient Node<E> last;     //尾节点

private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }
```



#### 构造方法

```
LinkedList() 
LinkedList(Collection<? extends E> c)
```

> LinkedList没有长度的概念，所以不存在容量不足的问题，因此不需要提供初始化大小的构造方法，因此值提供了两个方法，一个是无参构造方法，初始一个LinkedList对象，和将指定的集合元素转化为LinkedList构造方法。

#### getFirst和getLast

> 说白了，getFirst就是获取全局变量的first，getLast就是获取全局变量的last

```
public E getFirst() {
    final Node<E> f = first; // 第一个节点
    if (f == null)
        throw new NoSuchElementException();
    return f.item;
}
public E getLast() {
    final Node<E> l = last; // 最后一个节点
    if (l == null)
        throw new NoSuchElementException();
    return l.item;
}

```

#### 

#### 添加方法

```
public boolean add(E e) {
     linkLast(e);
     return true;
}
void linkLast(E e) {
     final Node<E> l = last;
     final Node<E> newNode = new Node<>(l, e, null);
     last = newNode;
     if (l == null)
         first = newNode;
     else
          l.next = newNode;
     size++;
     modCount++;
}

```

> 添加方法默认是添加到LinkedList的尾部，首先将last指定的节点赋值给l节点，然后新建节点newNode ,此节点的前驱指向l节点，data = e , next = null , 并将新节点赋值给last节点，它成为了最后一个节点，根据当前List是否为空做出相应的操作。若不为空将l的后继指针修改为newNodw。 size +1 , modCount＋１

#### 删除方法

```
public boolean remove(Object o) {
        if (o == null) {
            for (Node<E> x = first; x != null; x = x.next) {
                if (x.item == null) {
                    unlink(x);
                    return true;
                }
            }
        } else {
            for (Node<E> x = first; x != null; x = x.next) {
                if (o.equals(x.item)) {
                    unlink(x);
                    return true;
                }
            }
        }
        return false;
}

```

> 删除方法，先循环遍历列表，找到item == o 的节点，在调用unlink()方法删除

#### 总结

- LinkedList是一个功能很强大的类，可以被当作List集合，双端队列和栈来使用。
- LinkedList底层使用链表来保存集合中的元素，因此随机访问的性能较差，但是插入删除时性能非常的出色。
- LinkedList在1.8版本有添加了一点新的内容，添加了一个static final 修饰的内部类LLSpliterator 并实现了Spliterator ，为了实现并行遍历而新添加的功能，整体的变化并不是很大，感兴趣的可以自己去看一下。

#### List实现类的使用场景

- ArrayList，底层采用数组实现，如果需要遍历集合元素，应该使用随机访问的方式，对于LinkedList集合应该采用迭代器的方式
- 如果需要经常的插入。删除操作可以考虑使用LinkedList集合
- 如果有多个线程需要同时访问List集合中的元素，开发者可以考虑使用Collections将集合包装成线程安全的集合。

#### 补充说明

1. 从源码中很明显可以看出，LinkedList的实现是基于双向循环链表的，且头结点中不存放数据。

2. 注意两个不同的构造方法。无参构造方法直接建立一个仅包含head节点的空链表，包含Collection的构造方法，先调用无参构造方法建立一个空链表，而后将Collection中的数据加入到链表的尾部后面。

3. 在查找和删除某元素时，源码中都划分为该元素为null和不为null两种情况来处理，LinkedList中允许元素为null。

4. LinkedList是基于链表实现的，因此不存在容量不足的问题，所以这里没有扩容的方法。

5. 注意源码中的Entry<E> entry(int index)方法。该方法返回双向链表中指定位置处的节点，而链表中是没有下标索引的，要指定位置出的元素，就要遍历该链表，从源码的实现中，我们看到这里有一个加速动作。源码中先将index与长度size的一半比较，如果index<size/2，就只从位置0往后遍历到位置index处，而如果index>size/2，就只从位置size往前遍历到位置index处。这样可以减少一部分不必要的遍历，从而提高一定的效率（实际上效率还是很低）。

6. 注意链表类对应的数据结构Entry。

7. LinkedList是基于链表实现的，因此插入删除效率高，查找效率低（虽然有一个加速动作）。

8. 要注意源码中还实现了栈和队列的操作方法，因此也可以作为栈、队列和双端队列来使用。

   ​

> [HashMap基础解析](https://juejin.cn/post/6876105622274703368)

### HashMap

![1650715170130](../img/1650715170130.png)

HashMap基于Map接口实现，元素以键值对的方式存储，并且允许使用空key和空value，因为key不允许重复，因此只能有一个键为null,另外HashMap不能保证放入元素的顺序，它是无序的，和放入的顺序并不能相同。HashMap是线程不安全的。

在jdk1.7时，HashMap由数组加链表组成，数组是hashMap的主体，链表则是为了解决Hash冲突而存在的，如果定位到的数组位置不含链表，就是当前entry实体的next指针指向为空，那么对于查找、添加等操作就很快，仅需一次寻址即可。如果定位到的数组位置包含链表，对于添加操作来说，要线性遍历这个链表，然后用key对象的equals方法对比key值，存在则覆盖，不存在则新增，1.7是头插法，1.8是尾插法。在jdk1.8后，添加了红黑树这一结构，当链表长度超过阈值8的时候，将链表转换为红黑树，在性能上进一步得到提升。

![1650715719864](../img/1650715719864.png)

#### 常见参数 

```
static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; //默认参数化大小16
static final float DEFAULT_LOAD_FACTOR = 0.75f; // 默认负载因子0.75
// Map在使用过程中不断的往里面存放数据，当数量达到了16 * 0.75 = 12就需要将当前16的容量进行扩容，
static final int TREEIFY_THRESHOLD = 8;// 成为红黑树的阈值，为什么是8？
static final int UNTREEIFY_THRESHOLD = 6; // 红黑树转链表阈值6
static final int MIN_TREEIFY_CAPACITY = 64;

```

- 为什么容量要是 2 的整数次幂？ 因为获取 key 在数组中对应的下标是通过 key 的哈希值与数组长度 -1 进行与运算，如：tab[i = (n - 1) & hash]

1. n 为 2 的整数次幂，这样 n-1 后之前为 1 的位后面全是 1，这样就能保证 (n-1) & hash 后相应的位数既可能是 0 又可能是 1，这取决于 hash 的值，这样能保证散列的均匀，同时与运算效率高
2. 如果 n 不是 2 的整数次幂，会造成更多的 hash 冲突

> 举个例子：如 16：10000, 16-1=15：1111, 1111 再与 hash 做 & 运算的时候，各个位置的取值取决于 hash，如果不是2的整数次幂，必然会有的0的位，这样再进行 & 操作的时候就为 0了，会造成哈希冲突。

> 注意：HashMap的tableSizeFor方法做了处理，能保证n永远都是2次幂

<https://zhuanlan.zhihu.com/p/90816780>

- 为什么负载因子是0.75？

> 负载因子过低，频繁扩容，扩容会重新哈希，性能下降;负载因子过高，容易浪费容量.

- 为什么红黑树的阈值是8？

> 在 hash 函数设计合理的情况下，根据泊松分布，发生 hash 碰撞 8 次的几率小于百万分之 一，使用大于等于8的时候才进行转换，概率说话。(泊松分布)

- 为什么退化链表的阈值6？

> 6是因为如果 hash 碰撞次数在 8 附近徘徊，会一直发生链表和红黑树的转化，为了预防这种情况的发生。

#### 构造方法

HashMap提供了四个构造方法，构造方法中 ，依靠第三个方法来执行的，但是前三个方法都没有进行数组的初始化操作，即使调用了构造方法，此时存放HaspMap中数组元素的table表长度依旧为0 。在第四个构造方法中调用了inflateTable()方法完成了table的初始化操作，并将m中的元素添加到HashMap中。

```
HashMap()    //无参构造方法
HashMap(int initialCapacity)  //指定初始容量的构造方法 
HashMap(int initialCapacity, float loadFactor) //指定初始容量和负载因子
HashMap(Map<? extends K,? extends V> m)  //指定集合，转化为HashMap
```

#### hash 

```
static final int hash(Object key) {
    int h;
    return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
}

```

hash 函数是先拿到通过 key 的 hashcode，**是 32 位的 int 值**，然后让 **hashcode 的高 16 位和低 16 位进行异或操作**。这个也叫扰动函数，这么设计有二点原因：

- **一定要尽可能降低 hash 碰撞，越分散越好**；
- 算法一定要尽可能高效，因为这是高频操作, 因此采用位运算；

#### put 

> jdk1.8

```
public V put(K key, V value) {
    //调用putVal()方法完成
    return putVal(hash(key), key, value, false, true);
}

final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
               boolean evict) {
    Node<K,V>[] tab; Node<K,V> p; int n, i;
    //判断table是否初始化，否则初始化操作
    if ((tab = table) == null || (n = tab.length) == 0)
        n = (tab = resize()).length;
    //计算存储的索引位置，如果没有元素，直接赋值
    if ((p = tab[i = (n - 1) & hash]) == null)
        tab[i] = newNode(hash, key, value, null);
    else {
        Node<K,V> e; K k;
        //节点若已经存在，执行赋值操作
        if (p.hash == hash &&
            ((k = p.key) == key || (key != null && key.equals(k))))
            e = p;
        //判断链表是否是红黑树
        else if (p instanceof TreeNode)
            //红黑树对象操作
            e = ((TreeNode<K,V>)p).putTreeVal(this, tab, hash, key, value);
        else {
            //为链表，
            for (int binCount = 0; ; ++binCount) {
                if ((e = p.next) == null) {
                    p.next = newNode(hash, key, value, null);
                    //链表长度8，将链表转化为红黑树存储
                    if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                        treeifyBin(tab, hash);
                    break;
                }
                //key存在，直接覆盖
                if (e.hash == hash &&
                    ((k = e.key) == key || (key != null && key.equals(k))))
                    break;
                p = e;
            }
        }
        if (e != null) { // existing mapping for key
            V oldValue = e.value;
            if (!onlyIfAbsent || oldValue == null)
                e.value = value;
            afterNodeAccess(e);
            return oldValue;
        }
    }
    //记录修改次数
    ++modCount;
    //判断是否需要扩容
    if (++size > threshold)
        resize();
    //空操作
    afterNodeInsertion(evict);
    return null;
}
```



![hashmap-put-1.8](https://gitee.com/dreamcater/blog-img/raw/master/uPic/hashmap-put-1.8-WLBoNy.png)

- 判断数组是否为空，为空进行初始化;（初始化）
- 不为空，计算 k 的 hash 值，通过通过hash值计算应当存放在数组中的下标 index;
- 查看 table[index] 是否存在数据，没有数据就构造一个 Node 节点存放在 table[index] 中；（查看数组中是否哈希冲突）
- 存在数据，说明发生了 hash 冲突(存在二个节点 key 的 hash 值一样), 继续判断 key 是否相等，相等，用新的 value 替换原数据(onlyIfAbsent 为 false)；（冲突，判断key是否相等，相等则替换）
- 如果不相等，判断当前节点类型是不是树型节点，如果是树型节点，创造树型节点插入红黑树中；（判断是否红黑树）
- 如果不是树型节点，创建普通 Node 加入链表中；判断链表长度是否大于 8， 大于的话链表转换为红黑树；（判断是否转成红黑树）
- 插入完成之后判断当前节点数是否大于阈值，如果大于开始扩容为原数组的二倍。（扩容）

#### get 

```
 public V get(Object key) {
        Node<K,V> e;
        return (e = getNode(hash(key), key)) == null ? null : e.value;
    }
    
 final Node<K,V> getNode(int hash, Object key) {
        Node<K,V>[] tab; Node<K,V> first, e; int n; K k;
        if ((tab = table) != null && (n = tab.length) > 0 &&
            (first = tab[(n - 1) & hash]) != null) {
            if (first.hash == hash && // always check first node
                ((k = first.key) == key || (key != null && key.equals(k))))
                return first;
            if ((e = first.next) != null) {
                if (first instanceof TreeNode)
                    return ((TreeNode<K,V>)first).getTreeNode(hash, key);
                do {
                    if (e.hash == hash &&
                        ((k = e.key) == key || (key != null && key.equals(k))))
                        return e;
                } while ((e = e.next) != null);
            }
        }
        return null;
    }   
```



> get比较简单，定位桶，逻辑来了。

1. 判断：是否为空，为空，返回null
2. 不为空，判断第一个位置是否为查询key，是，返回value
3. 不是，下一个节点继续判断是否为红黑树，是，按树查找
4. 不是，按链表查找

#### 扩容 

> 先说1.7吧

```
for (HashMapEntry<K, V> e : table) {
    // 如果这个数组位置上有元素且存在哈希冲突的链表结构则继续遍历链表
    while (null != e) {
        //取当前数组索引位上单向链表的下一个元素
        HashMapEntry<K, V> next = e.next;
        //重新依据hash值计算元素在扩容后数组中的索引位置
        int i = indexFor(e.hash, newCapacity);
        e.next = newTable[i]; // 这一步和下一步就是头插法了，并且这两步出现线程不安全死循环问题
        newTable[i] = e;
        e = next; // 遍历链表
    }
}

```

> 可以适当画图讲， 最好！

> 1.8 HashMap的扩容使用的是2次幂的扩展(指长度扩为原来2倍)，所以，元素的位置要么是在原位置，要么是在原位置再移动2次幂的位置。也就是说省略了重新计算hash值的时间，而且新增的1位是0还是1机会是随机的，因此resize的过程，均匀的把之前的冲突的节点分散到新的bucket了。如果在新表的数组索引位置相同，则链表元素不会倒置。

#### 1.8的优化 

- 数组+链表改成了**数组+链表或红黑树**；
- 在1.8之前，新插入的元素都是放在了链表的头部位置，但是这种操作在高并发的环境下容易导致死锁，所以1.8之后，新插入的元素都放在了链表的尾部。
- 扩容的时候 1.7 需要对原数组中的元素进行**重新 hash 定位在新数组的位置**，1.8 采用更简单的判断逻辑，**位置不变或索引+旧容量大小**；因为JDK1.7是用单链表进行的纵向延伸，当采用头插法时会容易出现逆序且环形链表死循环问题。但是在JDK1.8之后是因为加入了红黑树使用尾插法，能够避免出现逆序且链表死循环的问题。
- 在插入时，**1.7 先判断是否需要扩容，再插入，1.8 先进行插入，插入完成再判断是否需要扩容**；

#### 并发问题 

- HashMap扩容的时候会调用resize()方法，就是这里的并发操作容易在一个桶上形成环形链表

- 这样当获取一个不存在的key时，计算出的index正好是环形链表的下标就会出现死循环。

- 但是1.7的头插法造成的问题，1.8改变了插入顺序，就解决了这个问题，但是为了内存可见性等安全性，还是需要ConCurrentHashMap

  **解决HashMap线程安全问题**

- Collections.synchronizedMap 创建线程安全的map集合

  在SynchronizedMap内部维护了一个普通对象Map，还有个对象排斥锁mutex，再操作Map的时候，就会对方法上锁。

  ![1650887030803](../img/1650887030803.png)

  ![1650887065864](../img/1650887065864.png)

- HashTable 

- ConcurrentHashMap 

### Hashtable

Hashtable相比HashMap是线程安全的，适合在多线程的情况下使用，但是效率不太乐观。

因为它是直接在操作方法上加 synchronized 关键字，锁住整个Entry数组，粒度比较大。

![1650887585385](../img/1650887585385.png)

**HashMap和Hashtable的区别：**

- HashMap性能高于Hashtable，但不是线程安全的。Hashtable是解决HashMap线程安全的一种解决方案。


- Hashtable是不允许键或值为null的，HashMap的键和值则都可以为null

  从代码层面上来看是因为Hashtable在外面put空值的时候会直接抛空指针异常，但是HashMap却做了特殊处理。

  从底层上来看，

- **实现方式不同**：Hashtable 继承了 Dictionary类，而 HashMap 继承的是 AbstractMap 类。Dictionary 是 JDK 1.0 添加的，貌似没人用过这个，我也没用过。

- **初始化容量不同**：HashMap 的初始容量为：16，Hashtable 初始容量为：11，两者的负载因子默认都是：0.75。

- **扩容机制不同**：当现有容量大于总容量 * 负载因子时，HashMap 扩容规则为当前容量翻倍，Hashtable 扩容规则为当前容量翻倍 + 1。

- **迭代器不同**：HashMap 中的 Iterator 迭代器是 fail-fast 的，而 Hashtable 的 Enumerator 不是 fail-fast 的。所以，当其他线程改变了HashMap 的结构，如：增加、删除元素，将会抛出ConcurrentModificationException 异常，而 Hashtable 则不会。

**快速失败**

是java集合中的一种机制，使用迭代器进行遍历集合时，除了通过迭代器自身的 `remove()` 方法之外，对集合进行任何其他方式的修改，则会抛出 ConcurrentModificationException 异常。

在 `java.util` 包下的集合类都采用的是快速失败机制，不能在多线程下发生并发修改（迭代过程中被修改）。

**原理**

为了保证不被修改，迭代器内部维护了一个 modCount 变量 ，当集合内容发生修改，就会改变 modCount 的值。每当迭代器遍历下一个元素之前，都会检查 modCount 的值是否等于 expectedmodCount 的值，当检测到不相等时，抛出 ConcurrentModificationException 异常，反之继续遍历。

> 注意: 不能依赖于这个异常是否抛出而进行并发操作的编程，这个异常只建议用于检测并发修改的bug。这里异常的抛出条件是检测到 `modCount != expectedmodCount` 这个条件。如果集合发生变化时修 modCount 的值刚好等于 expectedmodCount 的值，则异常不会抛出。

**优缺点**

- 单线程下效率相对较高。
- 多线程环境下，线程不安全。

**安全失败**

采用安全失败机制的集合容器，使用迭代器进行遍历时不是直接在集合内容上访问的，而是将原有集合内容进行拷贝，在拷贝的集合上进行遍历。

**原理**

迭代器在遍历时访问的是拷贝的集合，所以在遍历过程中对原集合所作的修改并不能被迭代器检测到，所以不会触发 ConcurrentModificationException 异常。

**优缺点**

- 由于对集合进行了拷贝，避免了 ConcurrentModificationException 异常，但拷贝时产生大量的无效对象，开销大。
- 无法保证读取到的数据是原集合中最新的数据，即迭代器进行遍历的是拷贝的集合，在遍历期间原集合发生的修改，迭代器是检测不到的。

### ConcurrentHashMap

**继承关系**

```
public class ConcurrentHashMap<K,V> 
    extends AbstractMap<K,V>
        implements ConcurrentMap<K,V>, Serializable
```

ConcurrentHashMap 继承了AbstractMap ,并且实现了ConcurrentMap接口。

**与hashMap对比：**

- 相同点：都集成了AbstractMap接口
- 不同点：HashMap实现了Map接口，ConcurrentHashMap实现了ConcurrentMap接口，而ConcurrentMap继承了Map接口，使用default关键字定义了一些方法 。

从继承关系上看ConcurrentHashMap与HashMap并没有太大的区别。

#### 基本属性

```
private static final int MAXIMUM_CAPACITY = 1 << 30; //最大容量2的30次方
private static final int DEFAULT_CAPACITY = 16; //默认容量  1<<4

private static final float LOAD_FACTOR = 0.75f;  //负载因子
static final int TREEIFY_THRESHOLD = 8;  //链表转为红黑树
static final int UNTREEIFY_THRESHOLD = 6;  //树转列表
static final int MIN_TREEIFY_CAPACITY = 64; //
private static final int MIN_TRANSFER_STRIDE = 16;
private static int RESIZE_STAMP_BITS = 16;
private static final int MAX_RESIZERS = (1 << (32 - RESIZE_STAMP_BITS)) - 1;
private static final int RESIZE_STAMP_SHIFT = 32 - RESIZE_STAMP_BITS;
static final int MOVED     = -1; // forwarding nodes 的hash值
static final int TREEBIN   = -2; // roots of trees 的hash值
static final int RESERVED  = -3; // transient reservations 的hash值
static final int HASH_BITS = 0x7fffffff; // usable bits of normal node hash
static final int NCPU = Runtime.getRuntime().availableProcessors(); //可用处理器数量
```

```
//表初始化或者扩容的一个控制标识位
//负数代表正在进行初始化或者扩容的操作
// -1 代表初始化
// -N 代表有n-1个线程在进行扩容操作
//正数或者0表示没有进行初始化操作，这个数值表示初始化或者下一次要扩容的大小。

//transient 修饰的属性不会被序列化，volatile保证可见性
private transient volatile int sizeCtl;
```

**构造方法**

```
 //无参构造方法，没有进行任何操作
 public ConcurrentHashMap() {}
 //指定初始化大小构造方法，判断参数的合法性，并创建了计算初始化的大小
 public ConcurrentHashMap(int initialCapacity) {}
 //将指定的集合转化为ConcurrentHashMap
 public ConcurrentHashMap(Map<? extends K, ? extends V> m) {}
 //指定初始化大小和负载因子的构造方法
 public ConcurrentHashMap(int initialCapacity, float loadFactor) {
        this(initialCapacity, loadFactor, 1);
    }
 //指定初始化大小，负载因子和concurrentLevel并发更新线程的数量，也可以理解为segment的个数
 public ConcurrentHashMap(int initialCapacity,float loadFactor, int concurrencyLevel) {}
```

> ConcurrentHashMap的构造方法并没做太多的工作，主要是进行了参数的合法性校验，和初始值大小的转换。这个方法 tableSizeFor()说明一下， 主要的功能就是将指定的初始化参数转换为2的幂次方形式， 如果初始化参数为9 ，转换后初始大小为16 。

#### 内部数据结构

**node**

首当其冲，因为它是ConcurrentHashMap的核心，它包装了key-value的键值对，所有插入的数据都包装在这里面，与HashMap很相似，但是有一些差别：

```
static class Node<K,V> implements Map.Entry<K,V> {
     final int hash;
     final K key;
     volatile V val;
     volatile Node<K,V> next;

     Node(int hash, K key, V val, Node<K,V> next) {
         this.hash = hash;
         this.key = key;
         this.val = val;
         this.next = next;
     }
}
```

value 和 next使用了volatile修饰，保证了线程之间的可见性。也不允许调用setValue()方法直接改变Node的值。并增加了find()方法辅助map.get()方法。

**TreeNode**

树节点类，另外一个核心的数据结构。当链表长度过长的时候，会转换为TreeNode。但是与HashMap不相同的是，它并不是直接转换为红黑树，而是把这些结点包装成TreeNode放在TreeBin对象中，由TreeBin完成对红黑树的包装。而且TreeNode在ConcurrentHashMap集成自Node类，而并非HashMap中的集成自LinkedHashMap.Entry类，也就是说TreeNode带有next指针，这样做的目的是方便基于TreeBin的访问。

**TreeBin**

这个类并不负责包装用户的key、value信息，而是包装的很多TreeNode节点。它代替了TreeNode的根节点，也就是说在实际的ConcurrentHashMap“数组”中，存放的是TreeBin对象，而不是TreeNode对象，这是与HashMap的区别。另外这个类还带有了读写锁。

**ForwardingNode**

一个用于连接两个table的节点类。它包含一个nextTable指针，用于指向下一张表。而且这个节点的key value next指针全部为null，它的hash值为-1. 这里面定义的find的方法是从nextTable里进行查询节点，而不是以自身为头节点进行查找



#### 1.7 

##### initTable

初始化方法是很重要的一个方法，因为在ConcurrentHashMap的构造方法中只是简单的进行了一些参数校验和参数转换的操作。整个Map的初始化是在插入元素的时候触发的。这一点在下面的put方法中会进行说明。

```
//执行初始化操作，单线程操作
private final Node<K,V>[] initTable() {
        Node<K,V>[] tab; int sc;
        while ((tab = table) == null || tab.length == 0) {
            if ((sc = sizeCtl) < 0)
                //sizeCtl < 0 表示有线程正在进行初始化操作，从运行状态变为就绪状态。
                Thread.yield(); // lost initialization race; just spin
                
            //设置SIZECTL的值为-1，阻塞其他线程的操作
            //该方法有四个参数
            //第一个参数：需要改变的对象
            //第二个参数：偏移量
            //第三个参数：期待的值
            //第四个参数：更新后的值
            else if (U.compareAndSwapInt(this, SIZECTL, sc, -1)) {
                try {
                    //再次检查是否有线程进行了初始化操作
                    if ((tab = table) == null || tab.length == 0) {
                        int n = (sc > 0) ? sc : DEFAULT_CAPACITY;
                        @SuppressWarnings("unchecked")
                        //初始化Node对象数组
                        Node<K,V>[] nt = (Node<K,V>[])new Node<?,?>[n];
                        table = tab = nt;
                        //sc的值设置为n的0.75倍
                        sc = n - (n >>> 2);  //相当于n*0.75
                    }
                } finally {
                    sizeCtl = sc;  //更改sizeCtl的值
                }
                break; //中断循坏返回
            }
        }
    return tab; //返回初始化的值
}
```

##### 扩容方法

当ConcurrentHashMap 容量不足的时候，需要对table进行扩容，这个方法是支持多个线程并发扩容的，我们所说的扩容，从本质上来说，无非是从一个数组到另外一个数组的拷贝。

扩容方法分为两个部分：

- 创建扩容后的新数组，容量变为原来的两倍 ，新数组的创建时单线程完成
- 将原来的数组元素复制到新的数组中，这个是多线程操作。

##### segment 

jdk1.7中ConcurrentHashMap由 Segment 数组、HashEntry 组成，和 HashMap 一样，仍然是**数组加链表**。其中 Segment 继承于 `ReentrantLock`。HashEntry跟HashMap差不多，不同点是它使用了volatile去修饰他的数据value还有下一个节点next，保证了获取时的可见性。

Jdk1.7使用分段锁，降低了锁粒度，让并发度大大提高。不会像HashTable那样不管是put还是get操作都需要做同步处理，理论上 ConcurrentHashMap 支持 CurrencyLevel (Segment 数组数量)的线程并发。每当一个线程占用锁访问一个 Segment 时，不会影响到其他的 Segment。

![1650890498021](../img/1650890498021.png)



##### put

- 虽然HashEntry中的value是用volatile关键字修饰的，但是并不能保证并发的原子性，所以put操作仍然需要加锁处理。

- 首先第一步的时候会尝试获取锁，如果获取失败肯定就是其他线程存在竞争，则利用 `scanAndLockForPut()` 自旋获取锁。


  - 如果重试的次数达到了`MAX_SCAN_RETRIES` 则改为阻塞锁获取，保证能获取成功。

总的来说：

- 将当前的Segment中的table通过key的hashcode定位到HashEntry
- 遍历该HashEntry，如果不为空则判断传入的key和当前遍历的key是否相等，相等则覆盖旧的value
- 不为空则需要新建一个HashEntry并加入到Segment中，同时会先判断是否需要扩容
- 最后会解除在1中所获取当前Segment的锁。

##### get

- 只需要将 Key 通过 Hash 之后定位到具体的 Segment ，再通过一次 Hash 定位到具体的元素上。
- 由于 HashEntry 中的 value 属性是用 volatile 关键词修饰的，保证了内存可见性，所以每次获取时都是最新值。
- ConcurrentHashMap 的 get 方法是非常高效的，**因为整个过程都不需要加锁。**

##### size

在 JDK1.7 中，第一种方案他会使用不加锁的模式去尝试多次计算 ConcurrentHashMap 的 size，最多三次，比较前后两次计算的结果，结果一致就认为当前没有元素加入，计算的结果是准确的。 第二种方案是如果第一种方案不符合，他就会给每个 Segment 加上锁，然后计算 ConcurrentHashMap 的 size 返回

#### 1.8 

1.7 **查询遍历链表效率太低**。其中抛弃了原有的 Segment 分段锁，而采用了 `CAS + synchronized` 来保证并发安全性。跟HashMap很像，也把之前的HashEntry改成了Node，但是作用不变，把值和next采用了volatile去修饰，保证了可见性，并且也引入了红黑树，在链表大于一定值的时候会转换（默认是8）。

##### put 

- 根据key计算出hashcode
- 判断是否需要进行初始化
- 然后为当前 key 定位出 Node，如果为空表示当前位置可以写入数据，利用 CAS 尝试写入，失败则自旋保证成功。
- 如果当前位置的hash值为-1，说明当前f是ForwardingNode节点，意味有其它线程正在扩容，则一起进行扩容操作。
- 如果都不满足，则利用`synchronized`锁写入数据
- 如果数量大于TREEIFY_THRESHOLD 则要转换为红黑树。

![](../img/Con8get.awebp)

##### get

- 根据计算出来的 hashcode 寻址，如果就在桶上那么直接返回值。
- 如果是红黑树那就按照树的方式获取值。
- 就不满足那就按照链表的方式遍历获取值。

![](../img/get.awebp)

##### size 

ConcurrentHashMap 提供了 baseCount、counterCells 两个辅助变量和一个 CounterCell 辅助内部类。sumCount() 就是迭代 counterCells 来统计 sum 的过程。 put 操作时，肯定会影响 size()，在 put() 方法最后会调用 addCount() 方法。

在addCount()方法中：

- 如果 counterCells == null, 则对 baseCount 做 CAS 自增操作。
- 如果并发导致 baseCount CAS 失败了使用 counterCells。
- 如果counterCells CAS 失败了，在 fullAddCount 方法中，会继续死循环操作，直到成功。
- CounterCell使用了 @sun.misc.Contended 标记的类

> 缓存系统中是以缓存行（cache line）为单位存储的。缓存行是2的整数幂个连续字节，一般为32-256个字节。最常见的缓存行大小是64个字节。当多线程修改互相独立的变量时，如果这些变量共享同一个缓存行，就会无意中影响彼此的性能，这就是伪共享。

实际上：

- JDK1.8 size 是通过对 baseCount 和 counterCell 进行 CAS 计算，最终通过 baseCount 和 遍历 CounterCell 数组得出 size。
- JDK 8 推荐使用mappingCount 方法，因为这个方法的返回值是 long 类型，不会因为 size 方法是 int 类型限制最大值。

JDK7中的ConcurrentHashmap主要使用Segment来实现减小锁粒度，把HashMap分割成若干个Segment，在put的时候需要锁住Segment，get时候不加锁，使用volatile来保证可见性，当要统计全局时（比如size），首先会尝试多次计算modcount来确定，这几次尝试中，是否有其他线程进行了修改操作，如果没有，则直接返回size。如果有，则需要依次锁住所有的Segment来计算。

jdk7中ConcurrentHashmap中，当长度过长，碰撞会很频繁，链表的增改删查操作都会消耗很长的时间，影响性能，所以jdk8 中完全重写了concurrentHashmap,代码量从原来的1000多行变成了 6000多 行，实现上也和原来的分段式存储有很大的区别。

主要设计上的变化有以下几点:

1. 不采用segment而采用node，锁住node来实现减小锁粒度。
2. 设计了MOVED状态 当resize的中过程中 线程2还在put数据，线程2会帮助resize。
3. 使用3个CAS操作来确保node的一些操作的原子性，这种方式代替了锁。
4. sizeCtl的不同值来代表不同含义，起到了控制的作用。





<https://zhuanlan.zhihu.com/p/40627259>

### HashSet 

HashSet中不允许有重复元素，这是因为HashSet是基于HashMap实现的，HashSet中的元素都存放在HashMap的key上面，而value中的值都是统一的一个`private static final Object PRESENT = new Object();`。 HashSet跟HashMap一样，都是一个存放链表的数组。

#### **基本属性**

```
private transient HashMap<E,Object> map;  //map集合，HashSet存放元素的容器
private static final Object PRESENT = new Object(); //map，中键对应的value值
```

**构造方法**

```
//无参构造方法，完成map的创建
public HashSet() {
    map = new HashMap<>();
}
//指定集合转化为HashSet, 完成map的创建
public HashSet(Collection<? extends E> c) {
   map = new HashMap<>(Math.max((int) (c.size()/.75f) + 1, 16));
   addAll(c);
}
//指定初始化大小，和负载因子
public HashSet(int initialCapacity, float loadFactor) {
    map = new HashMap<>(initialCapacity, loadFactor);
}
//指定初始化大小
public HashSet(int initialCapacity) {
    map = new HashMap<>(initialCapacity);
}
//指定初始化大小和负载因子，dummy 无实际意义
HashSet(int initialCapacity, float loadFactor, boolean dummy) {
    map = new LinkedHashMap<>(initialCapacity, loadFactor);
}
```

**Add**

PRESENT为HashSet类中定义的一个使用static final 修饰的常量，并无实际的意义，HashSet的add方法调用HashMap的put()方法实现，如果键已经存在，map.put()放回的是旧值，添加失败，如果添加成功map.put()方法返回的是null ,HashSet.add()方法返回true,要添加的元素可作为map中的key 。

```
public boolean add(E e) {
    return map.put(e, PRESENT)==null;
}
```

**remove**

删除方法，调用map.remove()方法实现，map.remove()能找到指定的key,则返回key对应的value,对于Hashset而言，它所有的key对应的值都是PRESENT。

```
public boolean remove(Object o) {
    return map.remove(o)==PRESENT;
}
```













### 手写lru

> 双向链表+HashMap，Java中的LinkedHashMap就实现了该算法。

#### get 

```
public int get(int key) {
    if (map.containsKey(key)) {
        Node n = map.get(key); // 获取内存中存在的值，比如A
        remove(n); //使用链表的方法，移除该节点
        setHead(n); //依然使用链表的方法，将该节点放入头部
        return n.value;
    } 
    return -1;
}

```

- 由于当一个节点通过key来访问到这个节点，那么这个节点就是刚被访问了，
- 就把这个节点删除掉，然后放到队列头，这样队列的头部都是最近访问的，
- 队列尾部是最近没有被访问的。

#### set

```
public void set(int key, int value) {
    if (map.containsKey(key)) {
        Node old = map.get(key);
        old.value = value;
        remove(old); // 移除旧节点
        setHead(old); // 放到队头
    } else {
        Node created = new Node(key, value);
        if (map.size() >= capacity) {
            map.remove(end.key); // clear该key
            remove(end); //链表也是依次
            setHead(created); // 将created放入队头
        } else {
            setHead(created); // 如果没满，直接放入队头
        }
        map.put(key,created);
    }
}

```

## 多线程

### 线程与进程的区别 

![1649901833535](../img/1649901833535.png)

进程

**进程是程序的一次执行过程，是系统进行资源分配和调度的基本单位**。系统运行一个程序即是一个进程从创建，运行到消亡的过程。

比如：当我们启动 main 函数时其实就是启动了一个 JVM 的进程，而 main 函数所在的线程就是这个进程中的一个线程，也称主线程。

线程

- **线程是一个比进程更小的执行单位**
- 一个进程在其执行的过程中可以产生**多个线程**
- 与进程不同的是同类的多个线程共享进程的**堆**和**方法区**资源，但每个线程有自己的**程序计数器**、**虚拟机栈**和**本地方法栈**，所以系统在产生一个线程，或是在各个线程之间作切换工作时，负担要比进程小得多，也正因为如此，线程也被称为轻量级进程

协程：

**协程是一种比线程更加轻量级的一种函数**。正如一个进程可以拥有多个线程一样，一个线程可以拥有多个协程，**多个协程的运行是串行的**。协程不是被操作系统内核所管理的，而是完全由程序所控制的，即在用户态执行，切换内存被保存在用自己的变量(用户栈或堆)中。 这样带来的好处是：性能有大幅度的提升，因为不会像线程切换那样消耗资源。

### 并行与并发

- 并发：**同一时间段**，多个任务都在执行。

  比如我们用单cpu的计算机，一边玩游戏，一边听歌。在单核cpu中，同一时间只能干一件事。这时候Windows这种操作系统会把CPU的时间划分成长短基本相同的时间区间，即”时间片”，通过操作系统的管理，把这些时间片依次轮流地分配给各个应用使用。实际上，在操作系统中，CPU是在游戏进程和音乐播放器进程之间来回切换执行的。

  操作系统时间片的使用是有规则的：某个作业在时间片结束之前,整个任务还没有完成，那么该作业就被暂停下来，放弃CPU，等待下一轮循环再继续做。此时CPU又分配给另一个作业去使用。

  现实生活中的例子就是在吃午饭，吃米粉，蔬菜，肉这三件事就是并发执行的，在吃不同的东西之间来回切换。

  那如果2个人一起吃午饭，我们两个人之间的吃放就是并行的

- 并行：**单位时间内**，当系统有一个以上CPU时，当一个CPU执行一个进程时，另一个CPU可以执行另一个进程，两个进程互不抢占CPU资源，可以同时进行。



### 并发编程缺点

#### 频繁的上下文切换

多线程编程中一般**线程的个数都大于 CPU 核心的个数**，而一个 CPU 核心在任意时刻只能被一个线程使用，为了让这些线程都能得到有效执行，CPU 采取的策略是为每个线程分配**时间片并轮转**的形式。当一个线程的时间片用完的时候就会重新处于就绪状态让给其他线程使用，这个过程就属于一次上下文切换。

每一次上下文切换中任务从保存到再加载非常消耗性能，**过于频繁反而无法发挥出多线程编程的优势**。**通常减少上下文切换可以采用无锁并发编程，CAS算法，使用最少的线程和使用协程**。



#### 线程安全

> 我的理解是：多个线程交替执行，本身是没有问题的，但是如果访问共享资源，结果可能会出现问题，于是就出现了线程不安全的问题，线程安全就是多线程在运行期间不会产生不符合常规的数据。

> 举个例子：我们这边有20个线程，每个线程分别执行100次数量++；那么最终全部执行完毕的结果理论上是2000,但实际运行却达不到。

```
public class Test {
    public volatile int inc = 0;
 
    public void increase() {
        inc++;
    }
 
    public static void main(String[] args) {
        final Test test = new Test();
        for(int i=0;i<20;i++){
            new Thread(){
                public void run() {
                    for(int j=0;j<100;j++)
                        test.increase();
                };
            }.start();
        }
 
        while(Thread.activeCount()>1)  //保证前面的线程都执行完
            Thread.yield();
        System.out.println(test.inc);
    }

```

这是因为数量++是个复合操作，包括读取数量的值，对其自增，然后写回主存，如果A线程读取到了数量的值然后被阻塞了，线程B此时也读到了数量的值并自增后写回主存。然后轮到线程A执行的时候，A进程工作内存并不是最新的值，会造成线程B的自增被覆盖，就导致总数量达不到预期，这就是一种线程不安全的情况。





### 线程周期

![img](https://www.pdai.tech/_images/pics/ace830df-9919-48ca-91b5-60b193f593d2.png)

- 线程创建之后它将处于`New`（新建）状态，调用 `start()` 方法后开始运行，线程这时候处于 `READY`（可运行） 状态。可运行状态的线程获得了 CPU 时间片（timeslice）后就处于 `RUNNING`（运行） 状态。
- 当线程执行 `wait()`方法之后，线程进入 `WAITING`（等待）状态。进入等待状态的线程需要依靠其他线程的通知才能够返回到运行状态，而 `TIME_WAITING`(超时等待) 状态相当于在等待状态的基础上增加了超时限制，比如通过 `sleep（long millis）`方法或 `wait（long millis）`方法可以将 Java 线程置于 `TIMED WAITING` 状态。当超时时间到达后 Java 线程将会返回到 RUNNABLE 状态。
- 当线程调用同步方法时，在没有获取到锁的情况下，线程将会进入到 `BLOCKED`（阻塞）状态。
- 线程在执行 Runnable 的`run()`方法之后将会进入到 `TERMINATED`（终止） 状态。

#### 直接调用Thread的run方法不行吗？

**start**

通过该方法启动线程的同时**也创建了一个线程，真正实现了多线程**。**无需等待run()方法中的代码执行完毕，就可以接着执行下面的代码**。此时start()的这个线程处于**就绪状态**，当得到CPU的时间片后就会执行其中的run()方法。这个run()方法包含了要执行的这个线程的内容，run()方法运行结束，此线程也就终止了。

从源码当中可以看到： 当一个线程启动的时候，它的状态（threadStatus）被设置为0，如果不为0，则抛出`IllegalThreadStateException`异常。正常的话，将该**线程加入线程组**，最后尝试调用start0方法，**而start0方法是私有的native方法**（Native Method是一个java调用非java代码的接口）。

**run**

通过run方法启动线程其实就是调用一个类中的方法，**当作普通的方法的方式调用**。并没有创建一个线程，程序中依旧只有一个主线程，必须等到run()方法里面的代码执行完毕，才会继续执行下面的代码，这样就没有达到写线程的目的。

### wait/notify 和 sleep 方法的异同？

相同点：

1. 它们都可以让线程阻塞。
2. 它们都可以响应 interrupt 中断：在等待的过程中如果收到中断信号，都可以进行响应，并抛出 InterruptedException 异常。

不同点：

1. wait 方法必须在 synchronized 保护的代码中使用，而 sleep 方法并没有这个要求。
2. 在同步代码中执行 sleep 方法时，并不会释放 monitor 锁，但执行 wait 方法时会主动释放 monitor 锁。
3. sleep 方法中会要求必须定义一个时间，时间到期后会主动恢复，而对于没有参数的 wait 方法而言，意味着永久等待，直到被中断或被唤醒才能恢复，它并不会主动恢复。
4. wait/notify 是 Object 类的方法，而 sleep 是 Thread 类的方法。

### 线程通信

> https://juejin.cn/post/6969122698563682311

**锁机制：包括互斥锁、条件变量、读写锁**

互斥锁提供了以排他方式防止数据结构被并发修改的方法。读写锁允许多个线程同时读共享数据，而对写操作是互斥的。条件变量可以以原子的方式阻塞进程，直到某个特定条件为真为止。对条件的测试是在互斥锁的保护下进行的。条件变量始终与互斥锁一起使用。

**wait/notify 等待**

等待通知机制是基于wait和notify方法来实现的，wait()方法可以让当前线程释放对象锁并进入阻塞状态。notify()方法用于唤醒一个正在等待相应对象锁的线程，使其进入就绪队列，以便在当前线程释放锁后竞争锁，进而得到CPU的执行。

> 每个锁对象都有两个队列，一个是就绪队列，一个是阻塞队列。就绪队列存储了已就绪（将要竞争锁）的线程，阻塞队列存储了被阻塞的线程。当一个阻塞线程被唤醒后，才会进入就绪队列，进而等待CPU的调度。反之，当一个线程被wait后，就会进入阻塞队列，等待被唤醒。

为什么要必须获取锁？因为调用wait方法时，必须要先释放锁，如果没有持有锁将会抛出异常。

wait: 使当前线程放弃同步锁并进入等待，直到其他线程进入此同步锁，并调用notify()方法，或notifyAll()方法唤醒该线程为止。
notify(): 唤醒此同步锁上等待的第一个调用wait()方法的线程。
notifyAll(): 唤醒同步锁上调用wait()方法的所有线程。

**Volatile 内存共享**

volatile有两大特性，一是可见性，二是有序性，禁止指令重排序，其中可见性就是可以让线程之间进行通信。

**信号量机制(Semaphore)**

包括无名线程信号量和命名线程信号量。

**信号机制(Signal)**

类似进程间的信号处理。

线程间的通信目的主要是用于线程同步，所以线程没有像进程通信中的用于数据交换的通信机制。





### 4种创建方式

1.继承Thread类实现多线程：它代表一个线程的实例，并且启动线程的唯一方法就是通过Thread类的start（）实例方法。start（）方法是一个native方法，它将启动一个新的进程，并执行run方法。这种方式实现多线程很简单，但因为java只支持单继承所以可扩展性比较差。

```
public class Test extents Thread {
    public void run() {
      // 重写Thread的run方法
      System.out.println("dream");
    }
    
    public static void main(String[] args) {
        new Test().start();
    }
}

```

2.实现Runnable接口方式实现多线程，这种方法实现了Thread与Runnable类的解耦，Thread类负责线程启动和属性设置等内容，Runable中的run方法负责执行内容的实现，权责分明，而且也更轻量。

```
public class Test {
    public static void main(String[] args) {
        new Thread(() -> {
            System.out.println("dream");
        }).start();
    } 
}

```

------

源码：

```
@FunctionalInterface
public interface Runnable {
   /**
    * 被线程执行，没有返回值也无法抛出异常
    */
    public abstract void run();
}

//当传入一个Runnable target参数给Thread后，Thread的run()方法就会调用target.run()
public void run() {
　　if (target != null) {
　　 target.run();
　　}
}
```

3.Callable：这种方创建线程可以有返回值。实现方式是创建一个类实现Callable接口并实现call（）方法，并创建一个实例注册进FutureTask类中，然后将FutureTask类的实例注册进入Thread中运行。最后可以采用FutureTask<V>中的get方法获取自定义线程的返回值

```
public class Test {
    public static void main(String[] args) {
        // FutureTask 构造方法包装了Callable和Runnable。
        FutureTask<Integer> task = new FutureTask<>(() -> {
            System.out.println("dream");
            return 0;
        });
        new Thread(task).start();
    }
}

```

![callable创建线程](../img/callable创建线程.png)

源码：

```
@FunctionalInterface
public interface Callable<V> {
    /**
     * 计算结果，或在无法这样做时抛出异常。
     * @return 计算得出的结果
     * @throws 如果无法计算结果，则抛出异常
     */
    V call() throws Exception;
}


public class FutureTask<V> implements RunnableFuture<V>{ 
 
 public FutureTask(Callable<V> callable) {
        if (callable == null)
            throw new NullPointerException();
        this.callable = callable;
        this.state = NEW;  // ensure visibility of callable
    }
}

public interface RunnableFuture<V> extends Runnable, Future<V> {
    /**
     * Sets this Future to the result of its computation
     * unless it has been cancelled.
     */
    void run();
}
```

4.线程池创建线程：

- **降低资源消耗。**通过重复利用已创建的线程降低线程创建和销毁造成的消耗。
- **提高响应速度**。当任务到达时，任务可以不需要要等到线程创建就能立即执行。
- **提高线程的可管理性**。线程是稀缺资源，如果无限制的创建，不仅会消耗系统资源，还会降低系统的稳定性，使用线程池可以进行统一的分配，调优和监控。

```
public class Test {
    public static void main(String[] args) {
        ExecutorService threadPool = Executors.newFixedThreadPool(1);
        threadPool.submit(() -> {
            System.out.println("dream");
        });
        threadPool.shutdown();
    }
}

```

### 死锁 

- **互斥条件**：该资源任意一个时刻只由一个线程占用。(同一时刻，这个碗是我的，你不能碰)
- **请求与保持条件**：一个进程因请求资源而阻塞时，对已获得的资源保持不放。（我拿着这个碗一直不放）
- **不剥夺条件**:线程已获得的资源在末使用完之前不能被其他线程强行剥夺，只有自己使用完毕后才释放资源。（我碗中的饭没吃完，你不能抢，释放权是我自己的，我想什么时候放就什么时候放）
- **循环等待条件**:若干进程之间形成一种头尾相接的循环等待资源关系。（我拿了A碗，你拿了B碗，但是我还想要你的B碗，你还想我的A碗）

------

```
public class Test {
    private static Object res1 = new Object();
    private static Object res2 = new Object();

    public static void main(String[] args) {
        new Thread(() -> {
            synchronized (res1) {
                System.out.println(Thread.currentThread().getName() + " res1");
                // 延迟一下, 确保B拿到了res2
                try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }
                synchronized (res2) {
                    System.out.println(Thread.currentThread().getName() + " res2");
                }
            }
        }, "ThreadA").start();

        new Thread(() -> {
            synchronized (res2) {
                System.out.println(Thread.currentThread().getName() + " res2");
                // 延迟一下，确保A拿到了res1
                synchronized (res1) {
                    System.out.println(Thread.currentThread().getName() + " res1");
                }
            }
        }, "ThreadB").start();
    }
}

```

找死锁的步骤：

1. 我们通过jps确定当前执行任务的进程号
2. 然后执行jstack命令查看当前进程堆栈信息
3. 然后将会看到`Found a total of 1 deadlock`

### 主线程等待子线程方式 

- sleep:好用是好用，但是缺点太明显，不可控的延迟
- Thread.activeCount()：缺点也明显，结合while一直判断
- Join：值得考虑，但是不优雅

```
public class Test {
    void m() {
        System.out.println(Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        Test t1 = new Test();
        ArrayList<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            threads.add(new Thread(t1::m, "Thread " + i));
        }
        threads.forEach(o -> o.start());
        threads.forEach(o -> {
            try {
                o.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        System.out.println("main thread");
    }
}

```

CountDownLatch

```
public class Test {
    private CountDownLatch latch;

    public Test(CountDownLatch latch) {
        this.latch = latch;
    }

    void m() {
        System.out.println(Thread.currentThread().getName());
        latch.countDown();
    }

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(5);
        Test t1 = new Test(countDownLatch);
        for (int i = 0; i < 5; i++) {
            new Thread(t1::m, "Thread " + i).start();
        }
        countDownLatch.await();
        System.out.println("main thread");
    }
}

```

### Java锁介绍 

#### 公平锁/非公平锁 

- 公平锁指多个线程按照**申请锁的顺序来获取锁**。
- 非公平锁指多个线程获取锁的顺序**并不是按照申请锁的顺序**，有可能后申请的线程比先申请的线程优先获取锁。有可能，会造成优先级反转或者饥饿现象（很长时间都没获取到锁）。

#### 可重入锁

可重入锁又名递归锁，是指在同一个线程在**外层方法获取锁的时候，在进入内层方法会自动获取锁**，典型的synchronized，了解一下。

#### 独享锁/共享锁 

- 独享锁：是指该锁一次只能**被一个线程所持有**。
- 共享锁：是该锁可**被多个线程所持有**。

#### 互斥锁/读写锁 

上面讲的独享锁/共享锁就是一种**广义的说法**，互斥锁/读写锁就是其具体的实现。

#### 乐观锁/悲观锁 

- 悲观锁认为对于同一个人数据的并发操作，**一定是会发生修改的，哪怕没有修改，也会认为修改**。因此对于同一个数据的并发操作，悲观锁采取加锁的形式。**悲观的认为，不加锁的并发操作一定会出现问题**。
- **乐观锁则认为对于同一个数据的并发操作，是不会发生修改的。在更新数据的时候，会采用尝试更新，不断重新的方式更新数据**。乐观的认为，不加锁的并发操作时没有事情的。
- **悲观锁适合写操作非常多的场景，乐观锁适合读操作非常多的场景，不加锁带来大量的性能提升。**
- 悲观锁在Java中的使用，就是利用各种锁。乐观锁在Java中的使用，是无锁编程，常常采用的是CAS算法，典型的例子就是原子类，通过CAS自旋实现原子类操作的更新。重量级锁是悲观锁的一种，自旋锁、轻量级锁与偏向锁属于乐观锁。

#### 分段锁 

- 分段锁其实是一种锁的设计，并不是具体的一种锁，对于ConcurrentHashMap而言，其并发的实现就是通过分段锁的形式来哦实现高效的并发操作。
- 以ConcurrentHashMap来说一下分段锁的含义以及设计思想，ConcurrentHashMap中的分段锁称为Segment，它即类似于HashMap（JDK7与JDK8中HashMap的实现）的结构，即内部拥有一个Entry数组，数组中的每个元素又是一个链表；同时又是ReentrantLock（Segment继承了ReentrantLock）
- 当需要put元素的时候，并不是对整个hashmap进行加锁，而是先通过hashcode来知道他要放在那一个分段中，然后对这个分段进行加锁，所以当多线程put的时候，只要不是放在一个分段中，就实现了真正的并行的插入。但是，在统计size的时候，可就是获取hashmap全局信息的时候，就需要获取所有的分段锁才能统计。**
- **分段锁的设计目的是细化锁的粒度，当操作不需要更新整个数组的时候，就仅仅针对数组中的一项进行加锁操作。**

#### 偏向锁/轻量级锁/重量级锁 

- 这三种锁是锁的状态，并且是针对这synchronized。在Java5通过引入锁升级的机制来实现高效synchronized。这三种锁的状态是通过**对象监视器在对象头中的字段来表明的**。偏向锁是指一段同步代码一直被一个线程所访问，那么该线程会自动获取锁。降低获取锁的代价。
- 偏向锁的适用场景：**始终只有一个线程在执行代码块，在它没有执行完释放锁之前，没有其它线程去执行同步快，在锁无竞争的情况下使用**，一旦有了竞争就升级为轻量级锁，升级为轻量级锁的时候需要撤销偏向锁，撤销偏向锁的时候会导致stop the word操作；在有锁竞争时，偏向锁会多做很多额外操作，尤其是撤销偏向锁的时候会导致进入安全点，安全点会导致stw，导致性能下降，这种情况下应当禁用。
- **轻量级锁是指当锁是偏向锁的时候，被另一个线程锁访问，偏向锁就会升级为轻量级锁，其他线程会通过自选的形式尝试获取锁，不会阻塞，提高性能。**
- **重量级锁是指当锁为轻量级锁的时候，另一个线程虽然是自旋，但自旋不会一直持续下去，当自旋一定次数的时候，还没有获取到锁，就会进入阻塞，该锁膨胀为重量级锁。重量级锁会让其他申请的线程进入阻塞，性能降低。**

#### 互斥锁/自旋锁 

**互斥锁**

互斥锁是一种独占锁，当线程A加锁成功后，只要还没释放手中的锁，其他线程加锁都会失败，失败的线程释放cpu，给其他线程。既然线程 B 释放掉了 CPU，自然线程 B 加锁的代码就会被阻塞。

![1650963586479](../img/1650963586479.png)

**自旋锁**

而自旋锁在拿锁时发现已经有线程拿了锁，不会阻塞自己，而是选择进行一次忙循环尝试。也就是不停循环看是否能等到上个线程自己释放锁。自适应自旋锁指的是例如第一次设置最多自旋10次，结果在自旋的过程中成功获得了锁，那么下一次就可以设置成最多自旋20次。

##### 手写自旋锁 

```
public class SpinLock {

    // 原子引用线程
    AtomicReference<Thread> atomicReference =  new AtomicReference<>();

    public void mylock() {
        Thread thread = Thread.currentThread();
        System.out.println(Thread.currentThread().getName() + " como in...");
        while (!atomicReference.compareAndSet(null, thread)) {
//            System.out.println("不爽，重新获取一次值瞧瞧...");
        }
    }

    public void  myUnlock() {
        Thread thread = Thread.currentThread();
        atomicReference.compareAndSet(thread, null);
        System.out.println(Thread.currentThread().getName() + " invoke myUnLock...");
    }

    public static void main(String[] args) {
        SpinLock spinLock = new SpinLock();
        new Thread(() -> {
            spinLock.mylock();
            try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }
            spinLock.myUnlock();
        }, "t1").start();
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        new Thread(() -> {
            spinLock.mylock();
            spinLock.myUnlock();
        }, "t2").start();
    }

}

```



### volatile 

> [volatile](https://juejin.cn/post/6844903520760496141)

#### 说说你对volatile关键字的理解

就我理解的而言，被volatile修饰的共享变量：

- 保证了不同线程对该变量操作的内存可见性
- 禁止指令重排序（重排序会破坏volatile的内存语义）

#### 说说可见性 

这里要先从Java内存模型说起。

java虚拟机定义了一种内存模型JMM，来屏蔽掉各种硬件和操作系统的内存访问差异，让java程序在各种平台上都能达到一致的内存访问效果。JMM规定所有变量都是存在主存中的，每个线程又包含自己的工作内存，可以把它理解为CPU上的寄存器或者高速缓存。所以线程的操作都是以工作内存为主，它们只能访问自己的工作内存，且工作前后都要把值在同步回主内存。

简而言之，JMM是为了解决多线程环境下可见性问题的一组规范。另外，JMM是通过**happens-before**原则来阐述多线程之间的内存可见性，happens-before表达的意思是前一个操作的结果对后续的操作是可见的，是判断多线程之间是否存在竞争和线程安全的依据。happens-before原则包括但不仅限于如下：

> **程序顺序规则**： 一个线程中的每个操作，happens-before于该线程中的任意后续操作(保证了单线程执行结果的正确性)
>
> **监视器锁规则**：对一个线程的解锁，happens-before于随后对这个线程的加锁
>
> **volatile变量规则**： 对一个volatile域的写，happens-before于后续对这个volatile域的读
>
> **传递性**：如果A happens-before B ,且 B happens-before C, 那么 A happens-before C
>
> **start()规则**： 如果线程A执行操作`ThreadB_start()`(启动线程B) ,  那么A线程的`ThreadB_start()`happens-before 于B中的任意操作
>
> **join()原则**： 如果A执行`ThreadB.join()`并且成功返回，那么线程B中的任意操作happens-before于线程A从`ThreadB.join()`操作成功返回。
>
> **interrupt()原则**： 对线程`interrupt()`方法的调用先行发生于被中断线程代码检测到中断事件的发生，可以通过`Thread.interrupted()`方法检测是否有中断发生
>
> **finalize()原则**：一个对象的初始化完成先行发生于它的`finalize()`方法的开始



![1649727616015](../img/1649727616015.png)

而可见性就是一个变量被volatile修饰时，那么对它的修改会立刻刷新到主存，当其它线程需要读取该变量时，会去内存中读取新值，也就是前面修改发生被后面的可见。而普通变量则不能保证这一点。

其实通过synchronized和Lock也能够保证可见性，线程在释放锁之前，会把共享变量值都刷回主存，但是synchronized和Lock的开销都更大。



#### 有序性

JMM是允许编译器和处理器对指令重排序的，但是规定了as-if-serial语义，即不管怎么重排序，程序的执行结果不能改变。JMM保证了重排序不会影响到单线程的执行，但是在多线程中却容易出问题。

这时候可以为flag加上volatile关键字，禁止重排序，可以确保程序的“有序性”，也可以上重量级的synchronized和Lock来保证有序性,它们能保证那一块区域里的代码都是一次性执行完毕的。

```
public class Test {
    private volatile static Test instance = null;
    private Test(){}

    private static Test getInstance() {
        if (instance == null) {
            synchronized (Test.class) {
                if (instance == null) {
                    instance = new Test();
                }
            }
        }
        return instance;
    }
}

```

上面的代码是一个很常见的单例模式实现方式，但是上述代码在多线程环境下是有问题的。为什么呢，问题出在instance对象的初始化上，因为`instance = new Singleton();`这个初始化操作并不是原子的，在JVM上会对应下面的几条指令：

```
memory =allocate();    //1. 分配对象的内存空间 
ctorInstance(memory);  //2. 初始化对象 
instance = memory;     //3. 设置instance指向刚分配的内存地址

```

上面三个指令中，步骤2依赖步骤1，但是步骤3不依赖步骤2，所以JVM可能针对他们进行指令重拍序优化，重排后的指令如下：

```
memory =allocate();    //1. 分配对象的内存空间 
instance = memory;     //3. 设置instance指向刚分配的内存地址
ctorInstance(memory);  //2. 初始化对象 

```

这样优化之后，内存的初始化被放到了instance分配内存地址的后面，这样的话当线程1执行步骤3这段赋值指令后，刚好有另外一个线程2进入getInstance方法判断instance不为null，这个时候线程2拿到的instance对应的内存其实还未初始化，这个时候拿去使用就会导致出错。

#### 不能保证原子性 

单个volatile变量的读写具有原子性，但是类似volatile++这样的复合操作就无能为力了，比如：

> 假设：我们这边有20个线程，每个线程分别执行100次数量++；那么最终全部执行完毕的结果理论上是2000,但实际运行却达不到。

```
public class Test {
    public volatile int inc = 0;
 
    public void increase() {
        inc++;
    }
 
    public static void main(String[] args) {
        final Test test = new Test();
        for(int i=0;i<20;i++){
            new Thread(){
                public void run() {
                    for(int j=0;j<100;j++)
                        test.increase();
                };
            }.start();
        }
 
        while(Thread.activeCount()>1)  //保证前面的线程都执行完
            Thread.yield();
        System.out.println(test.inc);
    }
```

这是因为数量++是个复合操作，包括读取数量的值，对其自增，然后写回主存，如果A线程读取到了数量的值然后被阻塞了，因为没有对变量进行修改，触发不了volatile规则。线程B此时也读到了数量的值并自增后写回主存。然后轮到线程A执行的时候，A进程工作内存并不是最新的值，会造成线程B的自增被覆盖，就导致总数量达不到预期。

>有人说，**volatile不是会使缓存行无效的吗**？但是这里线程A读取到线程B也进行操作之前，并没有修改inc值，所以线程B读取的时候，还是读的10。
>
>又有人说，线程B将11写回主存，**不会把线程A的缓存行设为无效吗**？但是线程A的读取操作已经做过了啊，只有在做读取操作时，发现自己缓存行无效，才会去读主存的值，所以这里线程A只能继续做自增了。



#### 内存屏障(补充) 

**Java的Volatile的特征是任何读都能读到最新值，本质上是JVM通过内存屏障来实现的；为了实现volatile内存语义，JMM会分别限制重排序类型。**

- 当第二个操作是volatile写时，不管第一个操作是什么，都不能重排序。这个规则确保volatile写之前的操作不会被编译器重排序到volatile写之后。
- 当第一个操作是volatile读时，不管第二个操作是什么，都不能重排序。这个规则确保volatile读之后的操作不会被编译器重排序到volatile读之前。
- 当第一个操作是volatile写，第二个操作是volatile读时，不能重排序。

**为了实现volatile的内存语义，编译器在生成字节码时，会在指令序列中插入内存屏障来禁止特定类型的处理器重排序。对于编译器来说，发现一个最优布置来最小化插入屏障的总数几乎不可能，为此，JMM采取保守策略。下面是基于保守策略的JMM内存屏障插入策略：**

- 在每个volatile写操作的前面插入一个StoreStore屏障。
- 在每个volatile写操作的后面插入一个StoreLoad屏障。
- 在每个volatile读操作的后面插入一个LoadLoad屏障。
- 在每个volatile读操作的后面插入一个LoadStore屏障。

#### volatile原理 

**volatile内存语义**

- 写的内存语义： 当写一个volatile变量时,JMM会把线程对应的本地内存中的共享变量刷新到主内存。
- 读的内存语义： 当读一个volatile变量时，JMM会把线程对应的本地内存置为无效，线程接下来从主内存中读取共享变量。

**volatile本质是通过一个lock前缀指令来实现的。** lock前缀指令相当于于一个内存屏障。

它的作用是使得本CPU的Cache写入了内存，该写入动作也会引起其他的的CPU将它的相关缓存行失效，从而重新从内存加载最新的数据。所以通过这样一个操作，可让前面volatile变量的修改对其他CPU立即可见。

- 锁住内存

- 任何读必须在写完成之后再执行

- 使其它线程这个值的栈缓存失效

  ​



### ThreadLocal

> [深层原理](https://juejin.cn/post/6844903974454329358)

ThreadLocal 是一个线程的本地变量，也就意味着这个变量是线程独有的，是不能与其他线程共享的，这样就可以避免资源竞争带来的多线程的问题，这种解决多线程的安全问题和lock(这里的lock 指通过synchronized 或者Lock 等实现的锁) 是有本质的区别的:

1. lock 的资源是多个线程共享的，所以访问的时候需要加锁。
2. ThreadLocal 是每个线程都有一个副本，是不需要加锁的。
3. lock 是通过时间换空间的做法。
4. ThreadLocal 是典型的通过空间换时间的做法。

![](../img/threadLocal.awebp)

**使用：**

```
public class Test {

    public static void main(String[] args) {
        ThreadLocal<String> local = new ThreadLocal<>();
        //设置值
        local.set("hello word");
        //获取刚刚设置的值
        System.out.println(local.get());
    }
}
```

**set方法：**

```
public void set(T value) {
        Thread t = Thread .currentThread();
        // 获取线程绑定的ThreadLocalMap
        ThreadLocalMap map = getMap(t);
        if (map != null)
            map.set(this, value);
        else
           //第一次设置值的时候进来是这里
            createMap(t, value);
    }
```

值真正是放在ThreadLocalMap 中存取的，ThreadLocalMap 内部类有一个Entry 类，key是ThreadLocal 对象，value 就是你要存放的值，上面的代码value 存放的就是hello word。ThreadLocalMap 和HashMap的功能类似，但是实现上却有很大的不同：

1. HashMap 的数据结构是数组+链表
2. ThreadLocalMap的数据结构仅仅是数组
3. HashMap 是通过链地址法解决hash 冲突的问题
4. ThreadLocalMap 是通过开放地址法来解决hash 冲突的问题
5. HashMap 里面的Entry 内部类的引用都是强引用
6. ThreadLocalMap里面的Entry 内部类中的key 是弱引用，value 是强引用

**为什么ThreadLocalMap 采用开放地址法来解决哈希冲突**

开放地址法：一旦发生了冲突，就去寻找下一个空的散列地址(这非常重要，源码都是根据这个特性，必须理解这里才能往下走)，只要散列表足够大，空的散列地址总能找到，并将记录存入。

jdk 中大多数的类都是采用了链地址法来解决hash 冲突，为什么ThreadLocalMap 采用开放地址法来解决哈希冲突呢？

**链地址法和开放地址法的优缺点**

开放地址法：

1. 容易产生堆积问题，不适于大规模的数据存储。
2. 散列函数的设计对冲突会有很大的影响，插入时可能会出现多次冲突的现象。
3. 删除的元素是多个冲突元素中的一个，需要对后面的元素作处理，实现较复杂。

链地址法：

1. 处理冲突简单，且无堆积现象，平均查找长度短。
2. 链表中的结点是动态申请的，适合构造表不能确定长度的情况。
3. 删除结点的操作易于实现。只要简单地删去链表上相应的结点即可。
4. 指针需要额外的空间，故当结点规模较小时，开放定址法较为节省空间。

**ThreadLocalMap 采用开放地址法原因**

1. ThreadLocal 中看到一个属性 HASH_INCREMENT = 0x61c88647 ，0x61c88647 是一个神奇的数字，让哈希码能均匀的分布在2的N次方的数组里, 即 Entry[] table，关于这个神奇的数字google 有很多解析，这里就不重复说了
2. ThreadLocal 往往存放的数据量不会特别大（而且key 是弱引用又会被垃圾回收，及时让数据量更小），这个时候开放地址法简单的结构会显得更省空间，同时数组的查询效率也是非常高，加上第一点的保障，冲突概率也低








### synchronized

> https://juejin.cn/post/6973571891915128846

synchronized之前一直都是重量级的锁，但是后来java官方是对他进行过升级的，他现在采用的是锁升级的方式去做的。

#### 修饰范围 

- 实例方法：作用于当前对象实例加锁，进入同步代码前要获得当前对象实例的锁

  ```
  public synchronized void add(){
    i++;
  }
  ```

- 静态方法：作用于当前类对象加锁，进入同步代码前要获得当前类对象的锁(和实例方法并无差别)

  ```
  public static synchronized void add(){
    i++;
  }
  ```

- 修饰代码块：指定加锁对象，对给定对象加锁，进入同步代码库前要获得给定对象的锁

  ```
  public void add{
    synchronized(this){
      i++;
    }
  }
  ```


#### 底层原理 

**java对象头**

>  对象头（标记字段，类型指针）、实例数据、对齐填充

在对象头的Mark Word中主要存储了对象自身的运行时数据，例如哈希码、GC分代年龄、锁状态、线程持有的锁、偏向线程ID以及偏向时间戳等。同时，Mark Word也记录了对象和锁有关的信息。

当对象被synchronized关键字当成同步锁时，和锁相关的一系列操作都与Mark Word有关。由于在JDK1.6版本中对synchronized进行了锁优化，引入了偏向锁和轻量级锁（关于锁优化后边详情讨论）。Mark Word在不同锁状态下存储的内容有所不同。我们以32位JVM中对象头的存储内容如下图所示。

![1650780096767](../img/1650780096767.png)

从图中可以清楚的看到，Mark Word中有2bit的数据用来标记锁的状态。无锁状态和偏向锁标记位为01，轻量级锁的状态为00，重量级锁的状态为10。

- 当对象为偏向锁时，Mark Word存储了偏向线程的ID；
- 当状态为轻量级锁时，Mark Word存储了指向线程栈中Lock Record的指针；
- 当状态为重量级锁时，Mark Word存储了指向堆中的Monitor对象的指针。

当前我们只讨论重量级锁，因为重量级锁相当于对synchronized优化之前的状态。关于偏向锁和轻量级锁在后边锁优化章节中详细讲解。

可以看到，当为重量级锁时，对象头的MarkWord中存储了指向Monitor对象的指针。

**Monitor对象锁**

Monitor对象被称为管程或者监视器锁。在Java中，每一个对象实例都会关联一个Monitor对象。这个Monitor对象既可以与对象一起创建销毁，也可以在线程试图获取对象锁时自动生成。当这个Monitor对象被线程持有后，它便处于锁定状态。

ObjectMonitor中有五个重要部分，分别为_ower,_WaitSet,_cxq,_EntryList和count。

- **_ower** 用来指向持有monitor的线程，它的初始值为NULL,表示当前没有任何线程持有monitor。当一个线程成功持有该锁之后会保存线程的ID标识，等到线程释放锁后_ower又会被重置为NULL;
- **_WaitSet** 调用了锁对象的wait方法后的线程会被加入到这个队列中；
- **_cxq**  是一个临时存放阻塞队列，线程被唤醒后根据决策判读是放入cxq还是EntryList;
- **_EntryList** 没有抢到锁的线程会被放到这个队列；
- **count** 用于记录线程获取锁的次数，成功获取到锁后count会加1，释放锁时count减1。

如果线程获取到对象的monitor后，就会将monitor中的ower设置为该线程的ID，同时monitor中的count进行加1.如果调用锁对象的wait()方法，线程会释放当前持有的monitor，并将owner变量重置为NULL，且count减1,同时该线程会进入到_WaitSet集合中等待被唤醒。

##### **修饰代码块原理**

synchronized 同步语句块的实现是机器会自动添加 monitorenter 和 monitorexit 指令，其中 monitorenter 指令指向同步代码块的开始位置，线程尝试获得该对象的锁。monitorexit 指令则指明同步代码块的结束位置，锁的计数器会减1。

倘若其他线程已经拥有monitor 的所有权，那么当前线程获取锁失败将被阻塞并进入到_EntryList中，直到等待的锁被释放为止。也就是说，当所有相应的monitorexit指令都被执行，计数器的值减为0，执行线程将释放 monitor(锁)，其他线程才有机会持有 monitor。

##### **修饰方法原理**

synchronized 修饰的方法并没有 monitorenter 指令和 monitorexit 指令，因为整个方法都是同步代码，因此就不需要标记同步代码的入口和出口了。取得代之的是在方法的flag上加入了 ACC_SYNCHRONIZED 标识，该标识指明了该方法是一个同步方法，当线程线程执行到这个方法时会判断是否有这个ACC_SYNCHRONIZED标志，如果有的话则会尝试获取monitor对象锁。执行步骤与同步代码块一致。



#### 1.6版本的优化

##### 1.6前

在 Java 早期版本中，`synchronized` 属于重量级锁，效率低下，因为监视器锁（monitor）是依赖于底层的操作系统的 **Mutex Lock** 来实现的，Java 的线程是映射到操作系统的原生线程之上的。如果要挂起或者唤醒一个线程，都需要**操作系统**帮忙完成，而操作系统实现线程之间的切换时需要进行系统调用，从**用户态转换到内核态**，这个状态之间的转换需要相对比较长的时间，时间成本相对较高，这也是为什么早期的 synchronized 效率低的原因。

##### 1.6后

JDK1.6对锁的实现引入了大量的优化，如**自旋锁、适应性自旋锁、锁消除、锁粗化、偏向锁、轻量级锁等**技术来减少锁操作的开销。

##### 偏向锁

偏向锁的核心思想是，如果一个线程获得了锁，那么锁就进入偏向模式。当这个线程再次请求锁时，无需再做任何同步操作，即可获取锁的过程，这样就省去了大量有关锁申请的操作，从而也就提升了程序的性能。所以，对于没有锁竞争的场合，偏向锁有很好的优化效果，毕竟极有可能连续多次是同一个线程申请相同的锁。但是，对于锁竞争比较激烈的情况，偏向锁就有问题了。因为每次申请锁的都可能是不同线程。这种情况使用偏向锁就会得不偿失，此时就会升级为轻量级锁。

轻量级锁在无竞争的情况下使用 CAS 操作去代替使用互斥量。而偏向锁在无竞争的情况下会把整个同步都消除掉。

- 访问Mark Word中**偏向锁的标识是否设置成1**，**锁标识位是否为01**，确认偏向状态
- 如果为可偏向状态，则判断**当前线程ID是否为偏向线程**
- 如果偏向线程位当前线程，则通过**cas操作竞争锁**，如果竞争成功则操作Mark Word中线程ID设置为当前线程ID
- 如果cas偏向锁获取失败，则挂起当前偏向锁线程，偏向锁升级为**轻量级锁**

##### 轻量级锁 

轻量级锁能够提升程序同步性能的依据是“对于绝大部分锁，在整个同步周期内都是不存在竞争的”。当升级为轻量级锁之后，MarkWord的结构也会随之变为轻量级锁结构。JVM会利用CAS尝试把对象原本的Mark Word 更新为Lock Record的指针，成功就说明加锁成功，改变锁标志位为00，然后执行相关同步操作。

轻量级锁所适应的场景是线程交替执行同步块的场合，如果存在同一时间访问同一锁的场合，就会导致轻量级锁就会失效，进而膨胀为重量级锁。

如果没有竞争，轻量级锁使用 CAS 操作避免了使用互斥操作的开销。但如果存在锁竞争，除了互斥量开销外，还会额外发生CAS操作，因此在有锁竞争的情况下，轻量级锁比传统的重量级锁更慢！如果锁竞争激烈，那么轻量级将很快膨胀为重量级锁！

- 线程由偏向锁升级为轻量级锁时，会先把**锁的对象头MarkWord复制一份到线程的栈帧中，建立一个名为锁记录空间（Lock Record），用于存储当前Mark Word的拷贝**。
- 虚拟机使用cas操作尝试将**对象的Mark Word指向Lock Record的指针，并将Lock record里的owner指针指对象的Mark Word**。
- 如果cas操作成功，则该线程拥有了对象的轻量级锁。第二个线程cas自旋锁等待锁线程释放锁。
- 如果多个线程竞争锁，轻量级锁要膨胀为**重量级锁**，**Mark Word中存储的就是指向重量级锁（互斥量）的指针**。其他等待线程进入阻塞状态。

##### 锁消除

锁消除理解起来很简单，它指的就是虚拟机即使编译器在运行时，如果检测到那些共享数据不可能存在竞争，那么就执行锁消除。锁消除可以节省毫无意义的请求锁的时间。

##### 自适应锁的自旋锁 

**一般线程持有锁的时间都不是太长，所以仅仅为了这一点时间去挂起线程/恢复线程是得不偿失的。** 所以，虚拟机的开发团队就这样去考虑：“我们能不能让后面来的请求获取锁的线程等待一会而不被挂起呢？看看持有锁的线程是否很快就会释放锁”。**为了让一个线程等待，我们只需要让线程执行一个忙循环（自旋），这项技术就叫做自旋**。

**在 JDK1.6 中引入了自适应的自旋锁。自适应的自旋锁带来的改进就是：自旋的时间不在固定了，而是和前一次同一个锁上的自旋时间以及锁的拥有者的状态来决定**。

##### 锁粗化

原则上，我们在编写代码的时候，总是推荐将同步块的作用范围限制得尽量小，——直在共享数据的实际作用域才进行同步，这样是为了使得需要同步的操作数量尽可能变小，如果存在锁竞争，那等待线程也能尽快拿到锁。

##### 总升级过程

![1650780096767](../img/1650780096767.png)

- 当没有被当成锁时，这就是一个普通的对象，头像头中的Mark Word标记字段记录对象的HashCode，锁标志位是01，是否偏向锁那一位是0。
- 当对象被当做同步锁并有一个线程抢到锁时，是否为偏向锁那位由0改为1，表示偏向锁状态。
- 然后有线程继续尝试获取锁，检测Mark Word里面是不是当前线程的ID，如果是，表示当前线程已经获得了这个偏向锁，可以执行同步中的代码


- 如果不是，则使用CAS将当前线程的ID替换Mard Word，如果成功则表示当前线程获得偏向锁，置偏向标志位1
- 如果失败，则说明发生竞争，撤销偏向锁，进而升级为轻量级锁。
- JVM会在当前线程的线程栈中开辟一块单独的空间，里面保存指向对象锁Mark Word的指针，同时在对象锁Mark Word中保存指向这片空间的指针。这两个保存操作都是CAS操作，如果成功，当前线程获得同步锁。锁标志为改为00，可以执行同步代码。
- 如果失败，表示其他线程竞争锁，当前线程便尝试使用自旋来获取锁。代表不断的重试，尝试抢锁。从JDK1.7开始，自旋锁默认启用，自旋次数由JVM决定。
- 如果自旋成功则依然处于轻量级状态。
- 如果自旋失败，则升级为重量级锁。锁标识为改为10。

#### 和ReentractLock对比 

- **两者都是可重入锁**:两者都是可重入锁。“可重入锁”概念是：**自己可以再次获取自己的内部锁**。比如一个线程获得了某个对象的锁，此时这个对象锁还没有释放，当其再次想要获取这个对象的锁的时候还是可以获取的，如果不可锁重入的话，就会造成死锁。同一个线程每次获取锁，锁的计数器都自增1，所以要等到锁的计数器下降为0时才能释放锁。
- **synchronized 依赖于 JVM 而 ReenTrantLock 依赖于 API**:synchronized 是依赖于 JVM 实现的，前面我们也讲到了 虚拟机团队在 JDK1.6 为 synchronized 关键字进行了很多优化，但是这些优化都是在虚拟机层面实现的，并没有直接暴露给我们。ReenTrantLock 是 JDK 层面实现的（也就是 API 层面，需要 lock() 和 unlock 方法配合 try/finally 语句块来完成），所以我们可以通过查看它的源代码，来看它是如何实现的。
- ReenTrantLock 比 synchronized 增加了一些高级功能
  1. **等待可中断**：过lock.lockInterruptibly()来实现这个机制。也就是说正在等待的线程可以选择放弃等待，改为处理其他事情。
  2. **可实现公平锁**
  3. **可实现选择性通知（锁可以绑定多个条件）**：线程对象可以注册在指定的Condition中，从而可以有选择性的进行线程通知，在调度线程上更加灵活。 在使用notify/notifyAll()方法进行通知时，被通知的线程是由 JVM 选择的，用ReentrantLock类结合Condition实例可以实现“选择性通知”
  4. **性能已不是选择标准**：在jdk1.6之前synchronized 关键字吞吐量随线程数的增加，下降得非常严重。1.6之后，**synchronized 和 ReenTrantLock 的性能基本是持平了。**

### Atomic

> 底层使用了cas

> https://juejin.cn/post/6977993272538955806

从JDK1.5开始，Java在java.util.concurrent.atomic包下引入了一些Atomic相关的**原子操作类**，这些类避免使用加锁来实现同步，从而更加方便、高效的实现原子操作。

Atomic包下所有的原子类都只适用于单个元素，即只能保证一个基本数据类型、对象、或者数组的原子性。根据使用范围，可以将这些类分为四种类型，分别为原子**更新基本类型**、**原子更新数组**、**原子更新引用**、**原子更新属性**。

**1.原子更新基本类型**

包括AtomicInteger、AtomicLong、AtomicBoolean三个类，这里，我们以AtomicInteger为例来学习如何使用。

```
// 获取当前值，然后自加，相当于i++
getAndIncrement()
// 获取当前值，然后自减，相当于i--
getAndDecrement()
// 自加1后并返回，相当于++i
incrementAndGet()
// 自减1后并返回，相当于--i
decrementAndGet()
// 获取当前值，并加上预期值
getAndAdd(int delta)
// 获取当前值，并设置新值
int getAndSet(int newValue)
```

**2.原子更新引用类型**

- AtomicReference 引用原子类
- AtomicStampedReference 原子更新带有版本号的引用类型。该类将整数值与引用关联起来，可用于解决原子的更新数据和数据的版本号，可以解决使用 CAS 进行原子更新时可能出现的 ABA 问题。（关于CAS及ABA问题后文详细分析）
- AtomicMarkableReference** 原子更新带有标记的引用类型。该类将 boolean 标记与引用关联起来。

接下来以AtomicReference为例来分析，首先看下AtomicReference的类结构：

```
public class AtomicReference<V> implements java.io.Serializable {

    public final V get() {
        return value;
    }

    public final void set(V newValue) {
        value = newValue;
    }
    
    public final boolean compareAndSet(V expectedValue, V newValue) {
        return VALUE.compareAndSet(this, expectedValue, newValue);
    }  
    
    // ...省略其他
}
```

可以看到AtomicReference是一个泛型类，内部设置及更新引用类型数据的方法。以compareAndSet方法为例来看如何使用。

```
public class Book {
    public String name;

    public int price;

    public Book(String name, int price) {
        this.name = name;
        this.price = price;
    }
}

AtomicReference<Book> atomicReference = new AtomicReference<>();
Book book1 = new Book("三国演义", 42);
atomicReference.set(book1);
Book book2 = new Book("水浒传", 40);
atomicReference.compareAndSet(book1, book2);
System.out.println("Book name is " + atomicReference.get().name + ",价格是" + atomicReference.get().price);           
```

**3.原子更新数组**

这里原子更新数组并不是对数组本身的原子操作，而是对数组中的元素。主要包括3个类：AtomicIntegerArray、AtomicLongArray及AtomicReferenceArray，分别表示原子更新整数数组的元素、原子更新长整数数组的元素以及原子更新引用类型数组的元素。我们以AtomicIntegerArray为例来看：

```
public class AtomicIntegerArray implements java.io.Serializable {
    // final类型的int数组
    private final int[] array;
    // 获取数组中第i个元素
    public final int get(int i) {
        return (int)AA.getVolatile(array, i);
    }   
    // 设置数组中第i个元素
    public final void set(int i, int newValue) {
        AA.setVolatile(array, i, newValue);
    }
    // CAS更改第i个元素
    public final boolean compareAndSet(int i, int expectedValue, int newValue) {
        return AA.compareAndSet(array, i, expectedValue, newValue);
    }
    // 获取第i个元素，并加1
    public final int getAndIncrement(int i) {
        return (int)AA.getAndAdd(array, i, 1);
    }
    // 获取第i个元素并减1
    public final int getAndDecrement(int i) {
        return (int)AA.getAndAdd(array, i, -1);
    }   
    // 对数组第i个元素加1后再获取
    public final int incrementAndGet(int i) {
        return (int)AA.getAndAdd(array, i, 1) + 1;
    }  
    // 对数组第i个元素减1后再获取
    public final int decrementAndGet(int i) {
        return (int)AA.getAndAdd(array, i, -1) - 1;
    }    
    // ... 省略
}    
```

可以看到，在AtomicIntegerArray内部维护了一个final修饰的int数组，且类中所有的操作都是针对数组元素的操作。同时，这些方法都是原子操作，可以保证多线程下数据的安全性。

**4.原子更新对象属性**

如果直选更新某个对象中的某个字段，可以使用更新对象字段的原子类。包括三个类，AtomicIntegerFieldUpdater、AtomicLongFieldUpdater以及AtomicReferenceFieldUpdater。需要注意的是这些类的使用需要满足以下条件才可。

- 被操作的字段不能是static类型；
- 被操纵的字段不能是final类型；
- 被操作的字段必须是volatile修饰的；
- 属性必须对于当前的Updater所在区域是可见的。

下面以AtomicIntegerFieldUpdater为例，结合前例中的Book类来更新Book的价格，注意将price用volatile修饰。

```
public class Book {
    public String name;

    public volatile int price;

    public Book(String name, int price) {
        this.name = name;
        this.price = price;
    }
}

AtomicIntegerFieldUpdater<Book> updater = AtomicIntegerFieldUpdater.newUpdater(Book.class, "price");
Book book = new Book("三国演义", 42);
updater.set(book, 50);
System.out.println( "更新后的价格是" + updater.get(book));
```





### CAS 

> 从思想上来说，Synchronized属于悲观锁，悲观地认为程序中的并发情况严重，所以严防死守。CAS属于乐观锁，乐观地认为程序中的并发情况不那么严重，所以让线程不断去尝试更新。

> **我们在读Concurrent包下的类的源码时，发现无论是**ReenterLock内部的AQS，还是各种Atomic开头的原子类，内部都应用到了`CAS`，并且在调用getAndAddInt方法中，会有compareAndSwapInt方法

CAS是Compare And Swap的简称，即比较并交换的意思。CAS是一种无锁算法。

CAS的函数公式：compareAndSwap(V,E,N)； 其中V表示要更新的变量，E表示预期值，N表示要更新成的值。

CAS其实存在一个循环的过程，如果有多个线程在同时修改这一个变量V，在修改之前会先拿到这个变量的值，再和变量对比看是否相等，如果相等，则说明没有其它线程修改这个变量，自己更新变量即可。如果发现要修改的变量和期望值不一样，则说明再读取变量V的值后，有其它线程对变量V做了修改，那么，放弃本次更新，重新读变量V的值，并再次尝试修改，直到修改成功为止。这个循环过程一般也称作**自旋**。

CAS 在底层保证了数据的原子性，它的好处是

- 不必做同步阻塞的挂起以及唤醒线程这样大量的开销
- 将保证数据原子性的这个操作交给了底层硬件性能远远高于做同步阻塞挂起、唤醒等操作，所以它的并发性更好
- 可以根据 CAS 返回的状态决定后续操作来达到数据的一致性，比如 increment 失败那就一值循环直到成功为止（下文会讲）等等

CAS操作的整个过程如下图所示：

![1650801861293](../img/1650801861293.png)

#### **CAS存在的缺点：**

1.只能保证一个共享变量的原子性

CAS不像synchronized和RetranLock一样可以保证一段代码和多个变量的同步。对于多个共享变量操作是CAS是无法保证的，这时候必须使用加锁来实现。

2.存在性能开销问题

由于CAS是一个自旋操作，如果长时间的CAS不成功会给cpu带来很大的开销。

3.ABA问题

> 因为CAS是通过检查值有没有发生改变来保证原子性的，假若一个变量V的值为A，线程1和线程2同时都读取到了这个变量的值A，此时线程1将V的值改为了B，然后又改回了A，期间线程2一直没有抢到CPU时间片。知道线程1将V的值改回A后线程2才得到执行。那么此时，线程2并不知道V的值曾经改变过。这个问题就被成为**ABA问题**。
>
> ABA问题的解决其实也容易处理，即添加一个版本号，更次更新值同时也更新版本号即可。上文中提到的AtomicStampedReference就是用来解决ABA问题的。
>
> 时间戳也可以，查询的时候把时间戳一起查出来，对的上才修改并且更新值的时候一起修改更新时间.



#### CAS的实现类--Unsafe

Unsafe类是位于sun.misc包下的一个类，这个类中提供了用于执行低级别、不安全的操作方法，它即可以让Java语言像C语言指针一样操作内存，同时还提供了CAS、内存屏障、线程调度、对象操作、数组操作等能力。

![](../img/unsafe.awebp)

**1.获取unsafe实例**

Unsafe类是一个单例，并且提供了一个getUnsafe的方法来获取Unsafe的实例。但是，这个方法只有在引导类加载器加载Unsafe类是调用才合法，否则会抛出一个SecurityException异常，如下：

```
Exception in thread "main" java.lang.SecurityException: Unsafe
	at jdk.unsupported/sun.misc.Unsafe.getUnsafe(Unsafe.java:99)
	at atomic.AtomicDemo.increase(AtomicDemo.java:28)
	at atomic.AtomicDemo.main(AtomicDemo.java:34)
```

因此，想要获取Unsafe类的实例就需要另辟蹊径了。使用反射来获取Unsafe实例是一个比较好的方案，实现代码如下：

```
        try {
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            Unsafe unsafe = (Unsafe) field.get(null);
        } catch (Exception e) {
            e.printStackTrace();
        }

```

**2.Unsafe类中的CAS**

Unsafe类中与CAS相关的主要有以下几个方法

```
    // 第一个参数o为给定对象，offset为对象内存的偏移量，通过这个偏移量迅速定位字段并设置或获取该字段的值，expected表示期望值，x表示要设置的值，下面3个方法都通过CAS原子指令执行操作。
    public final native boolean compareAndSetInt(Object o,long offset,int expected,int x);
                                                 
    public final native boolean compareAndSetObject(Object o, long offset,Object expected,Object x);    
    
    public final native boolean compareAndSetLong(Object o, long offset,long expected,long x);    

```

可以看到，这些方法都是native方法，调用的底层代码实现。在JDK1.8中还引入了getAndAddInt、getAndAddLong、getAndSetInt、getAndSetLong、getAndSetObject等方法来支持不同类型CAS操作。

而AtomicInteger中也正是使用了这里的方法才实现的CAS操作。

**3.线程调度相关**

在Unsafe中提供了线程挂起、恢复及锁机制相关的方法。

```
//取消阻塞线程
public native void unpark(Object thread);
//阻塞线程
public native void park(boolean isAbsolute, long time);
//获得对象锁（可重入锁）
@Deprecated
public native void monitorEnter(Object o);
//释放对象锁
@Deprecated
public native void monitorExit(Object o);
//尝试获取对象锁
@Deprecated
public native boolean tryMonitorEnter(Object o);
```

在上篇文章讲解RetranLock与AQS时涉及到线程挂起的操作其实也是调用的Unsafe的park方法。

```
    // LockSupport
    
    private static final Unsafe U = Unsafe.getUnsafe();
    
    public static void park(Object blocker) {
        Thread t = Thread.currentThread();
        setBlocker(t, blocker);
        U.park(false, 0L);
        setBlocker(t, null);
    }
```

**4.对象操作Unsafe还提供了对象实例化及操作对象属性相关的方法**

```
//返回对象成员属性在内存地址相对于此对象的内存地址的偏移量
public native long objectFieldOffset(Field f);
//获得给定对象的指定地址偏移量的值，与此类似操作还有：getInt，getDouble，getLong，getChar等
public native Object getObject(Object o, long offset);
//给定对象的指定地址偏移量设值，与此类似操作还有：putInt，putDouble，putLong，putChar等
public native void putObject(Object o, long offset, Object x);
//从对象的指定偏移量处获取变量的引用，使用volatile的加载语义
public native Object getObjectVolatile(Object o, long offset);
//存储变量的引用到对象的指定的偏移量处，使用volatile的存储语义
public native void putObjectVolatile(Object o, long offset, Object x);
//有序、延迟版本的putObjectVolatile方法，不保证值的改变被其他线程立即看到。只有在field被volatile修饰符修饰时有效
public native void putOrderedObject(Object o, long offset, Object x);
//绕过构造方法、初始化代码来创建对象
public native Object allocateInstance(Class<?> cls) throws InstantiationException;
复制代码
```

Unsafe中提供的allocateInstance方法可以绕过对象的构造方法直接创建对象，Gson解析json反序列化对象时就有用到这个方法。

```
// 来自Gson#UnsafeAllocator
public abstract <T> T newInstance(Class<T> var1) throws Exception;

public static UnsafeAllocator create() {
        try {
            Class<?> unsafeClass = Class.forName("sun.misc.Unsafe");
            Field f = unsafeClass.getDeclaredField("theUnsafe");
            f.setAccessible(true);
            final Object unsafe = f.get((Object)null);
            final Method allocateInstance = unsafeClass.getMethod("allocateInstance", Class.class);
            return new UnsafeAllocator() {
                public <T> T newInstance(Class<T> c) throws Exception {
                    assertInstantiable(c);
                    return allocateInstance.invoke(unsafe, c);
                }
            };
        } catch (Exception var6) {
            // ...省略异常处理
        }
    }
```

关于Gson使用allocateInstance实例化对象的详细过程可以可以参考[《一个非静态内部类引起的空指针》](https://link.juejin.cn?target=https%3A%2F%2Fgithub.com%2Fzhpanvip%2FAndroidNote%2Fwiki%2F%25E9%25A1%25B9%25E7%259B%25AE%25E4%25B8%25AD%25E9%2581%2587%25E5%2588%25B0%25E7%259A%2584%25E9%2597%25AE%25E9%25A2%2598)

**5.Unsafe的其它功能**

除了CAS、线程调度、对象相关的功能外，Unsafe还提供了内存操作，可以实现堆外内存的分配。提供的数组相关的方法来定位数组中每个元素在内存中的位置，等等。



### AQS 

> https://juejin.cn/post/6975435256111300621

> AQS 使用一个 int 成员变量来表示同步状态（state），通过内置的 FIFO 队列来完成获取资源线程的排队工作。AQS 使用 CAS 对该同步状态进行原子操作实现对其值的修改。(重点)

state

```
//返回同步状态的当前值
protected final int getState() {
        return state;
}
 // 设置同步状态的值
protected final void setState(int newState) {
        state = newState;
}
//原子地（CAS操作）将同步状态值设置为给定值update如果当前同步状态的值等于expect（期望值）
protected final boolean compareAndSetState(int expect, int update) {
        return unsafe.compareAndSwapInt(this, stateOffset, expect, update);
}
```

常用方法

```
isHeldExclusively()//该线程是否正在独占资源。只有用到condition才需要去实现它。
tryAcquire(int)//独占方式。尝试获取资源，成功则返回true，失败则返回false。
tryRelease(int)//独占方式。尝试释放资源，成功则返回true，失败则返回false。
tryAcquireShared(int)//共享方式。尝试获取资源。负数表示失败；0表示成功，但没有剩余可用资源；正数表示成功，且有剩余资源。
tryReleaseShared(int)//共享方式。尝试释放资源，成功则返回true，失败则返回false。

```

#### ReentrantLock

##### 构造方法

```
// 默认是非公平锁
public ReentrantLock() {
    sync = new NonfairSync();
}
// 可选参数，是否公平
public ReentrantLock(boolean fair) {
    sync = fair ? new FairSync() : new NonfairSync();
}

```

##### 公平锁的实现 

```
protected final boolean tryAcquire(int acquires) {
    final Thread current = Thread.currentThread();
    int c = getState();
    if (c == 0) {
        if (!hasQueuedPredecessors() && // 判断队列是否轮到该线程
            compareAndSetState(0, acquires)) { // 利用cas更换状态
            setExclusiveOwnerThread(current); // 如果都ok，就设置独占锁
            return true;
        }
    }
    else if (current == getExclusiveOwnerThread()) {// 判断当前独占锁是否还是当前线程
        int nextc = c + acquires;// 状态累加
        if (nextc < 0)
            throw new Error("Maximum lock count exceeded");
        setState(nextc); // 设置状态
        return true;
    } // 否则false
    return false;
}

```

##### 非公平锁的实现

```
final boolean nonfairTryAcquire(int acquires) {
    final Thread current = Thread.currentThread();
    int c = getState();
    if (c == 0) {
        if (compareAndSetState(0, acquires)) {// 在这里...
            setExclusiveOwnerThread(current);
            return true;
        }
    }
    else if (current == getExclusiveOwnerThread()) {
        int nextc = c + acquires;
        if (nextc < 0) // overflow
            throw new Error("Maximum lock count exceeded");
        setState(nextc);
        return true;
    }
    return false;
}

```

#### CountDownLatch

> CountDownLatch是共享锁的一种实现,它默认构造 `AQS` 的 `state` 值为 `count`。当线程使用`countDown`方法时,其实使用了`tryReleaseShared`方法以CAS的操作来减少`state`,直至`state`为0就代表所有的线程都调用了`countDown`方法。当调用`await`方法的时候，如果`state`不为0，就代表仍然有线程没有调用countDown方法，那么就把已经调用过countDown的线程都放入**阻塞队列Park,并自旋CAS判断state == 0**，直至最后一个线程调用了countDown，使得state == 0，于是阻塞的线程便判断成功，全部往下执行。

##### 构造方法

```
public CountDownLatch(int count) {
    if (count < 0) throw new IllegalArgumentException("count < 0");
    this.sync = new Sync(count); // 这里设置state的次数 
}

```

##### countDown 

```
public void countDown() {
    sync.releaseShared(1);
}

```

##### tryReleaseShared

```
protected boolean tryReleaseShared(int releases) {
    // Decrement count; signal when transition to zero
    for (;;) {
        int c = getState();
        if (c == 0)
            return false;
        int nextc = c-1; // 每执行一次该方法，状态减一
        if (compareAndSetState(c, nextc))
            return nextc == 0;
    }
}

```

##### 用法

- 某一线程在开始运行前等待 n 个线程执行完毕。将 CountDownLatch 的计数器初始化为 n ：`new CountDownLatch(n)`，每当一个任务线程执行完毕，就将计数器减 1 `countdownlatch.countDown()`，当计数器的值变为 0 时，在`CountDownLatch上 await()` 的线程就会被唤醒。一个典型应用场景就是启动一个服务时，主线程需要等待多个组件加载完毕，之后再继续执行。
- 实现多个线程开始执行任务的最大并行性。注意是并行性，不是并发，强调的是多个线程在某一时刻同时开始执行。类似于赛跑，将多个线程放到起点，等待发令枪响，然后同时开跑。做法是初始化一个共享的 `CountDownLatch` 对象，将其计数器初始化为 1 ：`new CountDownLatch(1)`，多个线程在开始执行任务前首先 `coundownlatch.await()`，当主线程调用 countDown() 时，计数器变为 0，多个线程同时被唤醒。
- 死锁检测：一个非常方便的使用场景是，你可以使用 n 个线程访问共享资源，在每次测试阶段的线程数目是不同的，并尝试产生死锁。

#### CyclicBarrier

> CyclicBarrier 的字面意思是可循环使用（Cyclic）的屏障（Barrier）。它要做的事情是，让一组线程到达一个屏障（也可以叫同步点）时被阻塞，直到最后一个线程到达屏障时，屏障才会开门，所有被屏障拦截的线程才会继续干活。CyclicBarrier 默认的构造方法是 `CyclicBarrier(int parties)`，其参数表示屏障拦截的线程数量，每个线程调用`await`方法告诉 CyclicBarrier 我已经到达了屏障，然后当前线程被阻塞。

##### dowait 

当调用 `CyclicBarrier` 对象调用 `await()` 方法时，实际上调用的是`dowait(false, 0L)`方法。 `await()` 方法就像树立起一个栅栏的行为一样，将线程挡住了，当拦住的线程数量达到 parties 的值时，栅栏才会打开，线程才得以通过执行。

代码就不贴了。要看，就去jdk上看

总结：`CyclicBarrier` 内部通过一个 count 变量作为计数器，count 的初始值为 parties 属性的初始化值，每当一个线程到了栅栏这里了，那么就将计数器减一。如果 count 值为 0 了，表示这是这一代最后一个线程到达栅栏，就尝试执行我们构造方法中输入的任务。

#### Semaphore 

**synchronized 和 ReentrantLock 都是一次只允许一个线程访问某个资源，Semaphore(信号量)可以指定多个线程同时访问某个资源。**

### ThreadLocal 

> 如果想实现每一个线程都有自己的专属本地变量该如何解决呢？ JDK中提供的`ThreadLocal`类正是为了解决这样的问题。 **`ThreadLocal`类主要解决的就是让每个线程绑定自己的值，可以将`ThreadLocal`类形象的比喻成存放数据的盒子，盒子中可以存储每个线程的私有数据。**如果你创建了一个`ThreadLocal`变量，那么访问这个变量的每个线程都会有这个变量的本地副本，这也是`ThreadLocal`变量名的由来。他们可以使用 `get（）` 和 `set（）` 方法来获取默认值或将其值更改为当前线程所存的副本的值，从而避免了线程安全问题。

#### 原理

```
public class Thread implements Runnable {
 ......
//与此线程有关的ThreadLocal值。由ThreadLocal类维护
ThreadLocal.ThreadLocalMap threadLocals = null;

//与此线程有关的InheritableThreadLocal值。由InheritableThreadLocal类维护
ThreadLocal.ThreadLocalMap inheritableThreadLocals = null;
 ......
}

```

从上面`Thread`类 源代码可以看出`Thread` 类中有一个 `threadLocals` 和 一个 `inheritableThreadLocals` 变量，它们都是 `ThreadLocalMap` 类型的变量,我们可以把 `ThreadLocalMap` 理解为`ThreadLocal` 类实现的定制化的 `HashMap`。默认情况下这两个变量都是null，只有当前线程调用 `ThreadLocal` 类的 `set`或`get`方法时才创建它们，实际上调用这两个方法的时候，我们调用的是`ThreadLocalMap`类对应的 `get()`、`set() `方法。

```
public void set(T value) {
    Thread t = Thread.currentThread();
    ThreadLocalMap map = getMap(t);
    if (map != null)
        map.set(this, value);
    else
        createMap(t, value);
}
ThreadLocalMap getMap(Thread t) {
    return t.threadLocals;
}

```

**最终的变量是放在了当前线程的 ThreadLocalMap 中，并不是存在 ThreadLocal 上，ThreadLocal 可以理解为只是ThreadLocalMap的封装，传递了变量值。**

**每个Thread中都具备一个ThreadLocalMap，而ThreadLocalMap可以存储以ThreadLocal为key的键值对。** 比如我们在同一个线程中声明了两个 `ThreadLocal` 对象的话，会使用 `Thread`内部都是使用仅有那个`ThreadLocalMap` 存放数据的，`ThreadLocalMap`的 key 就是 `ThreadLocal`对象，value 就是 `ThreadLocal` 对象调用`set`方法设置的值。`ThreadLocal` 是 map结构是为了让每个线程可以关联多个 `ThreadLocal`变量。这也就解释了ThreadLocal声明的变量为什么在每一个线程都有自己的专属本地变量。

#### 内存泄漏 

`ThreadLocalMap` 中使用的 key 为 `ThreadLocal` 的弱引用,而 value 是强引用。所以，如果 `ThreadLocal` 没有被外部强引用的情况下，在垃圾回收的时候会 key 会被清理掉，而 value 不会被清理掉。这样一来，`ThreadLocalMap` 中就会出现key为null的Entry。假如我们不做任何措施的话，value 永远无法被GC 回收，这个时候就可能会产生内存泄露。ThreadLocalMap实现中已经考虑了这种情况，在调用 `set()`、`get()`、`remove()` 方法的时候，会清理掉 key 为 null 的记录。使用完 `ThreadLocal`方法后 最好手动调用`remove()`方法。

#### 使用

```
import com.xbhog.springbootvueblog.pojo.SysUser;
 
 public class UserThreadLocal {
     private  UserThreadLocal(){}
 
     private static final ThreadLocal<SysUser> LOCAL = new ThreadLocal<>();
 
     public static void put(SysUser sysUser){
         LOCAL.set(sysUser);
     }
     public static SysUser get(){
         return LOCAL.get();
     }
 
     public static void remove(){
         LOCAL.remove();
     }
 }
```





### BlockingQueue 

- ArrayBlockingQueue：由数组结构组成的有界阻塞队列.
- LinkedBlockingQueue：由链表结构组成的有界(但大小默认值Integer>MAX_VALUE)阻塞队列.
- PriorityBlockingQueue：支持优先级排序（堆）的无界阻塞队列.
- DelayQueue：使用优先级队列实现的延迟无界阻塞队列.
- SynchronousQueue：不存储元素的阻塞队列,也即是单个元素的队列. 每个插入操作必须等待另一个线程相应的删除操作，反之亦然。 同步队列没有任何内部容量，甚至没有一个容量。 个人感觉是生产者生产一个元素，消费者必须消费，生产者才能继续生产。内部也维护了一个TransferQueue，其中部分操作是利用cas。
- LinkedTransferQueue：基于链接节点的无界TransferQueue 。 这个队列相对于任何给定的生产者订购元素FIFO（先进先出）。 队列的头部是那些已经排队的元素是一些生产者的最长时间。 队列的尾部是那些已经在队列上的元素是一些生产者的最短时间。
- LinkedBlockingDuque：由了解结构组成的双向阻塞队列.

#### 常见方法

- 抛出异常：add/remove
- 不抛出异常：offer/poll
- 阻塞：put/take
- 带时间：offer/poll

### 生产者和消费者 

#### synchronized

```
public class Test {

    private final LinkedList<String> lists = new LinkedList<>();

    public synchronized void put(String s) {
        while (lists.size() != 0) { // 用while怕有存在虚拟唤醒线程
            // 满了， 不生产了
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        lists.add(s);
        System.out.println(Thread.currentThread().getName() + " " + lists.peekFirst());
        this.notifyAll(); // 这里可是通知所有被挂起的线程，包括其他的生产者线程
    }

    public synchronized void get() {
        while (lists.size() == 0) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName() + " " + lists.removeFirst());
        this.notifyAll(); // 通知所有被wait挂起的线程  用notify可能就死锁了。
    }

    public static void main(String[] args) {
        Test test = new Test();

        // 启动消费者线程
        for (int i = 0; i < 5; i++) {
            new Thread(test::get, "ConsA" + i).start();
        }

        // 启动生产者线程
        for (int i = 0; i < 5; i++) {
            int tempI = i;
            new Thread(() -> {
                test.put("" + tempI);
            }, "ProdA" + i).start();
        }
    }
}

```

#### ReentrantLock 

```
public class Test {

    private LinkedList<String> lists = new LinkedList<>();
    private Lock lock = new ReentrantLock();
    private Condition prod = lock.newCondition();
    private Condition cons = lock.newCondition();

    public void put(String s) {
        lock.lock();
        try {
            // 1. 判断
            while (lists.size() != 0) {
                // 只要队列有元素，就不生产了，就停会儿
                prod.await();
            }
            // 2.干活
            lists.add(s);
            System.out.println(Thread.currentThread().getName() + " " + lists.peekFirst());
            // 3. 通知
            cons.signalAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void get() {
        lock.lock();
        try {
            // 1. 判断
            while (lists.size() == 0) {
                // 队列为空，消费者肯定等待呀
                cons.await();
            }
            // 2.干活
            System.out.println(Thread.currentThread().getName() + " " + lists.removeFirst());
            // 3. 通知
            prod.signalAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        Test test = new Test();
        for (int i = 0; i < 5; i++) {
            int tempI = i;
            new Thread(() -> {
                test.put(tempI + "");
            }, "ProdA" + i).start();
        }
        for (int i = 0; i < 5; i++) {
            new Thread(test::get, "ConsA" + i).start();
        }
    }
}

```

这里讲一下为什么用while，不用if？ 假如，此时队列元素为空，那么消费者肯定都挂起来了哈。在挂起前通知了生产者线程去生产，那么，生产者产了一个之后唤醒消费者，所有消费者醒了以后，就一个消费者抢到锁，开始消费，当消费过后释放锁，其他消费者线程的某一个抢到锁之后，从唤醒处走代码，如果是if，往下走取元素发现队列空的，直接抛异常。如果是while的话，还会继续判断队列是否为空，空就挂起。不会抛异常。

#### BlockingQueue 

```
public class Test {
    public static void main(String[] args) {
        ArrayBlockingQueue<Object> queue = new ArrayBlockingQueue<>(10);
        // 生产者
        Runnable product = () -> {
            while (true) {
                try {
                    String s = "生产者：" + Thread.currentThread().getName() + " "+ new Object();
                    System.out.println(s);
                    queue.put(s);
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(product, "p1").start();
        new Thread(product, "p2").start();
        // 消费者
        Runnable consume = () -> {
            while (true) {
                try {
                    Object o = queue.take();
                    System.out.println("消费者：" + Thread.currentThread().getName() + " " + o);
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(consume, "c1").start();
        new Thread(consume, "c2").start();
    }
}

```

利用 BlockingQueue 实现生产者消费者模式的代码。虽然代码非常简单，但实际上 ArrayBlockingQueue 已经在背后完成了很多工作，比如队列满了就去阻塞生产者线程，队列有空就去唤醒生产者线程等。

### 线程池 

- **降低资源消耗**。通过重复利用已创建的线程降低线程创建和销毁造成的消耗。
- **提高响应速度**。当任务到达时，任务可以不需要要等到线程创建就能立即执行。
- **提高线程的可管理性**。线程是稀缺资源，如果无限制的创建，不仅会消耗系统资源，还会降低系统的稳定性，使用线程池可以进行统一的分配，调优和监控。

#### 线程池的使用

线程池的实现被封装到了ThreadPoolExecutor中，我们可以通过ThreadPoolExecutor的构造方法来实例化出一个线程池，代码如下：

```
// 实例化一个线程池
ThreadPoolExecutor executor = new ThreadPoolExecutor(3, 10, 60,
        TimeUnit.SECONDS, new ArrayBlockingQueue<>(20));
// 使用线程池执行一个任务        
executor.execute(() -> {
    // Do something
});
// 关闭线程池,会阻止新任务提交，但不影响已提交的任务
executor.shutdown();
// 关闭线程池，阻止新任务提交，并且中断当前正在运行的线程
executor.showdownNow();
```

创建好线程池后直接调用execute方法并传入一个Runnable参数即可将任务交给线程池执行，通过shutdown/shutdownNow方法可以关闭线程池。

Executors提供了很多简便的创建线程池的方法:

```
// 实例化一个单线程的线程池
ExecutorService singleExecutor = Executors.newSingleThreadExecutor();
// 创建固定线程个数的线程池
ExecutorService fixedExecutor = Executors.newFixedThreadPool(10);
// 创建一个可重用固定线程数的线程池
ExecutorService executorService2 = Executors.newCachedThreadPool();
```



#### **线程池的生命周期**

线程池从诞生到死亡，中间会经历RUNNING、SHUTDOWN、STOP、TIDYING、TERMINATED五个生命周期状态。

- **RUNNING** 表示线程池处于运行状态，能够接受新提交的任务且能对已添加的任务进行处理。RUNNING状态是线程池的初始化状态，线程池一旦被创建就处于RUNNING状态。
- **SHUTDOWN** 线程处于关闭状态，不接受新任务，但可以处理已添加的任务。RUNNING状态的线程池调用shutdown后会进入SHUTDOWN状态。
- **STOP** 线程池处于停止状态，不接收任务，不处理已添加的任务，且会中断正在执行任务的线程。RUNNING状态的线程池调用了shutdownNow后会进入STOP状态。
- **TIDYING** 当所有任务已终止，且任务数量为0时，线程池会进入TIDYING。当线程池处于SHUTDOWN状态时，阻塞队列中的任务被执行完了，且线程池中没有正在执行的任务了，状态会由SHUTDOWN变为TIDYING。当线程处于STOP状态时，线程池中没有正在执行的任务时则会由STOP变为TIDYING。
- **TERMINATED** 线程终止状态。处于TIDYING状态的线程执行terminated()后进入TERMINATED状态。

![1650159849246](../img/1650159849246.png)



#### 线程池的工作原理

线程池提交任务是从execute方法开始的，我们可以从execute方法来分析线程池的工作流程。

（1）当execute方法提交一个任务时，如果线程池中线程数小于corePoolSize,那么不管线程池中是否有空闲的线程，都会创建一个新的线程来执行任务。

![1650160164116](../img/1650160164116.png)

（2）当execute方法提交一个任务时，线程池中的线程数已经达到了corePoolSize,且此时没有空闲的线程，那么则会将任务存储到workQueue阻塞队列中。

![1650160194585](../img/1650160194585.png)

（3）如果execute提交任务时线程池中的线程数已经到达了corePoolSize,并且workQueue已满，那么则会创建新的线程来执行任务，但总线程数应该小于maximumPoolSize。

![1650160290203](../img/1650160290203.png)

（4）如果线程池中的线程执行完了当前的任务，则会尝试从workQueue阻塞队列中取出第一个任务来执行。如果workQueue为空则会阻塞线程。

![1650160314719](../img/1650160314719.png)

（5）如果execute提交任务时，线程池中的线程数达到了maximumPoolSize，且workQueue已满，此时会执行拒绝策略来拒绝接受任务。

![1650160344803](../img/1650160344803.png)

（6）如果线程池中的线程数超过了corePoolSize，那么空闲时间超过keepAliveTime的线程会被销毁，但程池中线程个数会保持为corePoolSize。

![1650160558670](../img/1650160558670.png)

（7）如果线程池存在空闲的线程，并且设置了allowCoreThreadTimeOut为true。那么空闲时间超过keepAliveTime的线程都会被销毁。

![1650160581767](../img/1650160581767.png)

`execute()`是如何执行任务的：

![](../img/execute.awebp)

以上`execute()`方法的执行步骤可以总结为3步：

1. 如果有效线程数workerCount小于核心线程数，则尝试增加一个线程执行当前任务，如果成功，则会在新线程中执行任务，如果失败，则执行下一步；
2. 如果线程池状态是running，则尝试加入到等待队列，如果入队成功，则需要重新检查线程池状态是否是running，如果已经不是running则要将任务从队列中remove并按照拒绝策略处理；如果重新检查线程池状态是running，则要判断workCount是不是等于0，如果等于0则需要创建一个新的Worker用于执行刚入队的任务；
3. 如果在第二步入队失败，会再次尝试增加一个Worker执行该任务，如果这里还是不行，则表示线程池确实shutdown或者等待队列满了，就执行拒绝策略。





#### FixedThreadPool 

```
/**
 * 创建一个可重用固定数量线程的线程池
 */
public static ExecutorService newFixedThreadPool(int nThreads, ThreadFactory threadFactory) {
    return new ThreadPoolExecutor(nThreads, nThreads,
                                  0L, TimeUnit.MILLISECONDS,
                                  new LinkedBlockingQueue<Runnable>(),
                                  threadFactory);
}

```

**从上面源代码可以看出新创建的 FixedThreadPool 的 corePoolSize 和 maximumPoolSize 都被设置为 nThreads，这个 nThreads 参数是我们使用的时候自己传递的。**

- 如果当前运行的线程数小于 corePoolSize， 如果再来新任务的话，就创建新的线程来执行任务；
- 当前运行的线程数等于 corePoolSize 后， 如果再来新任务的话，会将任务加入 `LinkedBlockingQueue`；
- 线程池中的线程执行完 手头的任务后，会在循环中反复从 `LinkedBlockingQueue` 中获取任务来执行；

不推荐使用

**FixedThreadPool 使用无界队列 LinkedBlockingQueue（队列的容量为 Intger.MAX_VALUE）作为线程池的工作队列会对线程池带来如下影响 ：**

- 当线程池中的线程数达到 `corePoolSize` 后，新任务将在无界队列中等待，因此线程池中的线程数不会超过 corePoolSize；
- 由于使用无界队列时 `maximumPoolSize` 将是一个无效参数，因为不可能存在任务队列满的情况。所以，通过创建 `FixedThreadPool`的源码可以看出创建的 `FixedThreadPool` 的 `corePoolSize` 和 `maximumPoolSize` 被设置为同一个值。
- 由于 1 和 2，使用无界队列时 `keepAliveTime` 将是一个无效参数；
- 运行中的 `FixedThreadPool`（未执行 `shutdown()`或 `shutdownNow()`）不会拒绝任务，在任务比较多的时候会导致 OOM（内存溢出）。

#### SingleThreadExecutor 

```
/**
 *返回只有一个线程的线程池
 */
public static ExecutorService newSingleThreadExecutor(ThreadFactory threadFactory) {
    return new FinalizableDelegatedExecutorService
        (new ThreadPoolExecutor(1, 1,
                                0L, TimeUnit.MILLISECONDS,
                                new LinkedBlockingQueue<Runnable>(),
                                threadFactory));
}

```

和上面一个差不多，只不过core和max都被设置为1

#### CachedThreadPool 

```
/**
 * 创建一个线程池，根据需要创建新线程，但会在先前构建的线程可用时重用它。
 */
public static ExecutorService newCachedThreadPool(ThreadFactory threadFactory) {
    return new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                                  60L, TimeUnit.SECONDS,
                                  new SynchronousQueue<Runnable>(),
                                  threadFactory);
}

```

`CachedThreadPool` 的` corePoolSize` 被设置为空（0），`maximumPoolSize `被设置为 Integer.MAX.VALUE，即它是无界的，这也就意味着如果主线程提交任务的速度高于 `maximumPool` 中线程处理任务的速度时，`CachedThreadPool` 会不断创建新的线程。极端情况下，这样会导致耗尽 cpu 和内存资源。

- 首先执行 `SynchronousQueue.offer(Runnable task)` 提交任务到任务队列。如果当前 `maximumPool` 中有闲线程正在执行 `SynchronousQueue.poll(keepAliveTime,TimeUnit.NANOSECONDS)`，那么主线程执行 offer 操作与空闲线程执行的 `poll` 操作配对成功，主线程把任务交给空闲线程执行，`execute()`方法执行完成，否则执行下面的步骤 2；
- 当初始 `maximumPool` 为空，或者 `maximumPool` 中没有空闲线程时，将没有线程执行 `SynchronousQueue.poll(keepAliveTime,TimeUnit.NANOSECONDS)`。这种情况下，步骤 1 将失败，此时 `CachedThreadPool` 会创建新线程执行任务，execute 方法执行完成；

#### ThreadPoolExecutor

- corePoolSize：核心线程数线程数定义了最小可以同时运行的线程数量。当任务提交到线程池时，如果线程池的线程数量还没有达到corePoolSize，那么就会新创建的一个线程来执行任务，如果达到了，就将任务添加到任务队列中。
- maximumPoolSize：当队列中存放的任务达到队列容量的时候，当前可以同时运行的线程数量变为最大线程数
- keepAliveTime：当线程数大于核心线程数时，多余的空闲线程存活的最长时间
- TimeUnit：时间单位
- BlockingQueue：当新任务来的时候会先判断当前运行的线程数量是否达到核心线程数，如果达到的话，信任就会被存放在队列中
- ThreadFactory：线程工厂，用来创建线程，一般默认即可
- RejectedExecutionHandler：拒绝策略

##### 线程池的拒绝策略：

> 当前提交任务数大于（maxPoolSize + queueCapacity）时就会触发线程池的拒绝策略

JDK中提供了RejectedExecutionHandler接口来执行拒绝操作。

```
public interface RejectedExecutionHandler {  
void rejectedExecution(Runnable r, ThreadPoolExecutor executor);  
} 
```

实现RejectedExecutionHandler的类有四个，对应了四种拒绝策略。分别如下：

- AbortPolicy：**中止策略**

  当触发拒绝策略时，直接抛出拒绝执行的异常，中止策略的意思也就是打断当前执行流程

  注意：

  ThreadPoolExecutor中默认的策略就是AbortPolicy，ExecutorService接口的系列ThreadPoolExecutor因为都没有显示的设置拒绝策略，所以默认的都是这个。

  ExecutorService中的线程池实例队列都是无界的，也就是说把内存撑爆了都不会触发拒绝策略。当自己自定义线程池实例时，使用这个策略一定要处理好触发策略时抛的异常，因为他会打断当前的执行流程。

  ```
  public static class AbortPolicy implements RejectedExecutionHandler {  
     public AbortPolicy() { }  
      public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {  
          throw new RejectedExecutionException("Task " + r.toString() +  
                                               " rejected from " +  
                                               e.toString());  
      }  
  } 
  ```

  ​

- CallerRunsPolicy：**调用者运行策略**

  **功能**：当触发拒绝策略时，只要线程池没有关闭，就由提交任务的当前线程处理。

  **使用场景**：一般在不允许失败的、对性能要求不高、并发量较小的场景下使用，因为线程池一般情况下不会关闭，也就是提交的任务一定会被运行，但是由于是调用者线程自己执行的，当多次提交任务时，就会阻塞后续任务执行，性能和效率自然就慢了。

  ```
  public static class CallerRunsPolicy implements RejectedExecutionHandler {  
  public CallerRunsPolicy() { }  
     public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {  
         if (!e.isShutdown()) {  
             r.run(); 
          } 
    }  
    } 
  ```

- DiscardPolicy：丢弃策略

  不处理新任务，直接丢弃掉。

  **使用场景**：如果你提交的任务无关紧要，你就可以使用它 。因为它就是个空实现，会悄无声息的吞噬你的的任务。所以这个策略基本上不用了

  ```
  public static class DiscardPolicy implements RejectedExecutionHandler {  
      public DiscardPolicy() { }  
      public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {  
      }  
  } 
  ```

- DiscardOldestPolicy：此策略将丢弃最早的未处理的任务请求。

  **功能**：如果线程池未关闭，就弹出队列头部的元素，然后尝试执行

  **使用场景**：这个策略还是会丢弃任务，丢弃时也是毫无声息，但是特点是丢弃的是老的未执行的任务，而且是待执行优先级较高的任务。

  基于这个特性，我能想到的场景就是，发布消息，和修改消息，当消息发布出去后，还未执行，此时更新的消息又来了，这个时候未执行的消息的版本比现在提交的消息版本要低就可以被丢弃了。因为队列中还有可能存在消息版本更低的消息会排队执行，所以在真正处理消息的时候一定要做好消息的版本比较。

  ```
  public static class DiscardOldestPolicy implements RejectedExecutionHandler {  
      public DiscardOldestPolicy() { }  
      public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {  
          if (!e.isShutdown()) {  
              e.getQueue().poll();  
              e.execute(r);  
          }  
      }  
  } 
  ```

   [拒绝策略应用场景](https://juejin.im/post/6844904067139895309)

![线程池参数关系](https://gitee.com/dreamcater/blog-img/raw/master/uPic/%e7%ba%bf%e7%a8%8b%e6%b1%a0%e5%8f%82%e6%95%b0%e5%85%b3%e7%b3%bb-JgjlWU.png)

### 线程池的线程数量怎么确定

> 运行时会有一些I/O操作，可能是读写文件，网络收发报文等，这些I/O操作在进行时需要等待反馈的。比如网络读写时，需要等待报文发送或者接收到，在这个等待过程中，线程是等待状态，cpu没有工作。此时操作系统就会调度cpu去执行其他线程的指令，这样就完美的利用了cpu这段空闲期，提高了cpu的利用率

1. 一般来说，如果是CPU密集型应用，则线程池大小设置为核心数+1。
2. 一般来说，如果是IO密集型应用，则线程池大小设置为2*核心数。
3. 在IO优化中，线程等待时间所占比例越高，需要越多线程，线程CPU时间所占比例越高，需要越少线程。这样的估算公式可能更适合：最佳线程数目 = （（线程等待时间+线程CPU时间）/线程CPU时间 ）* CPU数目

#设计模式

##  创建型模式

创建型模式的作用就是创建对象，说到创建一个对象，最熟悉的就是 new 一个对象，然后 set 相关属性。但是，在很多场景下，我们需要给客户端提供更加友好的创建对象的方式，尤其是那种我们定义了类，但是需要提供给其他开发者用的时候。

> 简单工厂模式最简单；工厂模式在简单工厂模式的基础上增加了选择工厂的维度，需要第一步选择合适的工厂；抽象工厂模式有产品族的概念，如果各个产品是存在兼容性问题的，就要用抽象工厂模式。单例模式就不说了，为了保证全局使用的是同一对象，一方面是安全性考虑，一方面是为了节省资源；建造者模式专门对付属性很多的那种类，为了让代码更优美；原型模式用得最少，了解和 Object 类中的 clone() 方法相关的知识即可。

### 单例模式

**为什么要有单例模式**

实际编程应用场景中，有一些对象其实我们只需要一个，比如线程池对象、缓存、系统全局配置对象等。这样可以就保证一个在全局使用的类不被频繁地创建与销毁，节省系统资源。

**实现单例模式的几个要点**

- 首先要确保全局只有一个类的实例。要保证这一点，至少类的构造器要私有化。
- 单例的类只能自己创建自己的实例。因为，构造器私有了，但是还要有一个实例，只能自己创建咯！
- 单例类必须能够提供自己的唯一实例给其他类。就是要有一个公共的方法能返回该单例类的唯一实例。

#### 饿汉式（线程安全）

```
public class Singleton {
    private static Singleton instance = new Singleton();
    private Singleton(){}
    public static Singleton getInstance(){
        return instance;
    }
}

```

饿汉模式代码比较简单，对象在类中被定义为private static，类加载时就初始化实例，避免了多线程同步问题，天然线程安全。通过getInstance()，通过java的classLoader机制保证了单例对象唯一。 

扩展问题：

- instance什么时候被初始化？

  Singleton类被加载的时候就会被初始化，java虚拟机规范虽然没有强制性约束在什么时候开始类加载过程，但是对于类的初始化，虚拟机规范则严格规定了有且只有四种情况必须立即对类进行初始化，遇到new、getStatic、putStatic或invokeStatic这4条字节码指令时，如果类没有进行过初始化，则需要先触发其初始化。 生成这4条指令最常见的java代码场景是：1）使用new关键字实例化对象2）读取一个类的静态字段（被final修饰、已在编译期把结果放在常量池的静态字段除外）3）设置一个类的静态字段（被final修饰、已在编译期把结果放在常量池的静态字段除外）4）调用一个类的静态方法

- class的生命周期

  class的生命周期一般来说会经历加载、连接、初始化、使用、和卸载五个阶段

- class的加载机制

  这里可以聊下classloader的双亲委派模型。

#### 懒汉式（线程不安全）

```
public class Singleton {
    private static Singleton singleton;
    private Singleton() {}
    public static Singleton getInstance() {
        if (singleton == null) {
            singleton = new Singleton();
        }
        return singleton;
    }
}

```

这是最基本的实现方式，第一次调用才初始化，实现了懒加载的特性。多线程场景下禁止使用，因为可能会产生多个对象，不再是单例。

#### 懒汉式（线程安全，方法上加同步锁）

```
public class Singleton {
    private static Singleton singleton;
    private Singleton() {}
    public static synchronized Singleton getInstance() {
        if (singleton == null) {
            singleton = new Singleton();
        }
        return singleton;
    }
}

```

和上面 懒汉式（线程不安全）实现上唯一不同是：获取实例的getInstance()方法上加了同步锁。保证了多线程场景下的单例。但是效率会有所折损，不过还好。

#### 双重校验锁DCL

```
public class Singleton {  
    private volatile static Singleton singleton;  
    private Singleton (){}  
    public static Singleton getSingleton() {  
    if (singleton == null) {  
        synchronized (Singleton.class) {  
        if (singleton == null) {  
            singleton = new Singleton();  
        }  
        }  
    }  
    return singleton;  
    }  
}  

```

此种实现中不用每次需要获得锁，减少了获取锁和等待的事件。

synchronized同步块里面能够保证只创建一个对象。但是通过在synchronized的外面增加一层判断，就可以在对象一经创建以后，不再进入synchronized同步块。这种方案不仅减小了锁的粒度，保证了线程安全，性能方面也得到了大幅提升。

同时这里要注意一定要说volatile，这个很关键，volatile一般用于多线程的可见性，但是这里是用来防止指令重排序的。

扩展：

- 为什么需要volatile？volatile有什么用？

  首先要回答可见性，这个是毋庸质疑的，然后可能又会考到java内存模型。

  防止指令重排序: 防止new Singleton时指令重排序导致其他线程获取到未初始化完的对象。instance = new Singleton()这句，这并非是一个原子操作，事实上在 JVM 中这句话大概做了下面 3 件事情。1.给 instance 分配内存2.调用 Singleton 的构造函数来初始化成员变量3.将instance对象指向分配的内存空间（执行完这步 instance 就为非 null 了）但是在 JVM 的即时编译器中存在指令重排序的优化。也就是说上面的第二步和第三步的顺序是不能保证的，最终的执行顺序可能是 1-2-3 也可能是 1-3-2。如果是后者，则在 3 执行完毕、2 未执行之前，被线程二抢占了，这时 instance 已经是非 null 了（但却没有初始化），所以线程二会直接返回 instance，然后使用，然后报错。

  顺便也可以说下volatie原理用内存屏障

- 讲讲synchronized和volatile的区别

  这里可以从synchroized能保证原子性，volatile不能保证说起，以及讲下synchroized是重量级锁，甚至可以所以下他和Lock的区别等等。

- 线程安全一般怎么实现的？

  - 互斥同步。如lock,synchroized
  - 非阻塞同步。如cas。
  - 不同步。如threadLocal,局部变量。

#### 静态内部类实现单例（线程安全，效率高）

```
public class Singleton {  
    private static class SingletonHolder {  
    private static final Singleton INSTANCE = new Singleton();  
    }  
    private Singleton (){}  
    public static final Singleton getInstance() {  
    return SingletonHolder.INSTANCE;  
    }  
}

```

这种方式下 Singleton 类被装载了，instance 不一定被初始化。因为 SingletonHolder 类没有被主动使用，只有通过显式调用 getInstance 方法时，才会显式装载 SingletonHolder 类，从而实例化 instance。

注意内部类SingletonHolder要用static修饰且其中的静态变量INSTANCE必须是final的。

#### 枚举式

```
public enum Singleton{
    INSTANCE;
}

```

默认枚举实例的创建是线程安全的，所以不需要担心线程安全的问题。同时他也是《Effective Java》中推荐的模式。最后通过枚举类，他能自动避免序列化/反序列化攻击，以及反射攻击(枚举类不能通过反射生成)。



### 工厂方法模式

> 要先了解一下简单工厂模式

简单工厂模式是创建一个工厂类XXXFactory，里面后一个静态方法，根据不同的参数，返回不同的派生自同一个父类（或实现同一个接口）的实例对象。

```
public interface ICourse {
    void course();
}
public class JavaCourse implements ICourse{
    @Override
    public void course() {
        System.out.println("这个是Java课程");
    }    
}
public class PythonCourse implements ICourse{
    @Override
    public void course() {
        System.out.println("这个是Python课程");
    }
}
public class CourseFactory {
    public ICourse createCourse(String courseName){
        if(courseName.equalsIgnoreCase("java")){
            return new JavaCourse();
        }else if(courseName.equalsIgnoreCase("python")){
            return new PythonCourse();
        }
        return null;
    }
}
public class Test {
    public static void main(String[] args) {
        ICourse course = new CourseFactory().createCourse("python");
        if(course!=null){
            course.course();
        }
    }
}
```

> 在Jdk中有一个日历类,它创建对象就用到的简单工厂模式
>
> ```
> public static Calendar getInstance()
> {
>     return createCalendar(TimeZone.getDefault(), Locale.getDefault(Locale.Category.FORMAT));
> }
> ```

之所以需要引入工厂方法模式，是因为我们往往需要使用两个或两个以上的工厂。工厂方法模式指定义一个创建工厂类的接口类,让实现这个接口的类来决定实例化哪个类,工厂方法模式让类的实例化推迟到子类中进行。

```
//定义一个工厂类的接口类
public interface ICourseFactory {
    ICourse create();
}
public class JavaFactory implements ICourseFactory{
    @Override
    public ICourse create() {
        return new JavaCourse();
    }
}
public class PythonFactory implements ICourseFactory{
    @Override
    public ICourse create() {
        return new PythonCourse();
    }
}
public class Test {
    public static void main(String[] args) {
        ICourse iCourse = new JavaFactory().create();
        iCourse.course();
    }

}
```

优点：一个类通过其子类来指定创建哪个对象，用户只需关心所需产品对应的工厂,无须关心创建细节，提高了系统的可扩展性。

缺点：类的个数容易过多,增加了代码结构的复杂度 -，增加了系统的抽象性和理解难度。

### 抽象工厂模式

当涉及到**产品族**的时候，就需要引入抽象工厂模式了。

一个经典的例子是造一台电脑。我们先不引入抽象工厂模式，看看怎么实现。

因为电脑是由许多的构件组成的，我们将 CPU 和主板进行抽象，然后 CPU 由 CPU工厂类 生产，主板由主板生产类生产，然后，我们再将 CPU 和主板搭配起来组合在一起。

![1650285544174](../img/1650285544174.png)

这个时候的客户端调用是这样的：

```
// 得到 Intel 的 CPU
CPUFactory cpuFactory = new IntelCPUFactory();
CPU cpu = intelCPUFactory.makeCPU();

// 得到 AMD 的主板
MainBoardFactory mainBoardFactory = new AmdMainBoardFactory();
MainBoard mainBoard = mainBoardFactory.make();

// 组装 CPU 和主板
Computer computer = new Computer(cpu, mainBoard);
```

但是，这种方式有一个问题，那就是如果 **Intel 家产的 CPU 和 AMD 产的主板不能兼容使用**，那么这代码就容易出错，因为客户端并不知道它们不兼容，也就会错误地出现随意组合。

> 产品族：代表了组成某个产品的一系列附件的集合：
>
> ![1650285695423](../img/1650285695423.png)

当涉及到这种产品族的问题的时候，就需要抽象工厂模式来支持了。我们不再定义 CPU 工厂、主板工厂、硬盘工厂、显示屏工厂等等，我们直接定义电脑工厂，每个电脑工厂负责生产所有的设备，这样能保证肯定不存在兼容问题。

![1650285726485](../img/1650285726485.png)

这个时候，对于客户端来说，不再需要单独挑选 CPU厂商、主板厂商、硬盘厂商等，直接选择一家品牌工厂，品牌工厂会负责生产所有的东西，而且能保证肯定是兼容可用的。

```
public static void main(String[] args) {
    // 第一步就要选定一个“大厂”
    ComputerFactory cf = new AmdFactory();
    // 从这个大厂造 CPU
    CPU cpu = cf.makeCPU();
    // 从这个大厂造主板
    MainBoard board = cf.makeMainBoard();
      // 从这个大厂造硬盘
      HardDisk hardDisk = cf.makeHardDisk();

    // 将同一个厂子出来的 CPU、主板、硬盘组装在一起
    Computer result = new Computer(cpu, board, hardDisk);
}
```



### 建造者模式

经常碰见的 XxxBuilder 的类，通常都是建造者模式的产物。建造者模式其实有很多的变种，但是对于客户端来说，我们的使用通常都是一个模式的：

```
Food food = new FoodBuilder().a().b().c().build();
Food food = Food.builder().a().b().c().build();
```

套路就是先 new 一个 Builder，然后可以链式地调用一堆方法，最后再调用一次 build() 方法，我们需要的对象就有了。

```
class User {
    // 下面是“一堆”的属性
    private String name;
    private String password;
    private String nickName;
    private int age;

    // 构造方法私有化，不然客户端就会直接调用构造方法了
    private User(String name, String password, String nickName, int age) {
        this.name = name;
        this.password = password;
        this.nickName = nickName;
        this.age = age;
    }
    // 静态方法，用于生成一个 Builder，这个不一定要有，不过写这个方法是一个很好的习惯，
    // 有些代码要求别人写 new User.UserBuilder().a()...build() 看上去就没那么好
    public static UserBuilder builder() {
        return new UserBuilder();
    }

    public static class UserBuilder {
        // 下面是和 User 一模一样的一堆属性
        private String  name;
        private String password;
        private String nickName;
        private int age;

        private UserBuilder() {
        }

        // 链式调用设置各个属性值，返回 this，即 UserBuilder
        public UserBuilder name(String name) {
            this.name = name;
            return this;
        }

        public UserBuilder password(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder nickName(String nickName) {
            this.nickName = nickName;
            return this;
        }

        public UserBuilder age(int age) {
            this.age = age;
            return this;
        }

        // build() 方法负责将 UserBuilder 中设置好的属性“复制”到 User 中。
        // 当然，可以在 “复制” 之前做点检验
        public User build() {
            if (name == null || password == null) {
                throw new RuntimeException("用户名和密码必填");
            }
            if (age <= 0 || age >= 150) {
                throw new RuntimeException("年龄不合法");
            }
            // 还可以做赋予”默认值“的功能
              if (nickName == null) {
                nickName = name;
            }
            return new User(name, password, nickName, age);
        }
    }
}

```

核心是：先把所有的属性都设置给 Builder，然后 build() 方法的时候，将这些属性**复制**给实际产生的对象。

看看客户端的调用：

```
public class APP {
    public static void main(String[] args) {
        User d = User.builder()
                .name("foo")
                .password("pAss12345")
                .age(25)
                .build();
    }
}

```



### 原型模式

原型模式很简单：有一个原型**实例**，基于这个原型实例产生新的实例，也就是“克隆”了。

Object 类中有一个 clone() 方法，它用于生成一个新的对象，当然，如果我们要调用这个方法，java 要求我们的类必须先**实现 Cloneable 接口**，此接口没有定义任何方法，但是不这么做的话，在 clone() 的时候，会抛出 CloneNotSupportedException 异常。

```
protected native Object clone() throws CloneNotSupportedException;
```

> java 的克隆是浅克隆，碰到对象引用的时候，克隆出来的对象和原对象中的引用将指向同一个对象。通常实现深克隆的方法是将对象进行序列化，然后再进行反序列化。



## 结构型模式

结构型模式旨在通过改变代码结构来达到解耦的目的，使得我们的代码容易维护和扩展。

### 代理模式

**静态代理角色分析**

- 抽象角色 : 一般使用接口或者抽象类来实现
- 真实角色 : 被代理的角色
- 代理角色 : 代理真实角色 ; 代理真实角色后 , 一般会做一些附属的操作 .
- 客户  :  使用代理角色来进行一些操作 .

**代码实现**

Rent . java 即抽象角色

```java
//抽象角色：租房
public interface Rent {
   public void rent();
}
```

Host . java 即真实角色

```java
//真实角色: 房东，房东要出租房子
public class Host implements Rent{
   public void rent() {
       System.out.println("房屋出租");
  }
}
```

Proxy . java 即代理角色

```java
//代理角色：中介
public class Proxy implements Rent {

   private Host host;
   public Proxy() { }
   public Proxy(Host host) {
       this.host = host;
  }

   //租房
   public void rent(){
       seeHouse();
       host.rent();
       fare();
  }
   //看房
   public void seeHouse(){
       System.out.println("带房客看房");
  }
   //收中介费
   public void fare(){
       System.out.println("收中介费");
  }
}
```

Client . java 即客户

```java
//客户类，一般客户都会去找代理！
public class Client {
   public static void main(String[] args) {
       //房东要租房
       Host host = new Host();
       //中介帮助房东
       Proxy proxy = new Proxy(host);

       //你去找中介！
       proxy.rent();
  }
}
```

分析：在这个过程中，你直接接触的就是中介，就如同现实生活中的样子，你看不到房东，但是你依旧租到了房东的房子通过代理，这就是所谓的代理模式，程序源自于生活，所以学编程的人，一般能够更加抽象的看待生活中发生的事情。

**静态代理的好处:**

- 可以使得我们的真实角色更加纯粹 . 不再去关注一些公共的事情 .
- 公共的业务由代理来完成 . 实现了业务的分工 ,
- 公共业务发生扩展时变得更加集中和方便 .

缺点 :

- 类多了 , 多了代理类 , 工作量变大了 . 开发效率降低 .

用一个代理来隐藏具体实现类的实现细节，通常还用于在真实的实现的前后添加一部分逻辑。

既然说是**代理**，那就要对客户端隐藏真实实现，由代理来负责客户端的所有请求。

代理模式说白了就是做 **“方法包装”** 或做 **“方法增强”**。在 AOP 中，其实就是动态代理的过程。比如 Spring 中，我们自己不定义代理类，但是 Spring 会帮我们动态来定义代理，然后把我们定义在 @Before、@After、@Around 中的代码逻辑动态添加到代理中。

说到动态代理，又可以展开说 …… Spring 中实现动态代理有两种，一种是如果我们的类定义了接口，如 UserService 接口和 UserServiceImpl 实现，那么采用 JDK 的动态代理，感兴趣的读者可以去看看 java.lang.reflect.Proxy 类的源码；另一种是我们自己没有定义接口的，Spring 会采用 CGLIB 进行动态代理，它是一个 jar 包，性能还不错。













> [21种](https://juejin.cn/post/6844903695667167240)