package com.resolution.config;

import com.resolution.service.ElasticSearchService;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 文件名：ElasticSearchClientConfig
 * 创建者:xiebaoming
 * 创建时间:2022/3/24  20:47
 * 描述：TODO
 */

@Configuration
public class ElasticSearchClientConfig extends ElasticSearchService {

    @Bean
    public RestHighLevelClient restHighLevelClient() {
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")
                )
        );
        return client;
    }

}
