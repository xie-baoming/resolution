package com.resolution.controller;

import com.google.code.kaptcha.Producer;
import com.resolution.pojo.User;
import com.resolution.service.SendMailService;
import com.resolution.service.UserService;
import com.resolution.util.ResolutionUtil;
import jdk.nashorn.internal.runtime.Context;
import org.apache.maven.surefire.shade.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.imageio.ImageIO;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 文件名：LoginController
 * 创建者:xiebaoming
 * 创建时间:2022/2/23  21:16
 * 描述：登录的映射配置
 */

@Controller
public class LoginController {

     @Autowired
     private UserService userService;

     @Autowired
     private SendMailService sendMailService;

     @Autowired
     private Producer kaptchaProducer;

     @Value("${server.servlet.context-path}")
     private String contextPath;

     private static final Logger logging= LoggerFactory.getLogger(LoginController.class);

     //登录页面
     @RequestMapping(path = "/login",method = RequestMethod.GET)
     public String getLoginPage(){
          return "site/login";
     }

     @RequestMapping(path = "/login",method = RequestMethod.POST)
     public String login(String username, String password, String code,boolean rememberme,
                         Model model, HttpSession session, HttpServletResponse response){

          String kaptcha = (String) session.getAttribute("kaptcha");
          if(!StringUtils.isNotBlank(code)){
               model.addAttribute("codeMsg","请输入验证码");
               return "/site/login";
          }

          if(!kaptcha.equalsIgnoreCase(code)){
               model.addAttribute("codeMsg","验证码不正确或已过期");
               return "/site/login";
          }
           //默认状态的登录凭证的超时时间
          int DEFAULT_EXPIRED_SECONDS = 3600 * 12;

          //记住状态的登录凭证超时时间
          int REMEMBER_EXPIRED_SECONDS = 3600 * 24 * 10;

          //检查账号和密码
          int expiredTime = rememberme ? REMEMBER_EXPIRED_SECONDS : DEFAULT_EXPIRED_SECONDS;
          Map<String, Object> map = userService.login(username, password, expiredTime);
          if(map.containsKey("ticket")){
               Cookie cookie=new Cookie("ticket", map.get("ticket").toString());
               cookie.setPath(contextPath);
               cookie.setMaxAge(expiredTime);
               response.addCookie(cookie);
               //重定向到归结推理首页
               return "redirect:/index";
          }
          else {
               model.addAttribute("usernameMsg",map.get("usernameMsg"));
               model.addAttribute("passwordMsg",map.get("passwordMsg"));
               return "/site/login";
          }
     }



     @RequestMapping(path = "/register", method = RequestMethod.GET)
     public String getRegisterPage(){
          //验证码校验
          //String kaptcha
          return "site/register";
     }

     @RequestMapping(path = "/register", method = RequestMethod.POST)
     public String register(Model model,HttpSession session ,User user,String emailCode){
          //自动将表单的值封装到了user对象里面
          if(StringUtils.isBlank(emailCode)){
               //发送激活邮件
               String mailCode = UUID.randomUUID().toString().replaceAll("-","").substring(0,4);
               session.setAttribute("mailCode",mailCode);
               sendMailService.sendMail(user.getEmail(),mailCode);
               model.addAttribute("mailCodeMsg","成功发送邮件，请注意查收");
               return "site/register";
          }else{
               String code = (String)session.getAttribute("mailCode");
               if(code.equals(emailCode)){
                    //激活码正确，开始激活
                    user.setCreateTime(new Date(System.currentTimeMillis()));
                    Map<String,Object> map = userService.register(user);
                    if(map == null || map.isEmpty()){  //没有错误信息
                         model.addAttribute("msg","激活成功,你的账号已经可以正常使用了");
                         model.addAttribute("target","/login");
                         return "site/operate-result";
                    }else{  //存在错误信息
                         model.addAttribute("usernameMsg",map.get("usernameMsg"));
                         model.addAttribute("passwordMsg",map.get("passwordMsg"));
                         model.addAttribute("emailMsg",map.get("emailMsg"));
                         return "site/register";
                    }

               }else{
                    model.addAttribute("mailCodeMsg","邮件激活码错误，请重新输入");
                    return "site/register";
               }
          }
     }


     /**
      * 验证码生成
      * @param response
      * @param session
      */
     @RequestMapping(path = "/kaptcha",method = RequestMethod.GET)
     public void getKaptcha(HttpServletResponse response, HttpSession session){
          //生成验证码
          String text=kaptchaProducer.createText();
          BufferedImage image = kaptchaProducer.createImage(text);

          //将验证码存入session
          session.setAttribute("kaptcha",text);

          //图片传给浏览器
          response.setContentType("image/png");
          try {
               OutputStream os = response.getOutputStream();
               ImageIO.write(image,"png",os);
          } catch (IOException e) {
               logging.error("响应验证码失败"+e.getMessage());
          }

     }

     //发送邮件
     @RequestMapping("/send")
     public String sendMail(String email, Model model,HttpSession session){
          model.addAttribute("mailCodeMsg","邮件成功发送，请注意查看");
          String mailCode = ResolutionUtil.generateUUID();
          session.setAttribute("mailCode",mailCode);
          sendMailService.sendMail(email,mailCode);
          return "site/register";
     }

     @RequestMapping(path = "/logout",method = RequestMethod.GET)
     public String logout(@CookieValue("ticket") String ticket){
          userService.logout(ticket);
          SecurityContextHolder.clearContext();
          return "redirect:/login";
     }

}
