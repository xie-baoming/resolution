package com.resolution.controller;

import com.resolution.event.EventProducer;
import com.resolution.pojo.Comment;
import com.resolution.pojo.DiscussPost;
import com.resolution.pojo.Event;
import com.resolution.pojo.User;
import com.resolution.service.CommentService;
import com.resolution.service.DiscussPostService;
import com.resolution.service.LikeService;
import com.resolution.service.UserService;
import com.resolution.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.rmi.activation.ActivationGroupDesc;
import java.util.*;

/**
 * 文件名：DiscussPostController
 * 创建者:xiebaoming
 * 创建时间:2022/3/5  16:43
 * 描述：TODO
 */


@Controller
@RequestMapping("/discuss")
public class DiscussPostController implements ResolutionConstant {
    @Autowired
    private DiscussPostService discussPostService;

    @Autowired
    private UserService userService;

    @Autowired
    private LikeService likeService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private HostHolder hostHolder;

    @Autowired
    private EventProducer eventProducer;

    @Autowired
    private RedisTemplate redisTemplate;


    @RequestMapping(path = "/add",method = RequestMethod.POST)
    @ResponseBody
    public String addDiscussPost(String title, String content){
        User user = hostHolder.getUser();
        if(user==null){
            return ResolutionUtil.getJSONString(403, "没有登录!");
        }
        DiscussPost post = new DiscussPost();
        post.setUserId(user.getId());
        post.setTitle(title);
        post.setContent(content);
        post.setCreateTime(new Date());
        discussPostService.addDiscussPost(post);
       /* Map<String, Object> map=new HashMap<>();
        map.put("row",discussPostService.addDiscussPost(post));*/
        // 报错的情况,将来统一处理.
        //触发发帖事件
        DiscussPost post1 = discussPostService.findDiscussPostByTitle(title);
        System.out.println("触发发帖事件");
        Event event=new Event()
                .setTopic(TOPIC_PUBLISH)
                .setUserId(user.getId())
                .setEntityType(ENTITY_TYPE_POST)
                .setEntityId(post1.getId());
        System.out.println("postId:"+post1.getId());
        eventProducer.fireEvent(event);

        //计算帖子分数
        String redisKey= RedisKeyUtil.getPostScoreKey();
        redisTemplate.opsForSet().add(redisKey,post.getId());


        return ResolutionUtil.getJSONString(0,"发布成功");
    }

    @RequestMapping(path = "/detail/{discussPostId}",method = RequestMethod.GET)
    public String getDisscussPost(@PathVariable("discussPostId") int discussPostId, Model model, Page page){
        //查询帖子
        DiscussPost post = discussPostService.findDiscussPostById(discussPostId);
        model.addAttribute("post",post);
        // 查询作者
        User user = userService.findUserById(post.getUserId());
        model.addAttribute("user",user);
        //点赞
        long likeCount = likeService.findEntityLikeCount(1,discussPostId);
        model.addAttribute("likeCount",likeCount);
        //点赞状态
        int likeStatus = hostHolder.getUser() == null ? 0 :
                likeService.findEntityLikeStatus(hostHolder.getUser().getId(), 1,discussPostId);
        model.addAttribute("likeStatus", likeStatus);

        //评论分页信息
        page.setLimit(5);
        page.setPath("/discuss/detail/" + discussPostId);
        page.setRows(post.getCommentCount());

        //评论：给帖子的评论 ：cvo
        //回复：给评论的评论 ：rvo
        List<Comment> commentList = commentService.findCommentByEntity(
                1,post.getId(),page.getOffset(),page.getLimit());

        List<Map<String, Object>> commentVoList = new ArrayList<>();
        if(commentList != null){
            System.out.println("333");
            for(Comment comment : commentList){
                //评论vo
                Map<String,Object> commentVo = new HashMap<>();
                //评论
                commentVo.put("comment",comment);
                //作者
                commentVo.put("user",userService.findUserById(comment.getUserId()));
                //点赞数量
                likeCount = likeService.findEntityLikeCount(2,comment.getId());
                commentVo.put("likeCount",likeCount);
                //点赞状态
                likeStatus = hostHolder.getUser() == null ? 0 :
                        likeService.findEntityLikeStatus(hostHolder.getUser().getId(), 2,comment.getId());
                 commentVo.put("likeStatus",likeStatus);

                //回复列表
                System.out.println(post);
                List<Comment> replyList = commentService.findCommentByEntity(
                        2,comment.getId(),page.getOffset(),page.getLimit()
                );
                //回复vo列表
                List<Map<String,Object>> replyVoList = new ArrayList<>();
                System.out.println(replyList == null);
                if(replyList != null){
                    System.out.println(replyList);
                    System.out.println("222");
                    for(Comment reply : replyList){
                        System.out.println("111");
                        System.out.println(replyList);
                        Map<String,Object> replyVo = new HashMap<>();
                        //回复
                        replyVo.put("reply", reply);
                        //作者
                        replyVo.put("user", userService.findUserById(reply.getUserId()));
                        //回复目标
                        System.out.println(reply.getTargetId());
                        System.out.println(reply);

                        //点赞数量
                        likeCount = likeService.findEntityLikeCount(2,reply.getId());
                        replyVo.put("likeCount",likeCount);
                        //点赞状态
                        likeStatus = hostHolder.getUser() == null ? 0 :
                                likeService.findEntityLikeStatus(hostHolder.getUser().getId(), 2,reply.getId());
                        replyVo.put("likeStatus",likeStatus);

                        User target = reply.getTargetId() == 0 ? null : userService.findUserById(reply.getTargetId());
                        replyVo.put("target", target);

                        replyVoList.add(replyVo);
                    }
                    commentVo.put("replys", replyVoList);

                    //回复数量
                    int replyCount = commentService.findCommentCount(2,comment.getId());
                    commentVo.put("replysCount", replyCount);
                }

                commentVoList.add(commentVo);
            }

        }
        model.addAttribute("comments", commentVoList);
        return "/site/discuss-detail";
    }

    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ResponseBody
    public String setDelete(Integer id){
        discussPostService.updateStatus(id,2);
        //触发删帖事件
        Event event=new Event()
                .setTopic(TOPIC_DELETE)
                .setUserId(hostHolder.getUser().getId())
                .setEntityType(ENTITY_TYPE_POST)
                .setEntityId(id);
        eventProducer.fireEvent(event);

        return ResolutionUtil.getJSONString(0);
    }


}
