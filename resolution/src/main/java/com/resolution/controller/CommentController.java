package com.resolution.controller;

import com.resolution.event.EventProducer;
import com.resolution.pojo.Comment;
import com.resolution.pojo.DiscussPost;
import com.resolution.pojo.Event;
import com.resolution.service.CommentService;
import com.resolution.service.DiscussPostService;
import com.resolution.util.HostHolder;
import com.resolution.util.RedisKeyUtil;
import com.resolution.util.ResolutionConstant;
import org.apache.lucene.search.join.ToParentBlockJoinQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

/**
 * 文件名：CommentController
 * 创建者:xiebaoming
 * 创建时间:2022/3/7  11:06
 * 描述：TODO
 */
@Controller
@RequestMapping("/comment")
public class CommentController implements ResolutionConstant {

    @Autowired
    private CommentService commentService;

    @Autowired
    private HostHolder hostHolder;

    @Autowired
    private EventProducer eventProducer;

    @Autowired
    private DiscussPostService discussPostService;

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping(path = "/add/{discussPostId}", method = RequestMethod.POST)
    public String addComment(@PathVariable("discussPostId") int discussPostId, Comment comment){
         comment.setUserId(hostHolder.getUser().getId());
         comment.setStatus(0);
         comment.setCreateTime(new Date());
         commentService.addComment(comment);

         //触发评论事件
        Event event = new Event()
                .setTopic("comment")
                .setUserId(hostHolder.getUser().getId())
                .setEntityId(comment.getEntityId())
                .setEntityType(comment.getEntityType())
                .setData("postId", discussPostId);

        if(comment.getEntityType() == ENTITY_TYPE_POST){
            DiscussPost target = discussPostService.findDiscussPostById(comment.getEntityId());
            event.setEntityUserId(target.getUserId());
        }else if(comment.getEntityType() == ENTITY_TYPE_Comment){
             Comment target = commentService.findCommentById(comment.getEntityId());
             event.setEntityUserId(target.getUserId());
        }
        eventProducer.fireEvent(event);

        //不处理将评论上传到ES
        /*if(comment.getEntityType() == ENTITY_TYPE_POST){
            //触发发帖事件
            event = new Event()
                    .setTopic(TOPIC_PUBLISH)
                    .setUserId(comment.getUserId())
                    .setEntityType(ENTITY_TYPE_POST)
                    .setEntityId(discussPostId);
            eventProducer.fireEvent(event);
        }*/

        if(comment.getEntityType() == ENTITY_TYPE_POST){
            //计算帖子分数
            String redisKey = RedisKeyUtil.getPostScoreKey();
            redisTemplate.opsForSet().add(redisKey,discussPostId);
        }

         return "redirect:/discuss/detail/" + discussPostId;

    }
}
