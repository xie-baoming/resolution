package com.resolution.controller;


import com.resolution.pojo.DiscussPost;
import com.resolution.service.ElasticSearchService;
import com.resolution.service.LikeService;
import com.resolution.service.UserService;
import com.resolution.util.Page;
import com.resolution.util.ResolutionConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xiebaoming
 * @version 1.0
 * @Description No Description
 */

@Controller
public class SearchController extends ElasticSearchService implements ResolutionConstant {

    @Autowired
    private ElasticSearchService elasticSearchService;

    @Autowired
    private UserService userService;

    @Autowired
    private LikeService likeService;


    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(String keyword, Page page, Model model) throws IOException {
        //搜索帖子
        List<Map<String,Object>> discussPosts = elasticSearchService.search(keyword, page.getCurrent() - 1, page.getLimit());

        model.addAttribute("discussPost",discussPosts);
        model.addAttribute("keyword",keyword);

        //设置分页信息
        page.setPath("/search?keyword="+keyword);
        page.setRows(discussPosts==null?0:  discussPosts.size());

        return "/site/search";
    }


}
