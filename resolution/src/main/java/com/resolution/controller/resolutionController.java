package com.resolution.controller;

import com.resolution.service.Resolution;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 文件名：resolutionController
 * 创建者:xiebaoming
 * 创建时间:2022/3/1  13:28
 * 描述：归结推理页面的控制器
 */
@Controller
public class resolutionController {
    @RequestMapping(path = "resolution", method = RequestMethod.GET)
    public String getResolutionPage(){
        return "site/resolution";
    }

    @RequestMapping(path = "resolution", method = RequestMethod.POST)
    public String resolution(Model model,String text){
        model.addAttribute("text",text);
        String newtText = text.replace("\r\n","");   //去掉换行
        System.out.println(newtText);
        String content = "";
        content += "=============开始将谓词公式转换为字句集=============";
        content += "<br/>";
        String[] strs = newtText.split(";");
        int i = 1;
        for(String str : strs){
            System.out.println(str);
            content += i;
            content += ".  ";
            i++;
            content += Resolution.generate(str);
            content += "<br/>";
        }
        model.addAttribute("content",content);
        return "/site/resolution";
    }
}
