package com.resolution.controller;

import com.resolution.pojo.DiscussPost;
import com.resolution.pojo.User;
import com.resolution.service.DiscussPostService;
import com.resolution.service.LikeService;
import com.resolution.service.UserService;
import com.resolution.util.HostHolder;
import com.resolution.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文件名：HomeController
 * 创建者:xiebaoming
 * 创建时间:2022/3/5  19:18
 * 描述：TODO
 */
@Controller
public class HomeController {
    @Autowired
    private DiscussPostService discussPostService;

    @Autowired
    private UserService userService;

    /*@Autowired
    private MessageService messageService;*/

    @Autowired
    private HostHolder hostHolder;

    @Autowired
    private LikeService likeService;

    @RequestMapping("/")
    public String getFirstPage(){
        return "redirect:/home";
    }

    @RequestMapping(path = "/home")
    public String getHomePage(){
        return "/site/home";
    }


    @RequestMapping(path = "/index")
    public String getIndexPage(Model model, Page page, @RequestParam(name = "orderMode",defaultValue = "0") Integer orderMode){
        // 方法调用前，SpringMVC 会自动实例化 Model 和 Page(实体对象)，并将 Page 注入 Model，
        // 所以，在 thymeleaf 中可以直接访问到 page 对象中的数据
        page.setRows(discussPostService.findDiscussPostRows(0));
        page.setPath("/index");

        List<DiscussPost> list = discussPostService.findDiscussionPosts(0, page.getOffset(), page.getLimit(),orderMode);
        List<Map<String, Object>> discussPosts=new ArrayList<>();
        if(list!= null){
            for (DiscussPost discussPost : list) {
                Map<String, Object> map = new HashMap<>();
                map.put("post",discussPost);
                User user= userService.findUserById(discussPost.getUserId());
                map.put("user",user);

                long likeCount = likeService.findEntityLikeCount(1, discussPost.getId());
                map.put("likeCount",likeCount);
                discussPosts.add(map);
            }
        }
        model.addAttribute("discussPosts",discussPosts);
        return "/site/index";
    }
}
