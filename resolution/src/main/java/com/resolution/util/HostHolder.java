package com.resolution.util;

import com.resolution.pojo.User;
import org.springframework.stereotype.Component;

/**
 * 文件名：HostHolder
 * 创建者:xiebaoming
 * 创建时间:2022/3/4  19:19
 * 描述：TODO
 */
//持有用户信息，用于代替session对象
//以线程方式存取对象
@Component
public class HostHolder {
    private ThreadLocal<User> users = new ThreadLocal<>();

    public void setUser(User user){
        users.set(user);
    }

    public User getUser(){
        return users.get();
    }

    public void clear(){
        users.remove();
    }
}
