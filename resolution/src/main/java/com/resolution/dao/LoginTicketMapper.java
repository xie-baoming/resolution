package com.resolution.dao;

import com.resolution.pojo.LoginTicket;
import org.apache.ibatis.annotations.*;

/**
 * 文件名：LoginTicketMapper
 * 创建者:xiebaoming
 * 创建时间:2022/2/24  15:11
 * 描述：TODO
 */
@Mapper
public interface LoginTicketMapper {
    /**
     *插入登录凭证
     */
    @Insert({
            "insert into login_ticket(user_id,ticket,status,expired) ",
            "values(#{userId},#{ticket},#{status},#{expired})"
    })
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insertLoginTicket(LoginTicket loginTicket);
    /**
     *查询
     */
    @Select({
            "select id,user_id,ticket,status,expired ",
            "from login_ticket where ticket=#{ticket}"
    })
    LoginTicket selectLoginTicket(String ticket);
    /**
     * 软删除
     */
    @Update({
            "<script>",
            "update login_ticket set status=#{status} where ticket=#{ticket} ",
            "<if test=\"ticket!=null\"> ",
            "and 1=1 ",
            "</if>",
            "</script>"
    })
    int updateStatus(String ticket,int status);
}
