package com.resolution.dao;

import com.resolution.pojo.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件名：UserMapper
 * 创建者:xiebaoming
 * 创建时间:2022/2/23  21:20
 * 描述：TODO
 */
@Mapper
public interface UserMapper {

    User selectById(int id);

    User selectByName(String username);

    User selectByEmail(String email);

    int insertUser(User user);

    int updateName(int id, String name);

    int updatePassword(int id, String password);

    int updateHeaderUrl(int id,String headerUrl);

    int deleteById(int id);
}
