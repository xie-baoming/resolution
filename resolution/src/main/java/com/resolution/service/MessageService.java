package com.resolution.service;

import com.resolution.dao.MessageMapper;
import com.resolution.pojo.Message;
import com.resolution.util.SensitiveFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import java.util.List;

/**
 * 文件名：MessageService
 * 创建者:xiebaoming
 * 创建时间:2022/3/9  10:36
 * 描述：TODO
 */
@Service
public class MessageService extends Message {

    @Autowired(required = false)
    private MessageMapper messageMapper;

    @Autowired
    private SensitiveFilter sensitiveFilter;


    public List<Message> findConversations(Integer userId, Integer offset, Integer limit) {
        return messageMapper.selectConversations(userId, offset, limit);
    }


    public int findConvesationCount(int userId) {
        return messageMapper.selectConversasionCount(userId);
    }


    public List<Message> findLetters(String conversationId, Integer offset, Integer limit) {
        return messageMapper.selectLetters(conversationId, offset, limit);
    }


    public int findLetterCount(String conversationId) {
        return messageMapper.selectLetterCount(conversationId);
    }


    public int findLetterUnreadCount(Integer userId, String conversationId) {
        return messageMapper.selectLetterUnreadCount(userId, conversationId);
    }


    public int addMessage(Message message) {
        message.setContent(HtmlUtils.htmlEscape(message.getContent()));
        message.setContent(sensitiveFilter.filter(message.getContent()));
        return messageMapper.insertMessage(message);
    }


    public int readMessage(List<Integer> ids) {
        return messageMapper.updateStatus(ids,1);
    }

    //查询最新通知
    public Message findLatestNotice(Integer userId, String topic) {
        return messageMapper.selectLastestNotice(userId, topic);
    }

    //查询通知数量
    public int findNoticeCount(Integer userId, String topic) {
        return messageMapper.selectNoticeCount(userId, topic);
    }

    //查询未读数量
    public int findNoticeUnreadCount(Integer userId, String topic) {
        return messageMapper.selectNoticeUnreadCount(userId, topic);
    }

    //查询通知列表
    public List<Message> findNotices(Integer userId, String topic, Integer offset, Integer limit) {
        return messageMapper.selectNotices(userId, topic, offset, limit);
    }
}
