package com.resolution.service;

import com.resolution.util.MailClient;
import com.resolution.util.ResolutionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Map;

/**
 * 文件名：SendMailService
 * 创建者:xiebaoming
 * 创建时间:2022/2/24  20:43
 * 描述：TODO
 */
@Service
public class SendMailService {

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private MailClient mailClient;

    public void sendMail(String toMail,String mailCode){
        Context context=new Context();
        context.setVariable("email",toMail);
        context.setVariable("mailCode",mailCode);
        //注入数据
        String content =templateEngine.process("/mail/activation",context);
        mailClient.sendMail(toMail,"激活账号",content);

    }
}
