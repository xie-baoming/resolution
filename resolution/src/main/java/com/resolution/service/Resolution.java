package com.resolution.service;


import java.io.*;
import java.util.*;

/**
 * 文件名：Resolution
 * 创建者:xiebaoming
 * 创建时间:2022/2/26  21:43
 * 描述：TODO
 */
public class Resolution {


    private static boolean check(String s){
        int nums1 = 0;
        int nums2 = 0;
        for(char c : s.toCharArray()){
            if(c == '('){
                nums1++;
            }
            if(c == ')'){
                nums2++;
            }
        }
        return nums1 == nums2;
    }

    StringBuilder sb = new StringBuilder();
    private static String step1(String s){
        //消去"->"和"<->"连接词
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) == '-' && s.charAt(i+1) == '>'){
                int j = i - 1;  //首先寻找"->"右边的")"对应的"(",j移动到该位置
                int nums = 0;   //nums表示")"的数量
                while(j > 0){
                    if(s.charAt(j) == ')'){
                        nums++;
                    }
                    if(s.charAt(j) == '('){
                        nums--;
                        if(nums == 0){  //此时已找到
                            break;
                        }
                    }
                }
                //移动到分割位置
                j--;

            }
        }
        return s;
    }

    public static void print1(List<String> vec){   //有间隔打印
        for(Object s : vec.toArray()){
            System.out.print(s+"  ");
        }
    }

    public static String print2(List<String> vec){   //无间隔打印
        String temp = "";
        for(Object s : vec.toArray()){
            temp += s;
            System.out.print(s);
        }
        return temp;
    }

    public static void create(String str, List<String>vec){
        int NUMCHAR = 15;
        char[] ch = new char[NUMCHAR];
        int i,j=0,k,num=0;
        for(k=0;k<NUMCHAR;k++)
            ch[k]=' ';
        for(i=0;i<str.length();i++)   //扫描字符串
        {
            if(Character.isLetterOrDigit(str.charAt(i))||str.charAt(i)==','||str.charAt(i)=='∀'||str.charAt(i)=='∃')//是字母或者是','就继续添加
                ch[j++]=str.charAt(i);
            else if(str.charAt(i)=='(')  //开始符'('
            {
                if(j != 0)  //此时有可能是~(,∧(,∨(,((,或者有可能是P(这种单谓词形式
                {
                    //String cha = "∀∃∧∨";
                    if(String.valueOf(ch).equals("~") || String.valueOf(ch).equals("∧")
                            || String.valueOf(ch).equals("∨") || String.valueOf(ch).equals("("))
                    {
                        vec.add(String.valueOf(ch));   //添加进结果
                        j=0;    //置初始状态
                        //num=0;
                        for(k=0;k<NUMCHAR;k++)  //清空
                            ch[k]=' ';
                        vec.add("(");    //添加'('
                    }
                    else   //单谓词形式或者∀(就继续添加
                    {
                        //num++;  //新增
                        ch[j++]=str.charAt(i);
                    }
                }
                else   //如果'('前面是空说明是一个新的开始
                {
                    vec.add("(");
                    //num=0; //新增
                    //continue;
                }
            }
            else if(str.charAt(i)==')') //结束符')'
            {
                if(j != 0)///如果String不空
                {
                    String temp = String.valueOf(ch);
                    if(temp.contains("∀") || temp.contains("∃")){   //此时是∀x)这种情况，应该分开
                        vec.add(temp);
                        vec.add(")");
                        j = 0;
                    }else{    //此时是单谓词
                        ch[j++] = str.charAt(i);
                        vec.add(String.valueOf(ch));
                        j=0;
                    }
                    //清空
                    for(k=0;k<NUMCHAR;k++)
                        ch[k]=' ';
                }
                else if(j == 0) //若是空则是一个单独的终结
                {
                    vec.add(")");
                    //num=0; //新增
                    //continue;
                }
            }
            else if(str.charAt(i)=='<')///若是'<'证明是蕴涵式"<->"
            {
                i=i+2;
                vec.add("<->");
            }
            else if(str.charAt(i)=='-')///若是'-'证明是蕴涵式"->"
            {
                i=i+1;
                vec.add("->");
            }
            else if(str.charAt(i) == '~' || str.charAt(i) == '∨' || str.charAt(i) == '∧'){
                String temp = "";
                temp += str.charAt(i);
                if(j != 0) {
                    vec.add(String.valueOf(ch));
                    vec.add(temp);
                    j = 0;
                    //num=0;/新增
                    for (k = 0; k < NUMCHAR; k++)
                        ch[k] = ' ';
                }else{
                    vec.add(temp);
                }
            }
        }
        for(int z = 0; z < vec.size(); z++){
            String temp = vec.get(z);
            temp = temp.trim();
            vec.set(z,temp);
        }
    }

    public static void firstStep(List<String> vec){
        int i,j;
        ///消除<->和->
        for(i=0;i<vec.size();)
        {
            if(vec.get(i).equals("<->") || vec.get(i).equals("->"))    //寻找
            {
                ///j=i;
                //String cha = "∀∃∧∨";
                //分为strPreA (A -> B ) strFloB   ===>  strPreA ( ~A ∧ B ) strFloB
                //分为strPreA (A <-> B ) strFloB  ==>   strPreA ( ~A ∨ B ) ∧  ( A ∨ ~B ) strFloB
                String strPreA = ""; //A以前的字符串
                String strA = "";     //A
                String strB = "";     //B
                String strFloB = "";  //B以后的字符串
                String strTemp = "";
                int num=0,k;
                ///一下是寻找A
                for(k = i - 1; k >= 0; k--) //向前搜索(和)的匹配
                {
                    if(vec.get(k).equals(")")) num++;
                    if(vec.get(k).equals("(")) num--;
                    if(num == 0) break;
                }
                if(k>=0)
                    for( k = k - 1; k >= 0; k--) //向前搜索量词
                    {
                        if(!(vec.get(k).charAt(0) == '∀' || vec.get(k).charAt(0) == '∃')) {
                            break;
                        }
                    }
                //到此已经找到A (k+1--i-1)
                if(k>=0)///没有到头
                {
                    for(j = 0; j <= k; j++)///找到strPreA (0-k)
                    {
                        strPreA = strPreA + vec.get(j);
                    }
                    for(j = k + 1; j < i; j++)///找到A (k+1---i-1)
                    {
                        strA=strA + vec.get(j);
                    }
                }
                else///到头了
                {
                    strPreA = "";
                    for(j = k + 1; j < i; j++)
                    {
                        strA=strA + vec.get(j);
                    }
                }
                //然后是寻找B (i+1----k)
                for(k = i + 1; k < vec.size(); k++)//向后搜索量词
                {
                    if(!(vec.get(i).charAt(0) == '∀' || vec.get(i).charAt(0) == '∃')) break;
                }
                for(;k < vec.size(); k++)//向后搜索（和）匹配
                {
                    if(vec.get(k).equals("(")) num++;
                    if(vec.get(k).equals(")")) num--;
                    if(num==0) break;
                }
                ///到此已经找到B
                for(j = i + 1; j < k + 1; j++)///找到B (i+1---k)
                {
                    strB=strB + vec.get(j);
                }
                for(j = k + 1; j < vec.size(); j++)///B后面的字符串(k+1----end)
                {
                    strFloB = strFloB + vec.get(j);
                }
                //System.out.println("strPreA:  "+strPreA);
                //System.out.println("strA:  "+strA);
                //System.out.println("strB:  "+strB);
                //System.out.println("strFloB:  "+strFloB);
                if(vec.get(i).equals("->"))
                    //String cha = "∀∃∧∨";
                    ///利用公式：A->B <=> ~(A) ∨ B
                    strTemp = strPreA + "~" + strA + "∨" + strB + strFloB;
                if(vec.get(i).equals("<->"))
                    ///利用公式：A<->B<=>( ~(A) ∨ B) ∧ ( ~(B) ∨ A)
                    strTemp = strPreA + "(" + "~(" + strA + ")" + "∨" + strB +")" +
                            "∧" + "(" + "~(" + strB + ")" + "∨" + strA + ")" + strFloB;
                //System.out.println(strTemp);
                strTemp = strTemp.replace("~~","");
                vec.clear();   //请空全部函数
                create(strTemp,vec);   //继续调用分割函数
                i=0;
                continue;
            }
            i++;
        }
    }

    public static void secondStep(List<String> vec){
        int i,j;
        ///减少~辖域
        //假设谓词结构为strPreA (A 公式 B ) strFloB
        for(i = 0; i < vec.size();)
        {
            if(vec.get(i).equals("~"))    //开始寻找~
            {
                ///j=i;
                //String cha = "∀∃∧∨";
                int num=0,k;
                //然后是寻找~后面的部分B (i+1----k)，k为最后的位置
                k = i + 1;
                if(vec.get(k).charAt(0) == '∀' || vec.get(k).charAt(0) == '∃'){  //跳过量词,如∀x
                    k++;
                }
                if( k + 2 < vec.size() && vec.get(k).equals("(") && vec.get(k + 2).equals(")")){   //跳过量词,如(∀x)
                    if(vec.get(k+1).charAt(0) == '∀' || vec.get(k+1).charAt(0) == '∃'){
                            k = k + 3;
                    }
                }
                for(;k < vec.size(); k++)//向后搜索（和）匹配，搜索~后面的作用域
                {
                 //   System.out.println(vec.get(k));
                    if(vec.get(k).equals("(")) num++;
                    if(vec.get(k).equals(")")) num--;
                    if( num == 0) break;
                }
                //到此已经找到作用域为（i+1，k）
                //将旧公式装到一个新公式
                //先将~前的
                String strTemp = "";
                for(j = 0; j < i; j++){
                    strTemp += vec.get(j);
                }
                for(j = i + 1; j < k + 1; j++) //找到B (i+1---k)
                {
                     //System.out.println("in:");
                    // System.out.println(vec.get(j));
                    if(vec.get(j).equals("(") || vec.get(j).equals(")") ||vec.get(j).equals("~")){  //碰到'（'和'）'和'~'不处理
                        strTemp += vec.get(j);
                    }else if(vec.get(j).equals("∧")){  //和取变析取
                        strTemp += "∨";
                    }else if(vec.get(j).equals("∨")){  //析取变合取
                        strTemp += "∧";
                    }else if(vec.get(j).charAt(0) == '∀'){  //全称变特称
                       // System.out.println("333");
                        String temp = vec.get(j);
                        temp = temp.replace('∀','∃');
                        strTemp += temp;
                    }else if(vec.get(j).charAt(0) == '∃') {  //特称变全称
                       // System.out.println("444");
                        String temp = vec.get(j);
                        temp = temp.replace('∃','∀');
                        strTemp += temp;
                    } else{   //单谓词
                      //  System.out.println("555");
                        strTemp += "~";
                        strTemp += vec.get(j);
                    }
                }
                for(j = k + 1; j < vec.size(); j++){
                    strTemp += vec.get(j);
                }
                strTemp = strTemp.replace("~~","");  //去掉双重否定
                //System.out.println(strTemp);
                vec.clear();   //请空全部函数
                create(strTemp,vec);   //继续调用分割函数
                i = k + 1;
                continue;
            }
            i++;
        }

    }

    public static void threeStep(List<String> vec){
        //变量更名
        Set<String> set = new HashSet<String>();   //给变量去重
        String[] varList = new String[]{"x","y","z","h","g","e"};
        int i ,j = 0;
        for(i = 0;i < vec.size();){
            //String cha = "∀∃∧∨";
            if(vec.get(i).charAt(0) == '∀' || vec.get(i).charAt(0) == '∃'){
                //此时找到了量词
                //System.out.println(vec.get(i));
                String temp = vec.get(i);
                temp = temp.replace("∀","");   //去掉量词
                temp = temp.replace("∃","");
                temp = temp.replace("(","");   //去掉括号
                temp = temp.replace(")","");
                if(!set.contains(temp)){
                    set.add(temp);
                }else {    //开始替换，先找辖域，然后辖域内替换
                    //System.out.println(vec.get(i));
                    String changeTo = "";   //要修改为的字符串
                    String changeTemp = "";  //进行修改时的赋值字符串
                    for(int f = 0; f < varList.length; f++){
                        if(!set.contains(varList[f])){
                            changeTo = varList[f];
                            break;
                        }
                    }
                    //System.out.println(temp);
                    //System.out.println(changeTo);
                    //先将量词修改
                    // 此时应该将temp  -->  changeTo
                    changeTemp = vec.get(i);
                    changeTemp = changeTemp.replace(temp,changeTo);
                    vec.set(i,changeTemp);
                    //开始寻找辖域
                    int k = i + 1, num = 0;
                    if (vec.get(k).equals(")")) {  //跳过量词后面的")"
                        k++;
                    }
                    if (vec.get(k).equals("~")) {  //跳过"~"
                        k++;
                    }
                    for (; k < vec.size(); k++) //向后搜索（和）匹配
                    {
                        //System.out.println(vec.get(k));
                        if (vec.get(k).equals("(")) num++;
                        if (vec.get(k).equals(")")) num--;
                        if (num == 0) break;
                    }
                    //到此已经找到作用域为（i+1，k),将辖域内的temp字符串全部替换掉
                    for(j = i + 1; j < k + 1; j++){
                        //System.out.println(vec.get(j));
                        if(vec.get(j).contains(temp)){   //存在要修改的值
                            //System.out.println("111");
                            changeTemp = vec.get(j);
                            changeTemp = changeTemp.replace(temp,changeTo);
                            vec.set(j,changeTemp);
                        }
                    }
                    i = k + 1;
                }
            }
            i++;
        }
    }

    public static List<String> fourStep(List<String> vec){
        //System.out.println(vec.size());
        List<String> ans = new ArrayList<>();
        int i = 0,j = 0;
        //String cha = "∀∃∧∨";
        //先跳过全称量词
        boolean ignore = false;
        while(i + 1 < vec.size() && vec.get(i).equals("(") && vec.get(i+1).charAt(0) == '∀'){
            i = i + 3;
            if(vec.get(i).equals("(")){
                i = i + 1;
                ignore = true;   //表示跳过了一个"(",使用之后要跳过结尾的")"
            }
        }
        String temp = "";
        for(;i < vec.size(); i++){
            if(i == vec.size() - 1 && ignore && vec.get(i).equals(")")){
                break;
            }
            if(vec.get(i).equals("∧")){
                System.out.println(temp);
                coutToFile(temp);
                temp = "";
            }else{
                temp += vec.get(i);
            }
        }
        if(!temp.isEmpty()){
            System.out.println(temp);
            ans.add(temp);
            coutToFile(temp);
        }
        return ans;
    }

    public static void coutToFile(String str)  {

        FileWriter fw = null;
        try {
            //创建字符输出流对象，负责向文件内写入
            fw = new FileWriter("resolution.txt",true);   //加true防止覆盖
            //将str里面的内容读取到fw所指定的文件中并换行
            fw.write(str + "\r\n");
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            if(fw!=null){
                try {
                    fw.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    public static String generate(String str){
        StringBuilder sb = new StringBuilder();
        List<String> vec = new ArrayList<>();
        create(str,vec);  //分割字符串，存储到vec中
        sb.append("谓词公式：       ");
        sb.append(str);
        sb.append("<br/>");
        sb.append("     取消->和<->:  ");
        firstStep(vec);
        sb.append(print2(vec));
        sb.append("<br/>");
        sb.append("     将否定词的辖域减少: ");
        secondStep(vec);
        sb.append(print2(vec));
        sb.append("<br/>");
        sb.append("     变量更名:    ");
        threeStep(vec);
        sb.append(print2(vec));
        sb.append("<br/>");
        sb.append("     化成子句:   ");
        List<String> ans = fourStep(vec);
        for(int i = 0; i < ans.size(); i++){
            sb.append(ans.get(i));
        }
        sb.append("<br/>");
        return sb.toString();
    }

    public static void main(String[] args) throws FileNotFoundException {
        //String cha = "∀∃∧∨";
        System.out.println("原来谓词公式：");
        String s1 = "∀(x)(∀(y)P(x,y)->~(∀(y)(Q(x,y)->R(x,y)))";
        //String s1 = "(∀x)((~Poor(x)∧Smart(x))->happy(x))";
        //String s1 = "(∀x)(Read(x)->Smart(x))";
        //String s1 = "Read(Li)∧~Poor(Li)";
        //String s1 = "(∀x)(happy(x)->Exciting(x))";
        System.out.println(s1);
        System.out.println();
        List<String> vec = new ArrayList<>();
        System.out.println("分割后：");
        create(s1,vec);
        print1(vec);
        System.out.println();
        System.out.println();
        System.out.println("=============开始将谓词公式化为子句=============");
        System.out.println("1.取消->和<->:");
        firstStep(vec);
        print1(vec);
        System.out.println();
        System.out.println();
        System.out.println("====将否定词的辖域减少到只作用于一个谓词===========");
        secondStep(vec);
        print1(vec);
        System.out.println();
        System.out.println();
        System.out.println("================变量更名=====================");
        threeStep(vec);
        print1(vec);
        System.out.println();
        System.out.println();
        System.out.println("================化成子句=====================");
        fourStep(vec);
        print1(vec);
        //coutToFile("heiehihie gei ");
        //coutToFile("iogqweiohsdj");
        //String s = "∀(x)(∀(y)P(x,y)->~(∀(y)(Q(x,y)->R(x,y)))";
        //System.out.println(generate(s));
    }
}
