package com.resolution.service;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.resolution.pojo.DiscussPost;
import com.resolution.util.ResolutionConstant;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 文件名：ElasticSearchService
 * 创建者:xiebaoming
 * 创建时间:2022/3/24  21:01
 * 描述：TODO
 */
@Service
public class ElasticSearchService implements ResolutionConstant {



    @Autowired
    @Qualifier("restHighLevelClient")
    private RestHighLevelClient client;

    @Autowired
    LikeService likeService;

    @Autowired
    UserService userService;

    //复杂查询
    public List<Map<String,Object>> search(String keyword, int current, int limit) throws IOException {
        System.out.println("开始搜索"+"  keyword="+keyword+"  current" + current + " limit=" +limit);
        SearchRequest searchRequest = new SearchRequest("resolution");

        //构建搜索的条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        //高亮
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        //设置高亮的字段
        HighlightBuilder.Field highlightTitle =
                new HighlightBuilder.Field("content")
                        .preTags("<b style='color:pink'>")
                        .postTags("</b>");
        highlightTitle.highlighterType("unified");
        highlightBuilder.field(highlightTitle);

/*        HighlightBuilder.Field highlightContent =
                new HighlightBuilder.Field("content")
                        .preTags("<b style='color:pink'>")
                        .postTags("</b>");
        highlightTitle.highlighterType("unified");
        highlightBuilder.field(highlightContent);*/

        //highlightBuilder.field("title"); // 高亮的字段
        //highlightBuilder.requireFieldMatch(false); // 只高亮一次(有多个关键字,只高亮第一个)
        //highlightBuilder.preTags("<b style='color:pink'>");
        //highlightBuilder.postTags("</b>");
        searchSourceBuilder.highlighter(highlightBuilder);

        //查询条件，我们可以使用QueryBuliders工具来实现
        //termQuery表示精确匹配
        //TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", keyword);
        //匹配所有,单条件查询
        //MatchQueryBuilder matchQueryBuilder1 = new MatchQueryBuilder("title",keyword);
        //searchSourceBuilder.query(matchQueryBuilder1);


        //多条件查询
        BoolQueryBuilder must = QueryBuilders.boolQuery().should(QueryBuilders.matchPhraseQuery("title", keyword))
                    .should(QueryBuilders.matchPhraseQuery("content", keyword));

        searchSourceBuilder.query(must);

        //进阶版多条件查询，可以非空不执行
        //条件判断，如果传入的属性为空的话则不执行该查询条件
        /*BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        if(null != req.getItem_brand()){
            boolQueryBuilder.must(QueryBuilders.matchQuery("item_brand.keyword", req.getItem_brand()));
        }
        if(null != req.getItem_color()){
            boolQueryBuilder.must(QueryBuilders.matchQuery("item_color.keyword", req.getItem_color()));
        }*/
        //QueryBuilders.boolQuery().should(QueryBuilders.matchQuery()).should()


        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchSourceBuilder.from(current);
        searchSourceBuilder.size(limit);
        searchSourceBuilder.sort(new FieldSortBuilder("type").order(SortOrder.DESC));
        searchSourceBuilder.sort(new FieldSortBuilder("score").order(SortOrder.DESC));
        searchSourceBuilder.sort(new FieldSortBuilder("createTime").order(SortOrder.DESC));

        searchRequest.source(searchSourceBuilder);

        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        System.out.println(JSON.toJSONString(search.getHits()));
        System.out.println("===================");

        List<Map<String,Object>> discussPosts = new ArrayList<>();
        //对象然后获取每一个的值
        for (SearchHit hit : search.getHits().getHits()) {
            DiscussPost post = new DiscussPost();

            String id = hit.getSourceAsMap().get("id").toString();
            post.setId(Integer.valueOf(id));

            String userId = hit.getSourceAsMap().get("userId").toString();
            post.setUserId(Integer.valueOf(userId));

            String title = hit.getSourceAsMap().get("title").toString();
            post.setTitle(title);

            String content = hit.getSourceAsMap().get("content").toString();
            post.setContent(content);

            String status = hit.getSourceAsMap().get("status").toString();
            post.setStatus(Integer.valueOf(status));

            String createTime = hit.getSourceAsMap().get("createTime").toString();
            post.setCreateTime(new Date(Long.valueOf(createTime)));

            String commentCount = hit.getSourceAsMap().get("commentCount").toString();
            post.setCommentCount(Integer.valueOf(commentCount));

            System.out.println(post);
            //聚合数据
            Map<String, Object> map=new HashMap<>();
            map.put("post",post);
            map.put("user",userService.findUserById(post.getUserId()));
            map.put("likeCount",likeService.findEntityLikeCount(ENTITY_TYPE_POST,post.getId()));
            discussPosts.add(map);

        }
        return discussPosts;
    }

    //添加帖子进elasticsearch
     public void addPostToEs(DiscussPost discussPost) throws IOException{
        IndexRequest request = new IndexRequest("resolution");

        request.id(String.valueOf(discussPost.getId()));

        request.timeout("1s");

        request.source(JSON.toJSONString(discussPost), XContentType.JSON);

        client.index(request,RequestOptions.DEFAULT);
    }


    public void deleteDiscussPost(Integer postId) throws  IOException{

        DeleteRequest request = new DeleteRequest("resolution",String.valueOf(postId));
        request.timeout("1s");
        client.delete(request,RequestOptions.DEFAULT);
    }

}
