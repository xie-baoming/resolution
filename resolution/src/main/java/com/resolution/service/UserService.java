package com.resolution.service;

import com.resolution.dao.LoginTicketMapper;
import com.resolution.dao.UserMapper;
import com.resolution.pojo.LoginTicket;
import com.resolution.pojo.User;
import com.resolution.util.ResolutionConstant;
import com.resolution.util.ResolutionUtil;
import org.apache.maven.surefire.shade.org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import java.util.*;

/**
 * 文件名：UserService
 * 创建者:xiebaoming
 * 创建时间:2022/2/24  12:53
 * 描述：TODO
 */

@Service
public class UserService implements ResolutionConstant {
    @Autowired(required = false)
    private UserMapper userMapper;

    @Autowired(required = false)
    private LoginTicketMapper loginTicketMapper;

    //查找用户
    public User findUserById(int id){
        return userMapper.selectById(id);
    }

    //登录
    public Map<String, Object> login(String username, String password, int expired){
        Map<String, Object> map = new HashMap<>();
        //判断输入是否为空
        if(StringUtils.isBlank(username)){
            map.put("usernameMsg","用户名不能为空");
            return map;
        }
        if(StringUtils.isBlank(password)){
            map.put("passwordMsg","密码不能为空");
            return map;
        }
        //账号验证
        User user=userMapper.selectByName(username);
        if(user == null){
            map.put("usernameMsg","该账号不存在");
            return map;
        }
        System.out.println(user.getPassword());
        System.out.println(password);
        if(!password.equals(user.getPassword())){
            map.put("passwordMsg","密码不正确");
            return map;
        }
        //生成登录凭证,并返回
        LoginTicket loginTicket=new LoginTicket();
        loginTicket.setUserId(user.getId());
        loginTicket.setTicket(ResolutionUtil.generateUUID());
        loginTicket.setStatus(0);
        loginTicket.setExpired(new Date(System.currentTimeMillis() + expired*1000));
        loginTicketMapper.insertLoginTicket(loginTicket);
        map.put("ticket",loginTicket.getTicket());
        return map;
    }

    public User findUserByName(String name){
        return userMapper.selectByName(name);
    }

    //注册
    public Map<String, Object> register(User user){
        Map<String, Object> map = new HashMap<>();
        if(user == null){
            throw new IllegalArgumentException("参数不能为空");
        }
        if(StringUtils.isBlank(user.getUsername())){
            map.put("usernameMsg","账号不能为空");
            return map;
        }
        if(StringUtils.isBlank(user.getPassword())){
            map.put("passwordMsg","密码不能为空");
            return map;
        }
        if(StringUtils.isBlank(user.getEmail())){
            map.put("emailMsg","邮箱不能为空");
            return map;
        }
        /**
         * 验证账号
         */
        User u = userMapper.selectByName(user.getUsername());
        if(u!=null){
            map.put("usernameMsg","用户名已存在");
            return map;
        }
        u=userMapper.selectByEmail(user.getEmail());
        if(u!=null){
            map.put("emailMsg","邮箱已被注册");
            return map;
        }
        /**
         * 注册
         */
        //创建时间
        user.setCreateTime(new Date(System.currentTimeMillis()));
        userMapper.insertUser(user);
        return map;
    }

    public void logout(String ticket){
        loginTicketMapper.updateStatus(ticket,1);  //1为登出，0为登录
    }

    //查询凭证
    public LoginTicket findLoginTickert(String ticket){
        return loginTicketMapper.selectLoginTicket(ticket);
    }

    public int updateHeader(int userId, String headerUrl){
        return userMapper.updateHeaderUrl(userId,headerUrl);
    }

    public Collection<? extends GrantedAuthority> getAuthorities(Integer userId) {
        User user = this.findUserById(userId);
        List<GrantedAuthority> list=new ArrayList<>();
        list.add(new GrantedAuthority() {
            @Override
            public String getAuthority() {
                /*switch (user.getType()){
                    case 1:
                        return AUTHORITY_ADMIN;
                    case 2:
                        return AUTHORITY_MODERATOR;
                    default:
                        return AUTHORITY_USER;
                }*/
                if(user.getId() == 1){
                    return AUTHORITY_ADMIN;
                }
                return AUTHORITY_USER;
            }
        });

        return list;
    }

}
