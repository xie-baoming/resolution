package com.resolution.event;

import com.alibaba.fastjson.JSONObject;
import com.resolution.pojo.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * 文件名：EventProducer
 * 创建者:xiebaoming
 * 创建时间:2022/3/11  18:58
 * 描述：TODO
 */
@Component
public class EventProducer extends Event {

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    //处理事件
    public void fireEvent(Event event){
        // 将事件发布到指定的主体
        //System.out.println("放进主体");
        kafkaTemplate.send(event.getTopic(), JSONObject.toJSONString(event));
    }


}
