package com.resolution;

import com.resolution.dao.UserMapper;
import com.resolution.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 文件名：UserMapperTest
 * 创建者:xiebaoming
 * 创建时间:2022/2/23  21:45
 * 描述：TODO
 */
@SpringBootTest
@ContextConfiguration(classes = ResolutionApplication.class)
public class MapperTest {

    @Autowired(required = false)
    private UserMapper userMapper;

    @Test
    public void UserMapperTest(){
        userMapper.deleteById(1);
        userMapper.deleteById(2);

       User user = new User();
       user.setUsername("xiebaoming");
       user.setPassword("666");
       user.setEmail("3066369691@qq.com");
        user.setCreateTime(new Date(System.currentTimeMillis()));
        userMapper.insertUser(user);
        User user1 = userMapper.selectById(1);
        System.out.println(user1);
        User user2 = userMapper.selectByName("xiebaoming");
        System.out.println(user2);
        User user3 = userMapper.selectByEmail("3066369691@qq.com");
        System.out.println(user3);
    }

    @Test void updateHeaderUrlTest(){
        System.out.println(userMapper);
        int res = userMapper.updateHeaderUrl(5,"111");
        User user = userMapper.selectById(5);
        System.out.println(user);
    }
}
