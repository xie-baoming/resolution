package com.resolution;

import com.resolution.util.SensitiveFilter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

/**
 * 文件名：SensitiveTest
 * 创建者:xiebaoming
 * 创建时间:2022/3/5  9:40
 * 描述：TODO
 */

@SpringBootTest
@ContextConfiguration(classes = ResolutionApplication.class)
public class SensitiveTest {

    @Autowired
    private SensitiveFilter sensitiveFilter;

    @Test
    void testSensitive(){
        String text="211寝室聚众赌博,集体被捕,赌博头目幸泽升";
        text = sensitiveFilter.filter(text);
        System.out.println(text);
        text="@赌@博@";
        text = sensitiveFilter.filter(text);
        System.out.println(text);
    }


}
