package com.resolution;

import com.resolution.util.MailClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * 文件名：MailTest
 * 创建者:xiebaoming
 * 创建时间:2022/2/25  9:03
 * 描述：TODO
 */
@SpringBootTest
@ContextConfiguration(classes = ResolutionApplication.class)
public class MailTest {

    @Autowired
    private MailClient mailClient;

    /**
     * 模板引擎核心类
     */
    @Autowired
    private TemplateEngine templateEngine;

    @Test
    public void testTextMail(){
        mailClient.sendMail("3066369691@qq.com","testText","认真检查阿！大哥");
    }

    @Test
    public void testHTMLMail(){
        Context context=new Context();
        context.setVariable("email","fsds");
        context.setVariable("mailCode","gsagash");
        String content = templateEngine.process("/mail/activation", context);
        System.out.println(content);
        mailClient.sendMail("3066369691@qq.com","testHTML",content);
    }
}

