package com.resolution;

import com.resolution.dao.DiscussPostMapper;
import com.resolution.pojo.DiscussPost;
import com.resolution.service.ElasticSearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/**
 * 文件名：DiscussPostMapperTest
 * 创建者:xiebaoming
 * 创建时间:2022/3/5  14:25
 * 描述：TODO
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ResolutionApplication.class)
public class DiscussPostMapperTest {
    @Autowired(required = false)
    private DiscussPostMapper discussPostMapper;

    @Autowired
    private ElasticSearchService elasticSearchService;

    @Autowired(required = false)
    private ExecutorService executorService;

    private void sleep(long millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     *JDK普通线程池
     */
    @Test
    public void testExecutorService(){
        Runnable task=new Runnable() {
            @Override
            public void run() {
                //logger.debug("Hello ExecutorService");
                System.out.println("hello");
            }
        };
        for (int i = 0; i < 10; i++) {
            executorService.submit(task);
        }
        sleep(10000);
    }

    @Test
    public void test(){
        DiscussPost discussPost = new DiscussPost();
        discussPost.setUserId(6);
        discussPost.setTitle("测试代码");
        discussPost.setContent("这是一段测试代码");
        discussPost.setType(0);
        discussPost.setStatus(0);
        discussPost.setCreateTime(new Date(System.currentTimeMillis()));
        System.out.println(discussPost);
        System.out.println(discussPostMapper);
        System.out.println(discussPostMapper.insertDiscussPost(discussPost));
        int i = discussPostMapper.selectDiscussPostRows(0);
        System.out.println(i);
        List<DiscussPost> discussPosts = discussPostMapper.selectDiscussPosts(0, 0, 5, 1);
        for(DiscussPost discussPost1 : discussPosts){
            System.out.println(discussPost1);
        }

    }

    @Test
    public void testSearch() throws IOException {
        List<Map<String,Object>> discussPosts =  elasticSearchService.search("牛逼",0,5);
    }
}
