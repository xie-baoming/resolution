package com.resolution;

import com.resolution.dao.MessageMapper;
import com.resolution.pojo.Message;
import com.resolution.service.MessageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 文件名：MessageMapperTest
 * 创建者:xiebaoming
 * 创建时间:2022/3/9  10:17
 * 描述：TODO
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ResolutionApplication.class)
public class MessageMapperTest {

    @Autowired(required = false)
    MessageMapper messageMapper;

    @Autowired
    MessageService messageService;

    @Test
    public void testMessageMapper(){
        Message message = new Message();
        message.setContent("666");
        message.setFromId(3);
        message.setToId(6);
        message.setStatus(0);
        messageMapper.insertMessage(message);
        int i = messageMapper.selectConversasionCount(3);
        System.out.println(i);
        List<Message> messages = messageMapper.selectConversations(3, 0, 10);
        for(Message message1 : messages){
            System.out.println(message1);
        }
    }

    @Test
    public void testMapper(){
        int i = messageMapper.selectConversasionCount(3);
        System.out.println(i);
        int j = messageService.findConvesationCount(3);

        System.out.println(j);
    }
}
