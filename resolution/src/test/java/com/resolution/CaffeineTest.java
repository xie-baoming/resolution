package com.resolution;

import com.resolution.pojo.DiscussPost;
import com.resolution.service.DiscussPostService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.DispatcherServletCustomizer;

import java.util.Date;

/**
 * 文件名：CaffeineTest
 * 创建者:xiebaoming
 * 创建时间:2022/3/30  10:07
 * 描述：TODO
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ResolutionApplication.class)
public class CaffeineTest {

    @Autowired
    private DiscussPostService discussPostService;

    @Test
    public void initDataForTest(){
        for(int i = 0; i < 100000; i++){
            DiscussPost discussPost = new DiscussPost();
            discussPost.setUserId(3);
            discussPost.setTitle("压力测试帖子");
            discussPost.setContent("开始压力测试");
            discussPost.setCreateTime(new Date());
            discussPost.setScore(Math.random() * 2000);
            discussPostService.addDiscussPost(discussPost);
        }
    }

    @Test
    public void testCache(){
        System.out.println(discussPostService.findDiscussionPosts(0,0,10,1));
        System.out.println(discussPostService.findDiscussionPosts(0,0,10,1));
        System.out.println(discussPostService.findDiscussionPosts(0,0,10,1));
        System.out.println(discussPostService.findDiscussionPosts(0,0,10,0));
    }
}
