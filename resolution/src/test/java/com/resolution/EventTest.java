package com.resolution;

import com.resolution.pojo.Message;
import com.resolution.service.MessageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 文件名：EventTest
 * 创建者:xiebaoming
 * 创建时间:2022/3/11  21:51
 * 描述：TODO
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ResolutionApplication.class)
public class EventTest {

    @Autowired
    private MessageService messageService;

    @Test
    public void test(){
        Message message = messageService.findLatestNotice(3, "comment");
        System.out.println(message);
    }
}
