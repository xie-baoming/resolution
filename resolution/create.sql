CREATE DATABASE resolution;

USE resolution

DROP  TABLE USER;

CREATE TABLE `user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(40) DEFAULT NULL,
  `password` VARCHAR(40) DEFAULT NULL,
  `email` VARCHAR(40) DEFAULT NULL,
  `create_time` TIMESTAMP NULL DEFAULT '2022-02-23 10:24:02',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8

CREATE TABLE `login_ticket` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `ticket` VARCHAR(45) NOT NULL,
  `status` INT DEFAULT '0' COMMENT '0-有效;1-无效',
  `expired` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ticket` (`ticket`(20))
) ENGINE=INNODB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8